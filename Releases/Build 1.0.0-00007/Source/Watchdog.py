# -*- coding: utf-8 -*-
"""Stellt eine Klasse \"WatchDog\" zum Überwachen der Module zur Verfügung."""

import time
import threading

import Global


class Watchdog(object):
    """Klasse um ausgefallene Instanzen neu zu Starten."""

    def __init__(self):
        self.is_started = False
        self.allow_stop = False
        self.__run = True
        self.__thread = threading.Thread(target=self.__loop, args=(), name=u'Watchdog')
        self.__thread.daemon = True
        self.__thread.start()
        self.restart = False

    def __restart_instances(self):
        """Starte im Bedarfsfall alle Instanzen neu."""
        self.is_started = False
        Global.Instances.os_interface.set_led(Global.CONST.OSI.COLORS.BLUE)

        # stop all instances
        Global.Instances.logger.stop()
        Global.Instances.state_machine.stop()
        Global.Instances.display_server.stop()
        Global.Instances.command_server.stop()
        Global.Instances.web_server.stop()
        Global.Instances.state_machine.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN

        # set bluescreen
        Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_BLUESCREEN]
        time.sleep(5)
        time.sleep(2 * max(Global.CONST.SM.TIME_INCREMENT, Global.CONST.OSI.TIME_INCREMENT_LED))
        Global.Instances.os_interface.stop()
        # wait unit all instances are stopped
        while Global.Instances.logger.is_running() \
                or Global.Instances.state_machine.is_running() \
                or Global.Instances.display_server.is_running() \
                or Global.Instances.command_server.is_running() \
                or Global.Instances.web_server.is_running() \
                or Global.Instances.os_interface.is_running():
            time.sleep(0.01)

        Global.Instances.logger.start()
        Global.Instances.state_machine.init_values()
        Global.Instances.screen_handler.init_values()
        Global.Instances.state_machine.start()
        self.restart = False

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False

    def __update(self):
        if not self.is_started or self.allow_stop:
            return

        if Global.Instances.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_ON and \
                not Global.Instances.display_server.is_running():
            Global.Instances.logger.log(u'Start Display Server', __name__, Global.CONST.LOG.SEV.INFO)
            Global.Instances.display_server.start()
        if Global.Instances.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_OFF and \
                Global.Instances.display_server.is_running():
            Global.Instances.logger.log(u'Stop Display Server', __name__, Global.CONST.LOG.SEV.INFO)
            Global.Instances.display_server.stop()

        ''' deactivated in this version
        if not Global.Instances.command_server.is_running():
            Global.Instances.logger.log(u'Restart Command Server', __name__, Global.CONST.LOG.SEV.ERROR)
            Global.Instances.command_server.start()
        '''
        ''' deactivated in this version
        if not Global.Instances.web_server.is_running():
            Global.Instances.logger.log(u'Restart Web Server', __name__, Global.CONST.LOG.SEV.ERROR)
            Global.Instances.command_server.start()
        '''
        if not Global.Instances.logger.is_running():
            Global.Instances.logger.log(u'Restart Logger', __name__, Global.CONST.LOG.SEV.ERROR)
            Global.Instances.logger.start()
        if not (not self.restart and Global.Instances.state_machine.is_running() and
                Global.Instances.os_interface.is_running()):
            Global.Instances.logger.log(u'Restart all instances', __name__, Global.CONST.LOG.SEV.ERROR)
            self.__restart_instances()

    def __loop(self):
        while self.__run:
            try:
                self.__update()
            except Exception as e:
                Global.Instances.logger.log(u'Exception occurred during update Watchdog: %s' % e,
                                            __name__, Global.CONST.LOG.SEV.ERROR)
            time.sleep(Global.CONST.WD.TIME_INCREMENT)
