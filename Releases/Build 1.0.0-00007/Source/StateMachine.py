# -*- coding: utf-8 -*-
"""Stellt eine Klasse "StateMachine" zur Verfügung.
"""

import time
import threading
from datetime import datetime as dt

import Global
import UpdateHandler


class StateMachine(object):
    """Implementiert eine StateMachine zur Steuerung der Applikation."""

    def __init__(self):
        self.__run = False
        self.__commands = list()
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None
        self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN

    @property
    def state(self):
        """Der aktuelle Zustand."""
        return self.__state

    def init_values(self):
        """Initialisiert alle Variablen der StateMachine."""
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None

    def __fetch_ir_commands(self):
        """Holt IR-Kommandos aus dem OsInterface."""
        commands = Global.Instances.os_interface.get_ir_commands()
        for command in commands:
            self.__commands.append({'type': Global.CONST.SM.CMD_IR, 'data': command})

    def get_total_seconds_since_last_trans(self):
        """Gibt die Zeit seit dem letzten Übergang in Sekunden zurück. Die Zeit orientiert sich dabei an der Systemzeit 
        und ist daher nicht geeignet wenn die Systemzeit verstellt wird.
        
        :return: Int Zeit seit dem letzten Übergang in Sekunden.
        """
        delta = dt.now() - self.last_transition_time
        return delta.total_seconds()

    def get_total_seconds_since_last_command(self):
        """Gibt die Zeit seit dem letzten akzeptierten Kommando in Sekunden zurück. Die Zeit orientiert sich dabei an 
        der Systemzeit und ist daher nicht geeignet wenn die Systemzeit verstellt wird.

        :return: Int Zeit seit dem letzten akzeptierten Kommando in Sekunden.
        """
        delta = dt.now() - self.last_command_time
        return delta.total_seconds()

    def get_total_seconds_increment(self):
        """Gibt die Zeit seit dem letzten akzeptierten Kommando in Sekunden zurück. Die Zeit orientiert sich dabei an 
        der Anzahl Updateschleifen und kann daher von der echten Zeit abweichen. Da sie aber unabhängig von der 
        Systemzeit ist kann sie verwendet werden, wenn die Systemzeit verstellt wird.

        :return: Int Zeit seit dem letzten akzeptierten Kommando in Sekunden.
        """
        return self.__increments * Global.CONST.SM.TIME_INCREMENT

    def __set_command_time(self):
        """Muss bei jedem akzeptieren Kommando aufgerufen werden, damit die Zeit seit dem letzten Kommando 
        zurückgesetzt wird."""
        self.__increments = 0
        self.last_command_time = dt.now()

    def pass_command(self, command):
        """Nimmt ein neues Kommando entgegen.
        
        :param command: Dict mit Key "type", das Kommando
        """
        if isinstance(command, dict) and command.get('type', '') in (Global.CONST.SM.CMD_SYS,
                                                                     Global.CONST.SM.CMD_IR,
                                                                     Global.CONST.SM.CMD_NET):
            self.__commands.append(command)
        else:
            raise Exception(u'Command has to be an dict with key "type"')

    def __change_state(self, target_state, gui_params):
        """Setzt den Zustand der StateMachine.
        
        :param target_state: Int Zustand der eingenommen werden soll.
        :param gui_params: Parameter für die GUI, werden dem ScreenHandler übergeben.
        """
        self.__state = target_state
        self.last_transition_time = dt.now()
        self.__set_command_time()
        Global.Instances.screen_handler.set_page(target_state, gui_params)
        self.__state_changed = True
        if target_state == Global.CONST.SM.STATE_STANDBY:
            Global.Instances.os_interface.switch_hdmi_off()
        Global.Instances.logger.log(u'State changed: %s' % Global.CONST.SM.STATE_DICT.get(target_state, u'Unbekannt'),
                                    __name__, Global.CONST.LOG.SEV.INFO)

    def is_running(self):
        """Gibt zurück, ob die Updateschleife läuft.

        :return: Bool Wahr, wenn die Updateschleife läuft
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet die Updateschleife wenn sie noch nicht läuft."""
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'StateMachine')
            self.__thread.start()

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False

    def __update(self):
        # get command
        self.__fetch_ir_commands()
        if self.__commands:
            command = self.__commands.pop(0)
            command_type = command['type']
            Global.Instances.logger.log('IR command "%s" received' % command['data'],
                                        __name__, Global.CONST.LOG.SEV.DEBUG)
        else:
            command = None
            command_type = None

        self.__increments += 1

        # blink led if ir command
        if command_type == Global.CONST.SM.CMD_IR and (not self.state == Global.CONST.SM.STATE_STANDBY or
                                                       command['data'] == Global.CONST.SM.IRCMD.POWER):
            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.PURPLE, 0.2)

        # state machine
        if self.state == Global.CONST.SM.STATE_START:
            Global.Instances.screen_handler.set_start_screen_message(u'Initialisiere Hardware...')
            Global.Instances.logger.log('Start init hardware...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.os_interface.init_hardware()
            Global.Instances.logger.log('Hardware inited', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.screen_handler.set_start_screen_message(u'Teste LED...')
            Global.Instances.logger.log('Start init LED...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.os_interface.init_led()
            Global.Instances.logger.log('LED inited', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.screen_handler.set_start_screen_message(u'Starte alle Instanzen...')
            Global.Instances.logger.log('App start instances...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.app.start_instances()
            Global.Instances.logger.log('App instances start finished', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.os_interface.set_led(None)

            Global.Instances.screen_handler.set_start_screen_message(u'Prüfe ob Update vorhanden ist...')

            Global.Instances.logger.log('Start check update...', __name__, Global.CONST.LOG.SEV.DEBUG)
            if UpdateHandler.check_update_finish():
                Global.Instances.logger.log('Update finished detected. Delete update directory...', __name__,
                                            Global.CONST.LOG.SEV.DEBUG)
                try:
                    UpdateHandler.delete_update_dir()
                except Exception as e:
                    Global.Instances.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                Global.CONST.LOG.SEV.ERROR)
                    Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                             u'nicht gelöscht werden.', True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return
                else:
                    Global.Instances.logger.log('Update dir deleted.', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.logger.log('Check update next start available...', __name__, Global.CONST.LOG.SEV.DEBUG)
            if UpdateHandler.check_nextstart_script_available() and not UpdateHandler.check_update_finish():
                self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN
                Global.Instances.logger.log('Next start update script available. Execute next start script.', __name__,
                                            Global.CONST.LOG.SEV.DEBUG)
                Global.Instances.screen_handler.set_start_screen_message(u'Update wird weiter ausgeführt...')
                time.sleep(10)
                self.__change_state(Global.CONST.SM.STATE_QUIT, {})
                UpdateHandler.start_nextstart_process()
                return

            Global.Instances.logger.log('Delete update directory if exists...', __name__,
                                        Global.CONST.LOG.SEV.DEBUG)
            try:
                UpdateHandler.delete_update_dir()
            except Exception as e:
                Global.Instances.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                            Global.CONST.LOG.SEV.ERROR)
                Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                         u'nicht gelöscht werden.', True)
                time.sleep(10)
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                return

            if UpdateHandler.update_dir_exists():
                Global.Instances.logger.log(u'Update dir still exists. Going to execute the program normally.',
                                            __name__,
                                            Global.CONST.LOG.SEV.ERROR)
                Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                         u'nicht gelöscht werden. Programm wird '
                                                                         u'normal ausgeführt.', True)
                time.sleep(10)
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                return

            # Update package in home dir
            if UpdateHandler.check_update_in_home_dir():
                Global.Instances.logger.log(u'Update package found in home directory. Going to unpack update package.',
                                            __name__,
                                            Global.CONST.LOG.SEV.DEBUG)
                Global.Instances.screen_handler.set_start_screen_message(u'Update wurde im Home-Ordner gefunden.')
                time.sleep(10)
                Global.Instances.screen_handler.set_start_screen_message(u'Update wird entpackt...')
                try:
                    UpdateHandler.unpack_update_home_dir()
                except Exception as e:
                    Global.Instances.logger.log(u'Could not unpack update package: {0}'.format(unicode(e)), __name__,
                                                Global.CONST.LOG.SEV.ERROR)
                    Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update konnte nicht '
                                                                             u'entpackt werden. Update kann nicht '
                                                                             u'ausgeführt werden.', True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                Global.Instances.logger.log(u'Check if update is possible', __name__,
                                            Global.CONST.LOG.SEV.DEBUG)
                Global.Instances.screen_handler.set_start_screen_message(u'Prüfe ob Update möglich ist...')
                time.sleep(10)
                try:
                    update_check = UpdateHandler.check_update_possible()
                except Exception as e:
                    Global.Instances.logger.log(u'Failed to check update: {0}'.format(unicode(e)), __name__,
                                                Global.CONST.LOG.SEV.ERROR)
                    Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Prüfung schlug fehl. '
                                                                             u'Update kann nicht ausgeführt werden.',
                                                                             True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                if update_check[0]:  # update possible
                    Global.Instances.logger.log(u'Update possible: {0}'.format(update_check[1]), __name__,
                                                Global.CONST.LOG.SEV.DEBUG)
                    Global.Instances.screen_handler.set_start_screen_message(u'Update kann ausgeführt werden.')
                    time.sleep(10)
                    try:
                        UpdateHandler.delete_home_update()
                    except Exception as e:
                        Global.Instances.logger.log(u'Failed to delete update package in home dir: {0}'
                                                    .format(unicode(e)),
                                                    __name__,
                                                    Global.CONST.LOG.SEV.ERROR)
                        Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update konnte nicht '
                                                                                 u'aus Home-Ordner gelöscht werden.',
                                                                                 True)
                        time.sleep(10)
                        self.__change_state(Global.CONST.SM.STATE_CONFIRM_UPDATE, {})
                        return
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_UPDATE, {})
                    return
                else:
                    Global.Instances.logger.log(u'Update not possible: {0}'.format(update_check[1]), __name__,
                                                Global.CONST.LOG.SEV.DEBUG)
                    Global.Instances.screen_handler.set_start_screen_message(update_check[1], True)
                    time.sleep(10)
                    try:
                        UpdateHandler.delete_update_dir()
                    except Exception as e:
                        Global.Instances.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                    Global.CONST.LOG.SEV.ERROR)
                        Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                                 u'nicht gelöscht werden.',
                                                                                 True)
                        time.sleep(10)
                        self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                        return
                    else:
                        Global.Instances.logger.log('Update dir deleted.', __name__, Global.CONST.LOG.SEV.DEBUG)

            # Update package on usb device
            if UpdateHandler.check_update_on_usb_drive():
                Global.Instances.logger.log(u'Update package found on usb device. Going to unpack update package.',
                                            __name__,
                                            Global.CONST.LOG.SEV.DEBUG)
                Global.Instances.screen_handler.set_start_screen_message(u'Update wurde auf einem USB-Gerät gefunden.')
                time.sleep(10)
                Global.Instances.screen_handler.set_start_screen_message(u'Update wird entpackt...')
                try:
                    UpdateHandler.unpack_update_usb_drive()
                except Exception as e:
                    Global.Instances.logger.log(u'Could not unpack update package: {0}'.format(unicode(e)), __name__,
                                                Global.CONST.LOG.SEV.ERROR)
                    Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update konnte nicht '
                                                                             u'entpackt werden. Update kann nicht '
                                                                             u'ausgeführt werden.',
                                                                             True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                Global.Instances.logger.log(u'Check if update is possible', __name__,
                                            Global.CONST.LOG.SEV.DEBUG)
                Global.Instances.screen_handler.set_start_screen_message(u'Prüfe ob Update möglich ist...')
                time.sleep(10)
                try:
                    update_check = UpdateHandler.check_update_possible()
                except Exception as e:
                    Global.Instances.logger.log(u'Failed to check update: {0}'.format(unicode(e)), __name__,
                                                Global.CONST.LOG.SEV.ERROR)
                    Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Prüfung schlug fehl. '
                                                                             u'Update kann nicht ausgeführt werden.',
                                                                             True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                if update_check[0]:  # update possible
                    Global.Instances.logger.log(u'Update possible: {0}'.format(update_check[1]), __name__,
                                                Global.CONST.LOG.SEV.DEBUG)
                    Global.Instances.screen_handler.set_start_screen_message(u'Update kann ausgeführt werden.')
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_UPDATE, {})
                    return
                else:
                    Global.Instances.logger.log(u'Update not possible: {0}'.format(update_check[1]), __name__,
                                                Global.CONST.LOG.SEV.DEBUG)
                    Global.Instances.screen_handler.set_start_screen_message(update_check[1], True)
                    time.sleep(10)
                    try:
                        UpdateHandler.delete_update_dir()
                    except Exception as e:
                        Global.Instances.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                    Global.CONST.LOG.SEV.ERROR)
                        Global.Instances.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                                 u'nicht gelöscht werden.', True)
                        time.sleep(10)
                        self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                        return
                    else:
                        Global.Instances.logger.log('Update dir deleted.', __name__, Global.CONST.LOG.SEV.DEBUG)

            self.__change_state(Global.CONST.SM.STATE_IDLE, {})
            Global.Instances.screen_handler.set_start_screen_message(u'Liedanzeige initialisiert')

        elif self.state == Global.CONST.SM.STATE_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_QUIT, command['data'])
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__change_state(Global.CONST.SM.STATE_NUMBER, command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    if Global.Instances.song_handler.has_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                    else:
                        Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                elif command['data'] == Global.CONST.SM.IRCMD.MENU:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME, None)
                elif command['data'] == Global.CONST.SM.IRCMD.GUIDE:
                    self.__change_state(Global.CONST.SM.STATE_IP, None)
            elif Global.Instances.general_settings.time_1_5 != 0.0 and \
                 self.get_total_seconds_since_last_command() >= Global.Instances.general_settings.time_1_5 * 60:
                self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
        elif self.state == Global.CONST.SM.STATE_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N4_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.toggle_solo(command['data'])
            elif self.get_total_seconds_since_last_command() >= Global.Instances.general_settings.time_2_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_NUMBER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] in (Global.CONST.SM.IRCMD.EXIT, Global.CONST.SM.IRCMD.LEFT):
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.DELIMITER:
                    if Global.Instances.screen_handler.song_completed():
                        if Global.Instances.screen_handler.has_valide_song():
                            self.__change_state(Global.CONST.SM.STATE_SONG, None)
                        else:
                            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                            self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                    else:
                        self.__set_command_time()
                        Global.Instances.screen_handler.add_number(command['data'])
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_number(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.delete_number()
                elif command['data'] in (Global.CONST.SM.IRCMD.RIGHT, Global.CONST.SM.IRCMD.OK):
                    if Global.Instances.screen_handler.has_valide_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, None)
                    else:
                        Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                        self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= Global.Instances.general_settings.time_3_2:
                if Global.Instances.screen_handler.has_valide_song():
                    self.__change_state(Global.CONST.SM.STATE_SONG, None)
                else:
                    Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_BLUESCREEN:
            pass
        elif self.state == Global.CONST.SM.STATE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    Global.Instances.os_interface.switch_hdmi_on()
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_QUIT:
            # wait until slave has shutdown
            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.WHITE, 30)
            i = 30
            while i > 0:
                if not Global.Instances.display_server.is_running():
                    break
                time_delta = dt.now() - Global.Instances.display_server.last_slave_request
                if time_delta.total_seconds() > 10.0:
                    break
                time.sleep(1)
                i -= 1
            Global.Instances.app.quit()
        elif self.state == Global.CONST.SM.STATE_CONFIRM_QUIT:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.CONFIRM_SHUTDOWN, Global.CONST.SM.IRCMD.OK):
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_REBOOT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_QUIT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_7_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DATE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.PLUS:
                    self.__set_command_time()
                    Global.Instances.os_interface.hour_plus()
                elif command['data'] == Global.CONST.SM.IRCMD.MINUS:
                    self.__set_command_time()
                    Global.Instances.os_interface.hour_minus()
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_time_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_time_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_time()
            elif self.get_total_seconds_increment() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_DATE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_date_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_date_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_date()
            elif self.get_total_seconds_increment() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_CLOCK_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DATE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2, Global.CONST.SM.IRCMD.N3):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_CLOCK_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_ORDER_SOLO, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_ORDER_SOLO, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_SONG_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DISPLAY_SERVER, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SYSTEM_INFORMATION:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_IP:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_17_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_DISPLAY_SERVER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DISPLAY_SERVER, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SYSTEM_INFORMATION, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.DELIMITER:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(Global.CONST.SCH.DELIMITER)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_ORDER_SOLO:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2, Global.CONST.SM.IRCMD.N3,
                                         Global.CONST.SM.IRCMD.N4):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_CONFIRM_UPDATE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    if UpdateHandler.get_update_confirmation_number() == Global.Instances.screen_handler.get_cache():
                        self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN
                        UpdateHandler.start_update_process()
                        self.__change_state(Global.CONST.SM.STATE_QUIT, {})
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    try:
                        UpdateHandler.delete_update_dir()
                    except Exception as e:
                        Global.Instances.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                    Global.CONST.LOG.SEV.ERROR)

                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
            elif self.get_total_seconds_since_last_trans() > Global.CONST.SM.TIME.CHANGE_SETTINGS:
                try:
                    UpdateHandler.delete_update_dir()
                except Exception as e:
                    Global.Instances.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                Global.CONST.LOG.SEV.ERROR)
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
        else:
            Global.Instances.logger.log(u'Unknown state, restart with WatchDog', __name__, Global.CONST.LOG.SEV.ERROR)
            Global.Instances.watchdog.restart = True

    def __loop(self):
        while self.__run:
            self.__state_changed = True
            while self.__state_changed or self.__commands:
                self.__state_changed = False
                self.__update()
            time.sleep(Global.CONST.SM.TIME_INCREMENT)
