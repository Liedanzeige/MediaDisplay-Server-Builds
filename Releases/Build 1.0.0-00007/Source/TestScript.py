# -*- coding: utf-8 -*-
"""Stellt eine Klassen zur Prüfung der Liedanzeige bereit. Kann unabhängig von anderen Programmteilen ausgeführt werden.
"""

import Global
import OsInterface

import codecs
from datetime import datetime
import re

NORESULT = 0
PASSED = 1
FAILED = 2

VERDICT_DICT = {
    0: u'Kein Ergebnis',
    1: u'Bestanden',
    2: u'Fehlgeschlagen'
}


class TestLogger(object):
    """Log die Ergebnisse des Tests."""

    def __init__(self):
        file_name = u'TestScriptResult_%s' % str(datetime.now()).replace(':', '_')
        self.log_file = codecs.open(file_name, 'w', 'utf-8')
        self.log_text = u''
        self.separator = u'~' * 100
        self.verdict = NORESULT

    def log(self, message, verdict):
        """Log eine Meldung mit einem Verdict

        :param message: String, Meldung die geloggt werden soll
        :param verdict: Integer, Verdict der Meldung
        """
        if verdict > self.verdict:
            self.verdict = verdict
        message_wo_whitespace = re.sub(r'[\r\n\t]', u'', message)
        self.log_text += message_wo_whitespace
        print message_wo_whitespace
        self.log_file.write(message_wo_whitespace + u'\n')

    def ask_yes_no(self, prompt, expected_answers=('J', 'j'), log_passed='Bestanden', log_failed='Fehlgeschlagen'):
        """Stellt dem Benutzer eine Frage und loggt das Ergebnis

        :param prompt: String, Frage die gestellt wird
        :param expected_answers: Iterable, Erwartete Antwort(en)
        :param log_passed: String, Meldung die geloggt wird wenn die Antwort in expected_answers ist.
        :param log_failed: String, Meldung die geloggt wird wenn die Antwort nicht in expected_answers ist.
        :return:
        """
        answer = raw_input(prompt=prompt)
        if answer in expected_answers:
            self.log(log_passed, PASSED)
        else:
            self.log(log_failed, FAILED)


class TestRunner(TestLogger):
    """Führt den Test aus."""

    def test_bme280(self):
        """Prüft den BME280."""
        try:
            import Adafruit_BME280 as BME280
        except Exception as e:
            self.log(u'Import des Adafruit_BME280 fehlgeschlagen: {0}'.format(unicode(e)), FAILED)
            return
        else:
            self.log(u'Import des Adafruit_BME280 Moduls', PASSED)
        env_sensor = None
        try:
            env_sensor = BME280.BME280(t_mode=BME280.BME280_OSAMPLE_8, p_mode=BME280.BME280_OSAMPLE_8,
                                       h_mode=BME280.BME280_OSAMPLE_8, address=Global.CONST.OSI.BME280_ADDRESS_1)
        except Exception as e1:
            try:
                env_sensor = BME280.BME280(t_mode=BME280.BME280_OSAMPLE_8, p_mode=BME280.BME280_OSAMPLE_8,
                                           h_mode=BME280.BME280_OSAMPLE_8, address=Global.CONST.OSI.BME280_ADDRESS_2)
            except Exception as e2:
                self.log(u'Zwei Fehler beim Initialiseren des BME280: %s %s' % (str(e1), str(e2)), FAILED)
            else:
                self.log(u'Initialiseren des BME280 mit zweiter Adresse', PASSED)
        else:
            self.log(u'Initialiseren des BME280 mit erster Adresse', PASSED)

    def run(self):
        """ Führt den Test aus."""
        start_time = datetime.now()
        self.log(u'####################################### --> Starte Test <-- ########################################', NORESULT)
        self.log(u'Version: %s' % Global.CONST.VERSION, NORESULT)
        self.log(u'Build: %s' % Global.CONST.BUILD, NORESULT)
        self.log(u'Commit: %s' % Global.CONST.COMMIT, NORESULT)
        osi = OsInterface.OsInterface()
        self.log(u'Serial number or CPU: %s' % osi.serial_number, NORESULT)
        self.log(self.separator, NORESULT)
        self.test_bme280()
        self.log(self.separator, NORESULT)
        end_time = datetime.now()
        self.log(u'Duration: %f' % (end_time - start_time).total_seconds(), NORESULT)
        self.log(self.separator, NORESULT)
        self.log(u'################################### Testergebnis: {0} ###################################'.format(VERDICT_DICT[self.verdict]), NORESULT)
        self.log(u'####################################### --> Test beendet <-- #######################################', NORESULT)


if __name__ == '__main__':
    test_runner = TestRunner()
    test_runner.run()
