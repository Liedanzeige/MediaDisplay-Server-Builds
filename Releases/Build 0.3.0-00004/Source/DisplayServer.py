# -*- coding: utf-8 -*-
"""Stellt den DisplayServer zur Verfügung."""

from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import urllib2

import Global


class DisplayRequestHandler(BaseHTTPRequestHandler):
    """Handelt Requests für den DisplayServer."""

    @staticmethod
    def __generate_html():
        """Generate HTML for State 'Zeige Lied'
        
        :return: HTML Text
        """
        html = u'''<!DOCTYPE html>
<html>
    <head>
        <title>MediaDisplay-Display</title>
        <meta charset="utf-8"/>
        <meta http-equiv="refresh" content="3">
    </head>
    <body style="background-color:''' + Global.CONST.SCH.COLOR.BG + u'''; color:''' + \
               Global.CONST.SCH.COLOR.FG + u''';">
        %s
        <footer>
            <p>
                MediaDisplay-Server Display Server
            </p>
            <p>
                Version: ''' + Global.CONST.VERSION + u''' Build: ''' + Global.CONST.BUILD + u'''
            </p>
        </footer>
    </body>
</html>'''

        if Global.Instances.state_machine.state == Global.CONST.SM.STATE_SONG:
            current_song = Global.Instances.screen_handler.get_current_song()
            text = u'<h1> Zeige Lied </h1>\n'
            text += u'        <h3>Liederbuch: %s</h3>\n' % current_song['book']
            text += u'        <h3>Titel: %s</h3>\n' % current_song['title']
            text += u'        <table style="padding: 10px;">\n'
            if current_song['Sopran'] == '1':
                text += u'            <tr style="background-color:' + Global.CONST.SCH.COLOR.SOLO_SOPRAN_BG + \
                        u'; color:' + Global.CONST.SCH.COLOR.SOLO_SOPRAN_FG + \
                        u';"><td style="padding: 10px;">Sopran</td></tr>\n'
            if current_song['Alt'] == '1':
                text += u'            <tr style="background-color:' + Global.CONST.SCH.COLOR.SOLO_ALT_BG + \
                        u'; color:' + Global.CONST.SCH.COLOR.SOLO_ALT_FG + \
                        u';"><td style="padding: 10px;">Alt</td></tr>\n'
            if current_song['Tenor'] == '1':
                text += u'            <tr style="background-color:' + Global.CONST.SCH.COLOR.SOLO_TENOR_BG + \
                        u'; color:' + Global.CONST.SCH.COLOR.SOLO_TENOR_FG + \
                        u';"><td style="padding: 10px;">Tenor</td></tr>\n'
            if current_song['Bass'] == '1':
                text += u'            <tr style="background-color:' + Global.CONST.SCH.COLOR.SOLO_BASS_BG + \
                        u'; color:' + Global.CONST.SCH.COLOR.SOLO_BASS_FG + \
                        u';"><td style="padding: 10px;">Bass</td></tr>\n'
            text += u'        </table>\n'
            html = html % text
        else:
            html = html % u'<h1>%s</h1>' % Global.CONST.SM.STATE_DICT.get(
                Global.Instances.state_machine.state, u'Unbekannt')
        return html

    def log_error(self, format_, *args):
        """Log an arbitrary error message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip address and current date/time are prefixed to every
        message.

        :param format_: Str format string for the message to be logged.
        :param args: Parameters for messages to be logged.
        """
        message = u"%s - - [%s] %s" % (self.client_address[0], self.log_date_time_string(), format_ % args)
        Global.Instances.logger.log(message, __name__, Global.CONST.LOG.SEV.ERROR)

    def log_message(self, format_, *args):
        """Log an arbitrary message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip address and current date/time are prefixed to every
        message.
        
        :param format_: Str format string for the message to be logged.
        :param args: Parameters for messages to be logged.
        """
        message = u"%s - - [%s] %s" % (self.client_address[0], self.log_date_time_string(), format_ % args)
        Global.Instances.logger.log(message, __name__, Global.CONST.LOG.SEV.INFO)

    def do_GET(self):
        """Serve a GET request."""

        decoded_url = urllib2.unquote(self.path).decode('utf-8')

        if decoded_url in ('', '/'):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(self.__generate_html().encode('utf-8'))
        elif decoded_url == '/erstetelefonnachricht':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write("daspferdfrisstkeinengurkensalat")
        else:
            self.send_error(404, 'Nothing matches the given URI')
            self.wfile.write('Wolltest du mich testen oder was?')


class DisplayServer(ThreadingMixIn, HTTPServer):
    """Implementiert einen Server zur Anzegie des Zustands der Liedanzeige für den Tontechniker läuft auf port 48080."""

    def __init__(self, server_address, request_handler_class, bind_and_activate=True):
        HTTPServer.__init__(self, server_address, request_handler_class, bind_and_activate)
        self.__thread = None

    def is_running(self):
        """Gibt zurück ob der Server läuft.
        
        :return: Boolean, True wenn der Server läuft, sonst False.
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet den Server, wenn er noch nicht läuft."""
        if not self.is_running():
            self.__thread = threading.Thread(target=self.serve_forever, args=(), name='DisplayServer')
            self.__thread.start()

    def stop(self):
        """Stoppt den Server."""
        if self.is_running():
            self.shutdown()
