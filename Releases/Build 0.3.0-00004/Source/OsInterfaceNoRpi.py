# -*- coding: utf-8 -*-
"""Dieses Modul ist nur ein leeres Modul um das Betriebssystemverhalten zu simulierten, wenn die Anwendung nicht auf 
dem Raspberry ausgeführt wird. Für die richtige Dokumentation siehe OsInterfaceRaspberry.
"""

import time
import threading

import Global


class OsInterface(object):

    def __init__(self):
        self.__thread = None
        self.__commands = list()
        self.__run = False

    def init_hardware(self):
        time.sleep(0.5)

    def init_led(self):
        time.sleep(2)

    def get_ir_commands(self):
        temp_commands = self.__commands[:]
        self.__commands = list()
        return temp_commands

    def add_ir_command(self, command):
        # Very important! This is a workaround for ir_large_input_test. For more information look for "Python GIL"
        open('TestExecutionQueue.txt', 'r')

        self.__commands.append(command)

    def set_led(self, color):
        pass

    def set_led_time(self, color, time):
        pass

    @staticmethod
    def hour_plus():
        pass

    @staticmethod
    def hour_minus():
        pass

    @staticmethod
    def minute_plus():
        pass

    @staticmethod
    def minute_minus():
        pass

    @staticmethod
    def set_date_time(time_str):
        pass

    @staticmethod
    def switch_hdmi_off():
        pass

    @staticmethod
    def switch_hdmi_on():
        pass

    @staticmethod
    def reboot():
        pass

    @staticmethod
    def shutdown():
        pass

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'OsInterface')
            self.__thread.start()

    def stop(self):
        self.__run = False

    def __update(self):
        time.sleep(0.001)

    def __loop(self):
        while self.__run:
            self.__update()
            time.sleep(Global.CONST.OSI.TIME_INCREMENT_LED)
