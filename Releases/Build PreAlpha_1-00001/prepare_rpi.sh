#! /bin/bash

echo "change to home dir"
cd ~

echo "apt-get update"
sudo apt-get update

echo "install kivy dependecies"
sudo apt-get -y install pkg-config
sudo apt-get -y install libgl1-mesa-dev
sudo apt-get -y install libgles2-mesa-dev
sudo apt-get -y install python-pygame
sudo apt-get -y install python-setuptools
sudo apt-get -y install libgstreamer1.0-dev
sudo apt-get -y install git-core
sudo apt-get -y install gstreamer1.0-plugins-{bad,base,good,ugly}
sudo apt-get -y install gstreamer1.0-{omx,alsa}
sudo apt-get -y install python-dev
sudo pip install cython

echo "git clone kivy"
git clone https://github.com/kivy/kivy.git@master
cd kivy

echo "python build kivy"
python setup.py build
sudo python setup.py install

echo "Install LIRC"
sudo apt-get -y install lirc
sudo apt-get -y install python-lirc


# todo fetch MediaDisplay-Server
cd ~

# todo make autostart
