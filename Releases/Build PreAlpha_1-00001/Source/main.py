# -*- coding: utf-8 -*-

from kivy.app import App
from kivy.uix.screenmanager import ScreenManager
from kivy.lang import Builder
import os
import shutil

import Global
import CommandServer
import DisplayServer
import ScreenHandler
import SessionHandler
import SongHandler
import AjaxInterface
import StateMachine
import OsInterface
import TestRunner

Builder.load_file(u'data/main.kv')


class MainWindow(ScreenManager):

    def __init__(self):
        super(MainWindow, self).__init__()
        self.init_instances()
        self.app_quit = App.stop

    def init_instances(self):
        self.init_dirs()
        Global.Instances.main_window = self
        Global.Instances.os_interface = OsInterface.OsInterface()
        Global.Instances.screen_handler = ScreenHandler.ScreenHandler()
        Global.Instances.session_handler = SessionHandler.SessionHandler()
        Global.Instances.song_handler = SongHandler.SongHandler()
        Global.Instances.state_machine = StateMachine.StateMachine()
        Global.Instances.ajax_interface = AjaxInterface.AjaxInterface()
        Global.Instances.command_server = CommandServer.Server()
        Global.Instances.display_server = DisplayServer.Server()
        if Global.CONST.TEST:
            Global.Instances.test_runner = TestRunner.TestRunner()

        Global.Instances.state_machine.start()
        Global.Instances.screen_handler.start()
        Global.Instances.display_server.start()
        Global.Instances.command_server.start()
        Global.Instances.os_interface.start()
        if Global.CONST.TEST:
            Global.Instances.test_runner.start()

    def init_dirs(self):
        if not os.path.exists(Global.CONST.USER_DATA_DIR):
            os.makedirs(Global.CONST.USER_DATA_DIR)
        Global.CONST.DATA_DIR = os.path.join(Global.CONST.USER_DATA_DIR, 'data')
        if not os.path.exists(Global.CONST.DATA_DIR):
            os.makedirs(Global.CONST.DATA_DIR)
        Global.CONST.SETTINGS_DIR = os.path.join(Global.CONST.USER_DATA_DIR, 'settings')
        if not os.path.exists(Global.CONST.SETTINGS_DIR):
            os.makedirs(Global.CONST.SETTINGS_DIR)
        Global.CONST.SONG_DB_PATH = os.path.join(Global.CONST.DATA_DIR, 'Liedbearbeitungen.csv')
        if not os.path.exists(Global.CONST.SONG_DB_PATH):
            shutil.copy('data/Liedbearbeitungen.csv', Global.CONST.SONG_DB_PATH)
        Global.CONST.INSTRUMENTS_DB_PATH = os.path.join(Global.CONST.DATA_DIR, 'Instrumente.csv')
        if not os.path.exists(Global.CONST.INSTRUMENTS_DB_PATH):
            shutil.copy('data/Instrumente.csv', Global.CONST.INSTRUMENTS_DB_PATH)
        # todo password file


class MediaDisplayServerApp(App):
    def build(self):
        return MainWindow()

    def on_start(self):
        pass

    def on_stop(self):
        # todo check if all instances will be stopped
        Global.Instances.state_machine.stop()
        Global.Instances.screen_handler.stop()
        Global.Instances.display_server.stop()
        Global.Instances.command_server.stop()
        Global.Instances.os_interface.stop()
        if Global.CONST.TEST:
            Global.Instances.test_runner.stop()

    def on_pause(self):
        # Here you can save data if needed
        return True

    def on_resume(self):
        # Here you can check if any data needs replacing (usually nothing)
        pass


if __name__ == u'__main__':
    App = MediaDisplayServerApp()
    # if os.path.exists(os.path.join(App.user_data_dir, 'testenviron')):
    # Window.size = (1280, 800) # todo set fullscreen
    App.run()
