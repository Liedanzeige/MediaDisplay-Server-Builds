[-------------------------------------------------------------------------------------------------]
[                                                                                                 ]
[                                           TEST RESULT                                           ]
[                                                                                                 ]
[-------------------------------------------------------------------------------------------------]
[2016-10-22 22:53:52.682] [   ~] Commit: f2ed74a1871a7dab26604e73e4d97f12edc62847
[2016-10-22 22:53:52.682] [   ~] Maybe not everything is committed and pushed
[2016-10-22 22:53:52.682] [----] Test FSM IR Commands Test started
[2016-10-22 22:54:52.682] [    ] Screen "Zeige Uhr" is correct
[2016-10-22 22:54:52.682] [   ~] IR Command : 1
[2016-10-22 22:54:53.182] [   ~] IR Command : 2
[2016-10-22 22:54:53.683] [   ~] IR Command : 3
[2016-10-22 22:54:54.184] [   ~] IR Command : +
[2016-10-22 22:54:54.684] [   ~] IR Command : 4
[2016-10-22 22:54:55.185] [    ] Labels "Number" text is correct
[2016-10-22 22:54:55.185] [    ] Screen "Zeige Zahl" is correct
[2016-10-22 22:55:02.185] [    ] Screen "Zeige Lied" is correct
[2016-10-22 22:55:02.185] [    ] Labels "SongNumber" text is correct
[2016-10-22 22:55:02.185] [    ] Labels "BookTitle" text is correct
[2016-10-22 22:55:02.185] [   ~] IR Command : 1
[2016-10-22 22:55:02.686] [   ~] IR Command : 2
[2016-10-22 22:55:03.186] [   ~] IR Command : 3
[2016-10-22 22:55:03.687] [   ~] IR Command : 1
[2016-10-22 22:55:33.687] [   ~] IR Command : c
[2016-10-22 22:55:34.187] [    ] Screen "Zeige Uhr" is correct
[2016-10-22 22:55:44.188] [   ~] IR Command : set clock
[2016-10-22 22:55:44.688] [    ] Screen "Stelle Stunde" is correct
[2016-10-22 22:55:54.688] [   ~] IR Command : set clock
[2016-10-22 22:55:55.189] [    ] Screen "Stelle Minute" is correct
[2016-10-22 22:56:55.195] [   ~] IR Command : end
[2016-10-22 22:56:55.696] [    ] Screen "Bestätige Beenden" is correct
[2016-10-22 22:57:05.696] [   ~] IR Command : 0
[2016-10-22 22:57:05.796] [    ] Screen "Beenden" is correct
[2016-10-22 22:57:05.796] [  OK] Test finished
[2016-10-22 22:57:06.281] [  OK] 1 Tests finished 1 succeeded, 0 failed; time: 193.664s
[-------------------------------------------------------------------------------------------------]
[                         SSSS   U   U   CCC   CCC  EEEEE   SSSS    SSSS                          ]
[                        SS      U   U  CC    CC    E      SS      SS                             ]
[                         SSSS   U   U  C     C     EEEEE   SSSS    SSSS                          ]
[                            SS  U   U  CC    CC    E          SS      SS                         ]
[                         SSSS    UUU    CCC   CCC  EEEEE   SSSS    SSSS                          ]
[-------------------------------------------------------------------------------------------------]
