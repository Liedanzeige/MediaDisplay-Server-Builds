# -*- coding: utf-8 -*-

from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
import threading
import urllib2
import urlparse

import Global


class DisplayRequestHandler(SimpleHTTPRequestHandler):

    def generate_html(self):
        html = u'''<!DOCTYPE html>
<head>
    <title>MediaDisplay-Display</title>
    <meta http-equiv="refresh" content="3">
</head>
<html>
<body>
    %s
</body>
</html>'''
        if Global.Instances.state_machine.state == Global.CONST.SM.STATE_START:
            html =  html % u'<h1> Starten </h1>'
        # elif ...:
        else:
            html = html % u'<h1>%s</h1>' % Global.CONST.SM.STATE_DICT.get(
                Global.Instances.state_machine.state, u'Unbekannt')
        return html

    def do_GET(self):
        """Serve a GET request."""

        decoded_url = urllib2.unquote(self.path).decode('utf-8')

        if decoded_url in ('', '/'):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(self.generate_html())
        else:
            self.send_error(404, u'Nothing matches the given URI')
            self.wfile.write('Wolltest du mich testen oder was?')


class Server:
    def __init__(self):
        self.server = HTTPServer(("", Global.CONST.DS.PORT), DisplayRequestHandler)
        # import ssl  # todo https
        # self.server.socket = ssl.wrap_socket(self.server.socket, certfile='static/server.pem', server_side=True)
        self.server_thread = threading.Thread(target=self.server.serve_forever, args=(), name='DisplayServer')
        self.server_thread.daemon = True

    def start(self):  # todo restart
        self.server_thread.start()

    def stop(self):
        self.server.shutdown()
