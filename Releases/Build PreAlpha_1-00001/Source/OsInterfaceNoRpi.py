# -*- coding: utf-8 -*-

commands = list()


class OsInterface(object):

    def __init__(self):
        self.__commands = list()

    def init_led(self):
        pass

    def get_ir_commands(self):
        temp_commands = self.__commands[:]
        self.__commands = list()
        return temp_commands

    def add_ir_command(self, command):
        self.__commands.append(command)

    def set_led(self, color):
        pass

    def set_led_time(self, color, time):
        pass

    @staticmethod
    def hour_plus():
        pass

    @staticmethod
    def hour_minus():
        pass

    @staticmethod
    def minute_plus():
        pass

    @staticmethod
    def minute_minus():
        pass

    def get_temp(self, sensor):
        return None

    def is_running(self):
        return True

    def start(self):
        pass

    def stop(self):
        pass
