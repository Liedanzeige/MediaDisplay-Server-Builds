# -*- coding: utf-8 -*-

import json
import inspect
import os


class CONST(object):
    VERSION = 'PreAlpha_1'
    BUILD = '00001'
    COMMIT = '5ce47798049f9eb77e795942c49bf0d0f6e810bb'
    SCRIPT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    USER_DATA_DIR = os.path.join(SCRIPT_DIR, '..', 'user_data')
    try:
        if os.uname()[4].startswith("arm"):
            ON_RPI = True
        else:
            ON_RPI = False
    except:
        ON_RPI = False
    TEST = os.path.exists('TestExecutionQueue.txt')

    class OSI(object):  # os dependent functions (especially hardware stuff)
        LIRC_CONFIG_FILE_PATH = os.path.abspath('data/lircrc')
        RED_LED = 36
        GREEN_LED = 38
        BLUE_LED = 40

        class COLORS(object):
            RED = [100, 0, 0]
            GREEN = [0, 100, 0]
            BLUE = [0, 0, 100]
            WHITE = [100, 100, 100]
            BLACK = [0, 0, 0]
            OFF = BLACK

    class WS(object):  # web server
        PORT = 58060

    class DS(object):  # display server
        PORT = 48080

    class CS(object):  # command server
        PORT = 8080

    class AI(object):  # ajax interface
        ERROR0 = json.dumps({u'ok': False, u'errnr': 0, u'error': u'Invalid name or password.'})
        ERROR1 = json.dumps({u'ok': False, u'errnr': 1, u'error': u'User already logged in.'})
        ERROR2 = json.dumps({u'ok': False, u'errnr': 2, u'error': u'Not logged in.'})
        ERROR3 = json.dumps({u'ok': False, u'errnr': 3, u'error': u'No such command.'})
        ERROR4 = json.dumps({u'ok': False, u'errnr': 4, u'error': u'Book not found.'})
        ERROR5 = json.dumps({u'ok': False, u'errnr': 5, u'error': u'Song not found.'})
        ERROR6 = json.dumps({u'ok': False, u'errnr': 6, u'error': u'No song.'})

    class SH(object):  # session handler
        SESSION_LIFETIME = 300.0  # in seconds

    class SM(object):
        STATE_QUIT = -3
        STATE_CONFIRM_QUIT = -2
        STATE_START = -1
        STATE_IDLE = 0
        STATE_SONG = 1
        STATE_NUMBER = 2
        STATE_SET_HOUR = 3
        STATE_SET_MINUTE = 4
        STATE_DICT = {
            -3: u'Beenden',
            -2: u'Bestätige Beenden',
            -1: u'Start',
            0: u'Zeige Uhr',
            1: u'Zeige Lied',
            2: u'Zeige Zahl',
            3: u'Stelle Stunde',
            4: u'Stelle Minute',
        }
        CMD_SYS = 0
        CMD_IR = 1
        CMD_NET = 2

        class IR_CMD(object):
            N1 = '1'
            N2 = '2'
            N3 = '3'
            N4 = '4'
            N5 = '5'
            N6 = '6'
            N7 = '7'
            N8 = '8'
            N9 = '9'
            N0 = '0'
            N4_LIST = [N1,
                       N2,
                       N3,
                       N4
                       ]
            N_LIST = [N1,
                      N2,
                      N3,
                      N4,
                      N5,
                      N6,
                      N7,
                      N8,
                      N9,
                      N0
                      ]
            SET_CLOCK = 'set clock'
            PLUS = '+'
            MINUS = '-'
            ABORT = 'c'
            END = 'end'
            CONFIRM_QUIT = '0'
            CONFIRM_SHUTDOWN = '1'
            DELIMITER = '+'

        class TIME(object):
            CHANGE_1_0 = 240.0
            CHANGE_2_1 = 5.0
            CHANGE_3_0 = 30.0
            CHANGE_4_0 = 30.0


class Instances(object):
    main_window = None
    screen_handler = None
    session_handler = None
    song_handler = None
    ajax_interface = None
    command_server = None
    display_server = None
    state_machine = None
    os_interface = None
    test_runner = None
