# -*- coding: utf-8 -*-

from kivy.graphics import Color
from kivy.graphics import Rectangle
import time
import threading
from datetime import datetime as dt

import Global


class ScreenHandler(object):
    voice_dict = {'1': 'Sopran',
                  '2': 'Alt',
                  '3': 'Tenor',
                  '4': 'Bass'
                  }

    color_dict = {'Sopran': Color(1, 0, 0, 1),
                  'Alt': Color(1, 1, 0, 1),
                  'Tenor': Color(0, 1, 0, 1),
                  'Bass': Color(0, 0, 1, 1),
                  'Black': Color(1, 0, 0, 0)  # todo replace all colors in class
                  }

    def __init__(self):
        self.__run = False
        self.__number = ''
        self.__current_song = None
        self.__now_time = dt.now().strftime('%H:%M:%S')
        self.__gui_inited = False
        self.__gui_init_finish = False
        self.__thread = None

    @classmethod
    def fit_font_size(cls, widget):
        if widget.text:
            while widget.texture_size[0] < widget.size[0] and widget.texture_size[1] < widget.size[1]:
                time.sleep(0.001)
                widget.font_size += 1
            while widget.texture_size[0] > widget.size[0] or widget.texture_size[1] > widget.size[1]:
                time.sleep(0.001)
                widget.font_size -= 1

    def init_gui(self):
        if not self.__gui_inited:
            wait_time = 0.2
            self.__gui_inited = True
            # if on test wait for several seconds to resize window
            if Global.CONST.TEST:
                pass
                # time.sleep(10)
            # todo fix screen size
            from kivy.config import Config
            Config.set('graphics', 'resizable', False)  # does not work

            # call all pages and fit sizes of all labels
            # -3
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_QUIT]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['LabelQuit']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # -2
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_CONFIRM_QUIT]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['LabelConfirmQuit']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # -1
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_START]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['LabelStart']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # 0
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_IDLE]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['Clock']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # 1
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SONG]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['BookTitle']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['SongNumber']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['Sopran']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['Alt']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['Tenor']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['Bass']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['SongScreenClock']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # 2
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_NUMBER]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['Number']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # 3
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SET_HOUR]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['ClockSetHour']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            # 4
            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SET_MINUTE]
            time.sleep(wait_time)
            label = Global.Instances.main_window.ids['ClockSetMinute']
            ScreenHandler.fit_font_size(label)
            time.sleep(wait_time)

            Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_START]

            time.sleep(10 * wait_time)

            self.__gui_init_finish = True

    def __update_song_screen(self, data):
        book = data['book']
        number = data['number']
        sopran = data['sopran']
        alt = data['alt']
        tenor = data['tenor']
        bass = data['bass']

        label_book = Global.Instances.main_window.ids['BookTitle']
        label_number = Global.Instances.main_window.ids['SongNumber']
        label_sopran = Global.Instances.main_window.ids['Sopran']
        label_alt = Global.Instances.main_window.ids['Alt']
        label_tenor = Global.Instances.main_window.ids['Tenor']
        label_bass = Global.Instances.main_window.ids['Bass']

        label_book.text = book
        time.sleep(0.1)
        ScreenHandler.fit_font_size(label_book)
        time.sleep(0.1)

        label_number.text = number
        time.sleep(0.1)
        ScreenHandler.fit_font_size(label_number)
        time.sleep(0.1)

        if sopran == '1':
            label_sopran.text = 'Sopran'
            color = ScreenHandler.color_dict['Sopran']
        else:
            label_sopran.text = ''
            color = Color(1, 0, 0, 0)
        label_sopran.canvas.before.clear()
        label_sopran.canvas.before.add(color)
        label_sopran.canvas.before.add(Rectangle(pos=label_sopran.pos, size=label_sopran.size))
        # ScreenHandler.fit_font_size(label_sopran)
        if alt == '1':
            label_alt.text = 'Alt'
            color = ScreenHandler.color_dict['Alt']
        else:
            label_alt.text = ''
            color = Color(1, 0, 0, 0)
        label_alt.canvas.before.clear()
        label_alt.canvas.before.add(color)
        label_alt.canvas.before.add(Rectangle(pos=label_alt.pos, size=label_alt.size))
        # ScreenHandler.fit_font_size(label_alt)

        if tenor == '1':
            label_tenor.text = 'Tenor'
            color = ScreenHandler.color_dict['Tenor']
        else:
            label_tenor.text = ''
            color = Color(0, 1, 0, 0)
        label_tenor.canvas.before.clear()
        label_tenor.canvas.before.add(color)
        label_tenor.canvas.before.add(Rectangle(pos=label_tenor.pos, size=label_tenor.size))
        # ScreenHandler.fit_font_size(label_tenor)

        if bass == '1':
            label_bass.text = 'Bass'
            color = ScreenHandler.color_dict['Bass']
        else:
            label_bass.text = ''
            color = Color(1, 0, 0, 0)
        label_bass.canvas.before.clear()
        label_bass.canvas.before.add(color)
        label_bass.canvas.before.add(Rectangle(pos=label_bass.pos, size=label_bass.size))
        # ScreenHandler.fit_font_size(label_bass)

    def add_number(self, number):
        if number in Global.CONST.SM.IR_CMD.N_LIST:
            self.__number += number
        else:
            self.__number += '+'
        label = Global.Instances.main_window.ids['Number']
        label.text = self.__number
        ScreenHandler.fit_font_size(label)

    def toggle_solo(self, voice):
        if Global.Instances.main_window.current == Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SONG]:
            voice = ScreenHandler.voice_dict[voice]
            label = Global.Instances.main_window.ids[voice]
            if label.text == '':
                label.text = voice
                color = ScreenHandler.color_dict[voice]
            else:
                label.text = ''
                color = Color(1, 0, 0, 0)
            label.canvas.before.clear()
            label.canvas.before.add(color)
            label.canvas.before.add(Rectangle(pos=label.pos, size=label.size))
            # ScreenHandler.fit_font_size(label)

    def set_page(self, target_state, gui_params):
        Global.Instances.main_window.current = Global.CONST.SM.STATE_DICT.get(target_state, 'Zeige Uhr')
        if target_state == Global.CONST.SM.STATE_IDLE:
            label = Global.Instances.main_window.ids['Clock']
            now_time = dt.now().strftime('%H:%M:%S')
            label.text = now_time
            time.sleep(0.1)
            ScreenHandler.fit_font_size(label)
        elif target_state == Global.CONST.SM.STATE_SONG:
            # todo get song data
            if gui_params is None:
                temp = self.__number.split('+')
                number = temp[0]
                if len(temp) >= 2:
                    book = temp[1]
                else:
                    book = ''
                data = dict()
                data['book'] = book
                data['number'] = number
                data['sopran'] = '0'
                data['alt'] = '0'
                data['tenor'] = '0'
                data['bass'] = '0'
                self.__update_song_screen(data)
            else:
                pass  # todo

            label = Global.Instances.main_window.ids['SongScreenClock']  # todo make global variable for clock id
            now_time = dt.now().strftime('%H:%M:%S')
            label.text = now_time
            ScreenHandler.fit_font_size(label)
        elif target_state == Global.CONST.SM.STATE_NUMBER:
            self.__number = gui_params
            label = Global.Instances.main_window.ids['Number']
            label.text = self.__number
            ScreenHandler.fit_font_size(label)
        elif target_state == Global.CONST.SM.STATE_SET_HOUR:
            label = Global.Instances.main_window.ids['ClockSetHour']
            now_time = dt.now().strftime('%H:%M:%S')
            label.text = now_time
            ScreenHandler.fit_font_size(label)
        elif target_state == Global.CONST.SM.STATE_SET_MINUTE:
            label = Global.Instances.main_window.ids['ClockSetMinute']
            now_time = dt.now().strftime('%H:%M:%S')
            label.text = now_time
            ScreenHandler.fit_font_size(label)

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'ScreenHandler')
            self.__thread.start()

    def stop(self):
        self.__run = False

    def __update(self):
        ds = 0.5
        ds = min(ds, 0.95)
        if self.__gui_init_finish:
            # clock
            if Global.Instances.main_window.current == Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_IDLE]:
                label = Global.Instances.main_window.ids['Clock']
                now_time = dt.now().strftime('%H:%M:%S')
                if now_time != self.__now_time:
                    label.text = now_time
                self.__now_time = now_time
            # song
            elif Global.Instances.main_window.current == Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SONG]:
                label = Global.Instances.main_window.ids['SongScreenClock']
                now_time = dt.now().strftime('%H:%M:%S')
                if now_time != self.__now_time:
                    label.text = now_time
                self.__now_time = now_time
            # set hour
            elif Global.Instances.main_window.current == Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SET_HOUR]:
                label = Global.Instances.main_window.ids['ClockSetHour']
                now_dt = dt.now()
                now_time = now_dt.strftime('%H:%M:%S')
                even_sec = now_dt.second % 2
                micro_sec = now_dt.microsecond
                if (even_sec and micro_sec > ds * 1000000) or (not even_sec and micro_sec < 1000000 - ds * 1000000):
                    now_time = '--' + now_time[2:]
                if now_time != self.__now_time:
                    label.text = now_time
                self.__now_time = now_time
            # set minute
            elif Global.Instances.main_window.current == Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_SET_MINUTE]:
                label = Global.Instances.main_window.ids['ClockSetMinute']
                now_dt = dt.now()
                now_time = now_dt.strftime('%H:%M:%S')
                even_sec = now_dt.second % 2
                micro_sec = now_dt.microsecond
                if (even_sec and micro_sec > ds * 1000000) or (not even_sec and micro_sec < 1000000 - ds * 1000000):
                    now_time = now_time[:3] + '--' + now_time[5:]
                if now_time != self.__now_time:
                    label.text = now_time
                self.__now_time = now_time

        pass  # todo

    def __loop(self):
        while self.__run:
            self.__update()
            time.sleep(0.001)
