# -*- coding: utf-8 -*-

import lirc
import RPi.GPIO as GPIO
import threading
import time

import Global

# sockid = lirc.init("irremote", Global.CONST.OSI.LIRC_CONFIG_FILE_PATH, blocking=False)

class OsInterface(object):

    def __init__(self):
        self.__run = False
        self.__thread = None
        self.__led_set = False
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(Global.CONST.OSI.RED_LED, GPIO.OUT)
        GPIO.setup(Global.CONST.OSI.GREEN_LED, GPIO.OUT)
        GPIO.setup(Global.CONST.OSI.BLUE_LED, GPIO.OUT)
        self.red_led = GPIO.PWM(Global.CONST.OSI.RED_LED, 1000)
        self.green_led = GPIO.PWM(Global.CONST.OSI.GREEN_LED, 1000)
        self.blue_led = GPIO.PWM(Global.CONST.OSI.BLUE_LED, 1000)
        self.red_led.start(0)
        self.green_led.start(0)
        self.blue_led.start(0)
        self.__commands = list()

    def init_led(self):
        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)

        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.green_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.green_led.ChangeDutyCycle(i)

        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.red_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.red_led.ChangeDutyCycle(i)
        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
            self.green_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
            self.green_led.ChangeDutyCycle(i)
        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
            self.red_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
            self.red_led.ChangeDutyCycle(i)
        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.red_led.ChangeDutyCycle(i)
            self.green_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.red_led.ChangeDutyCycle(i)
            self.green_led.ChangeDutyCycle(i)
        for i in range(0, 100, 1):
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
            self.green_led.ChangeDutyCycle(i)
            self.red_led.ChangeDutyCycle(i)
        time.sleep(3)
        for i in range(0, 100, 1)[::-1]:
            time.sleep(0.01)
            self.blue_led.ChangeDutyCycle(i)
            self.green_led.ChangeDutyCycle(i)
            self.red_led.ChangeDutyCycle(i)

    def get_ir_commands(self):
        # return [lirc.nextcode()[0]]
        temp_commands = self.__commands[:]
        self.__commands = list()
        return temp_commands

    def add_ir_command(self, command):
        self.__commands.append(command)

    def set_led(self, color):
        if color is None:
            self.__led_set = False
            return
        red = min(max(int(color[0]), 0), 100)
        green = min(max(int(color[0]), 0), 100)
        blue = min(max(int(color[0]), 0), 100)
        self.red_led.ChangeDutyCycle(red)
        self.green_led.ChangeDutyCycle(green)
        self.blue_led.ChangeDutyCycle(blue)

    def set_led_time(self, color, time):
        pass

    @staticmethod
    def hour_plus():
        pass

    @staticmethod
    def hour_minus():
        pass

    @staticmethod
    def minute_plus():
        pass

    @staticmethod
    def minute_minus():
        pass

    def get_temp(self, sensor):
        return None

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'OsInterface')
            self.__thread.start()

    def stop(self):
        self.__run = False

    def __update(self):
        pass

    def __loop(self):
        while self.__run:
            time.sleep(0.001)
        lirc.deinit()
        GPIO.cleanup()
