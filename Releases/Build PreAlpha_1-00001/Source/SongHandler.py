# -*- coding: utf-8 -*-

import csv

import Global


class SongHandler(object):

    def __init__(self):
        self.indexes = {}
        self.read_song_db()

    def read_song_db(self):
        indexes = {}
        instruments = {}

        csv_file = open(Global.CONST.SONG_DB_PATH, 'rb')
        csv_reader = csv.reader(csv_file, delimiter=';')
        for row in csv_reader:
            if len(row) == 13:
                song_id = row[0]
                title = row[1].decode('utf-8')
                book = row[8].decode('utf-8')
                number = row[9].decode('utf-8')
                if book not in ('', '---'):
                    if book not in indexes:
                        indexes[book] = {}
                    if number not in book:
                        indexes[book][number] = {'title': title, 'song_id': song_id, 'instruments': []}
                    else:
                        print 'Error', book, number, song_id, indexes[book][number]['song_id']
            else:
                print 'error', row

        csv_file = open(Global.CONST.INSTRUMENTS_DB_PATH, 'rb')
        csv_reader = csv.reader(csv_file, delimiter=';')
        for row in csv_reader:
            if not len(row) == 4:
                print 'eeerror', row
            else:
                song_id = row[1]
                instrument = row[2].decode('utf-8')
                if song_id not in instruments:
                    instruments[song_id] = []
                instruments[song_id].append(instrument)

        for book, book_indexes in indexes.iteritems():
            for song, song_data in book_indexes.iteritems():
                song_data_song_id = song_data['song_id']
                if song_data_song_id in instruments.keys():
                    indexes[book][song]['instruments'].extend(instruments[song_data_song_id])

        self.indexes = indexes

    def has_book(self, book_title):
        return book_title in self.indexes

    def has_song(self, book, number):
        return book in self.indexes and number in self.indexes[book]

    def get_song_name(self, book, number):
        try:
            return self.indexes[book][number]['title']
        except:
            return u'%s_%s' % (book, number)
