# -*- coding: utf-8 -*-

import Global

if Global.CONST.ON_RPI:
    from OsInterfaceRaspberry import OsInterface
else:
    from OsInterfaceNoRpi import OsInterface
