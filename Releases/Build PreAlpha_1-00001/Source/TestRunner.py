# -*- coding: utf-8 -*-

import time
from datetime import datetime as dt
import codecs
import threading
import os
from random import randint

import Global


class TestRunner(object):

    def __init__(self):
        self.thread = threading.Thread(target=self.run_tests, name="TestRunner")
        self.test_result = None

    def check_expected_gui_screen(self, expected_screen):
        screen = Global.Instances.main_window.current
        if screen == expected_screen:
            self.test_result.pass_('Screen "%s" is correct' % screen)
        else:
            self.test_result.fail('Screen should be "%s" but was "%s"' %
                                  (expected_screen, screen))

    def check_expected_label_text(self, label_id, expected_text):
        label = Global.Instances.main_window.ids[label_id]
        if label.text == expected_text:
            self.test_result.pass_('Labels "%s" text is correct' % label_id)
        else:
            self.test_result.fail('Labels "%s" text should be "%s" but was "%s"' %
                                  (label_id, expected_text, label.text))

    def send_ir_command(self, ir_command):
        Global.Instances.os_interface.add_ir_command(ir_command)
        self.test_result.message(u'IR Command : %s' % ir_command)

    def run_tests(self):
        self.test_result = TestResult()
        self.test_result.message('Commit: %s' % get_commit())
        self.test_result.message('Maybe not everything is committed and pushed')
        with open('TestExecutionQueue.txt', 'r') as test_queue_file:
            test_queue = test_queue_file.readlines()
        for test in test_queue:
            test_func_str = test.strip()
            if hasattr(self, test_func_str):
                test_func = getattr(self, test_func_str)
                try:
                    test_func()
                except Exception as e:
                    self.test_result.fail('Test Failed with message: %s' % str(e))
                    self.test_result.finish_test()

    def stop_test(self):
        self.test_result.start_test('Stop Test')
        self.send_ir_command('c')
        time.sleep(0.5)
        self.send_ir_command('end')
        time.sleep(0.5)
        self.check_expected_gui_screen(u'Bestätige Beenden')
        time.sleep(10)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_gui_screen('Beenden')
        self.test_result.finish_test()

    def ir_large_input_test(self):
        self.test_result.start_test('IR Large Input Test')

        time.sleep(60)
        self.check_expected_gui_screen('Zeige Uhr')
        # test song input
        song = ''
        book = ''
        third = ''
        song_list = [str(randint(0, 9)) for i in range(100)]
        book_list = [str(randint(0, 9)) for i in range(100)]
        third_list = [str(randint(0, 9)) for i in range(100)]

        for i in song_list:
            song += i
            self.send_ir_command(i)
            time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in book_list:
            book += i
            self.send_ir_command(i)
            time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in third_list:
            third += i
            self.send_ir_command(i)
            time.sleep(0.5)

        self.check_expected_label_text('Number', song + '+' + book + '+' + third)
        self.check_expected_gui_screen(u'Zeige Zahl')
        time.sleep(7)
        self.check_expected_gui_screen(u'Zeige Lied')
        self.check_expected_label_text('SongNumber', song)
        self.check_expected_label_text('BookTitle', book)

        time.sleep(30)
        self.send_ir_command('c')
        time.sleep(0.5)
        self.check_expected_gui_screen(u'Zeige Uhr')

        self.test_result.finish_test()

    def fsm_ir_commands_test(self):
        """
        This is to test the fsm with simulated ir commands
        :return:
        """
        # wait until the program has started
        self.test_result.start_test('FSM IR Commands Test')
        time.sleep(60)
        self.check_expected_gui_screen('Zeige Uhr')
        # test song input
        self.send_ir_command('1')
        time.sleep(0.5)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.send_ir_command('3')
        time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '123+4')
        self.check_expected_gui_screen(u'Zeige Zahl')
        time.sleep(7)
        self.check_expected_gui_screen(u'Zeige Lied')
        self.check_expected_label_text('SongNumber', '123')
        self.check_expected_label_text('BookTitle', '4')

        # test change solos
        self.send_ir_command('1')
        time.sleep(0.5)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.send_ir_command('3')
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(30)
        self.send_ir_command('c')
        time.sleep(0.5)
        self.check_expected_gui_screen(u'Zeige Uhr')
        time.sleep(10)

        # test set clock
        self.send_ir_command('set clock')
        time.sleep(0.5)
        self.check_expected_gui_screen(u'Stelle Stunde')
        time.sleep(10)
        self.send_ir_command('set clock')
        time.sleep(0.5)
        self.check_expected_gui_screen(u'Stelle Minute')
        time.sleep(60)

        # test quit
        self.send_ir_command('end')
        time.sleep(0.5)
        self.check_expected_gui_screen(u'Bestätige Beenden')
        time.sleep(10)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_gui_screen('Beenden')
        self.test_result.finish_test()

    def is_running(self):
        self.thread.is_alive()

    def start(self):
        self.thread.start()

    def stop(self):
        self.test_result.finish('TestResult.txt')


def get_time_str():
    return str(dt.now())[:23]


def get_commit():
    return os.popen('git rev-parse HEAD').read().strip()


class TestResult(object):
    """
    test verdicts can be 'Success' or 'Fail'
    """

    file_end_fail = '''[-------------------------------------------------------------------------------------------------]
[                                     FFFFF    A     I  L                                         ]
[                                     F       A A    I  L                                         ]
[                                     FFFF   AAAAA   I  L                                         ]
[                                     F     A     A  I  LLLLL                                     ]
[-------------------------------------------------------------------------------------------------]
'''

    file_end_success = '''[-------------------------------------------------------------------------------------------------]
[                         SSSS   U   U   CCC   CCC  EEEEE   SSSS    SSSS                          ]
[                        SS      U   U  CC    CC    E      SS      SS                             ]
[                         SSSS   U   U  C     C     EEEEE   SSSS    SSSS                          ]
[                            SS  U   U  CC    CC    E          SS      SS                         ]
[                         SSSS    UUU    CCC   CCC  EEEEE   SSSS    SSSS                          ]
[-------------------------------------------------------------------------------------------------]
'''

    def __init__(self):
        """

        """
        self.start_time = dt.now()
        self.finish_time = None
        self.tests_number = 0
        self.test_failed = 0
        self.tests_succeded = 0
        self.current_test_verdict = 'Success'
        self.__log_text = '''[-------------------------------------------------------------------------------------------------]
[                                                                                                 ]
[                                           TEST RESULT                                           ]
[                                                                                                 ]
[-------------------------------------------------------------------------------------------------]
'''

    def get_duration(self):
        if not self.finish_time:
            return (dt.now() - self.start_time).total_seconds()
        else:
            return (self.finish_time - self.start_time).total_seconds()

    def __log(self, message, severity=''):
        self.__log_text += '[' + get_time_str() + '] [% 4s] ' % severity + message + '\n'

    def fail(self, message=''):
        self.current_test_verdict = 'Fail'
        self.__log(message, 'Fail')

    def pass_(self, message=''):
        self.__log(message)

    def message(self, message=''):
        self.__log(message, '~')

    def write(self, file_name):
        with codecs.open(file_name, 'w', 'utf-8') as result_file:
            result_file.write(self.__log_text)

    def start_test(self, test_name):
        self.current_test_verdict = None
        self.tests_number += 1
        self.__log('Test %s started' % test_name, '----')

    def finish_test(self):
        if self.current_test_verdict is None:
            self.tests_succeded += 1
            self.__log('Test finished', 'OK')
        else:
            self.test_failed += 1
            self.__log('Test finished', 'Fail')

    def finish(self, file_name):
        if self.finish_time:
            return
        self.finish_time = dt.now()
        if self.test_failed:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), 'Fail')
            self.__log_text += TestResult.file_end_fail
        else:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), 'OK')
            self.__log_text += TestResult.file_end_success
        self.write(file_name)
