# -*- coding: utf-8 -*-

import lirc
import RPi.GPIO as GPIO
import threading
import time
from datetime import datetime, timedelta
import os
import subprocess

import Global


class OsInterface(object):
    HOUR = timedelta(hours=1)
    MINUTE = timedelta(minutes=1)

    def __init__(self):
        self.__run = False
        self.__led_thread = None
        self.__util_thread = None
        self.red_led = None
        self.green_led = None
        self.blue_led = None
        self.__commands = list()
        self.__time_color = Global.CONST.OSI.COLORS.BLACK
        self.__color_time_counter = 0
        self.__static_color = None

    def init_hardware(self):

        sockid = lirc.init("ir_remote", Global.CONST.OSI.LIRC_CONFIG_FILE_PATH, blocking=False)

        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(Global.CONST.OSI.RED_LED, GPIO.OUT)
        GPIO.setup(Global.CONST.OSI.GREEN_LED, GPIO.OUT)
        GPIO.setup(Global.CONST.OSI.BLUE_LED, GPIO.OUT)

        self.red_led = GPIO.PWM(Global.CONST.OSI.RED_LED, 1000)
        self.green_led = GPIO.PWM(Global.CONST.OSI.GREEN_LED, 1000)
        self.blue_led = GPIO.PWM(Global.CONST.OSI.BLUE_LED, 1000)

        self.red_led.start(0)
        self.green_led.start(0)
        self.blue_led.start(0)

    def init_led(self):
        for color_name in dir(Global.CONST.OSI.COLORS):
            if not color_name.isupper():
                continue
            color = getattr(Global.CONST.OSI.COLORS, color_name)
            Global.Instances.os_interface.__write_led_color(color)
            time.sleep(3)
        Global.Instances.os_interface.__write_led_color(Global.CONST.OSI.COLORS.WHITE)

    def get_ir_commands(self):
        try:
            ir_command = lirc.nextcode()
            if ir_command:
                return [ir_command[0]]
            if Global.CONST.TEST:
                temp_commands = self.__commands[:]
                self.__commands = list()
                return temp_commands
        except:
            # todo log error message
            pass
        return []

    def add_ir_command(self, command):
        self.__commands.append(command)

    def __write_led_color(self, color):
        if not color:
            color = Global.CONST.OSI.COLORS.BLACK
        red = min(max(int(color[0]), 0), 100)
        green = min(max(int(color[1]), 0), 100)
        blue = min(max(int(color[2]), 0), 100)
        self.red_led.ChangeDutyCycle(red)
        self.green_led.ChangeDutyCycle(green)
        self.blue_led.ChangeDutyCycle(blue)

    def set_led(self, color):
        self.__static_color = color

    def set_led_time(self, color, time):
        self.__time_color = color
        self.__color_time_counter = int(time / Global.CONST.OSI.TIME_INCREMENT_LED)

    @staticmethod
    def hour_plus():
        now = datetime.now()
        set_time = now + OsInterface.HOUR
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))

    @staticmethod
    def hour_minus():
        now = datetime.now()
        set_time = now - OsInterface.HOUR
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))

    @staticmethod
    def minute_plus():
        now = datetime.now()
        set_time = now + OsInterface.MINUTE
        set_time -= timedelta(seconds=set_time.second, microseconds=set_time.microsecond)
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))

    @staticmethod
    def minute_minus():
        now = datetime.now()
        set_time = now - OsInterface.MINUTE
        set_time -= timedelta(seconds=set_time.second, microseconds=set_time.microsecond)
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))

    def get_temp(self, sensor):
        return None

    def __create_certificate(self):
        process = subprocess.Popen(["openssl", "req", "-x509", "-newkey", "rsa:4096",
                                    "-keyout", "/home/pi/user_data/cert/key.pem",  # todo variable path
                                    "-out", "/home/pi/user_data/cert/cert.pem", "-days", "365"],  # todo variable path
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   stdin=subprocess.PIPE)
        process.wait()

    def __check_certificate(self):
        return True  # todo

    @staticmethod
    def reboot():
        os.system('sudo shutdown -r now')

    @staticmethod
    def shutdown():
        os.system('sudo shutdown -h now')

    def is_running(self):
        if self.__led_thread is None or self.__util_thread is None:
            return False
        else:
            return self.__led_thread.is_alive() and self.__util_thread.is_alive()

    def start(self):
        if not self.is_running():
            self.__run = True
            self.__led_thread = threading.Thread(target=self.__loop_led, args=(), name=u'OsInterfaceLED')
            self.__led_thread.start()
            self.__util_thread = threading.Thread(target=self.__loop_util, args=(), name=u'OsInterfaceUtils')
            self.__util_thread.start()

    def stop(self):
        self.__run = False

    def __update_led(self):
        if self.__color_time_counter > 0:
            self.__color_time_counter -= 1
            self.__write_led_color(self.__time_color)
        elif self.__static_color:
            self.__write_led_color(self.__static_color)
        else:
            self.__write_led_color(Global.CONST.OSI.COLORS.BLACK)

    def __update_util(self):
        if not self.__check_certificate():
            self.__create_certificate()

    def __loop_led(self):
        while self.__run:
            self.__update_led()
            time.sleep(Global.CONST.OSI.TIME_INCREMENT_LED)
        lirc.deinit()
        GPIO.cleanup()

    def __loop_util(self):
        while self.__run:
            self.__update_util()
            time.sleep(Global.CONST.OSI.TIME_INCREMENT_UTIL)
