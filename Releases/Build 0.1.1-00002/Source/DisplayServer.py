# -*- coding: utf-8 -*-

from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
import threading
import urllib2
import ssl
import urlparse

import Global


class DisplayRequestHandler(SimpleHTTPRequestHandler):

    def generate_html(self):
        html = u'''<!DOCTYPE html>
<html>
    <head>
        <title>MediaDisplay-Display</title>
        <meta charset="utf-8"/>
        <meta http-equiv="refresh" content="3">
    </head>
    <body>
        %s
        <footer>
            <p>
                MediaDisplay-Server Display Server
            </p>
            <p>
                Version: ''' + Global.CONST.VERSION + u'''
            </p>
            <p>
                Build: ''' + Global.CONST.BUILD + u'''
            </p>
        </footer>
    </body>
</html>'''

        if Global.Instances.state_machine.state == Global.CONST.SM.STATE_START:
            html =  html % u'<h1> Starten </h1>'
        # elif ...:  todo
        elif Global.Instances.state_machine.state == Global.CONST.SM.STATE_SONG:
            current_song = Global.Instances.screen_handler.get_current_song()
            text = u'<h1> Zeige Lied </h1>\n\n'
            text += u'Liederbuch: %s<br>' % current_song['book']
            text += u'Titel: %s<br>' % current_song['title']
            text += u'<table style="padding: 10px;">'
            if current_song['Sopran'] == '1':
                text += u'<tr style="background-color:red; color:white;"><td style="padding: 10px;">Sopran</td></tr>'
            if current_song['Alt'] == '1':
                text += u'<tr style="background-color:yellow;"><td style="padding: 10px;">Alt</td></tr>'
            if current_song['Tenor'] == '1':
                text += u'<tr style="background-color:green;"><td style="padding: 10px;">Tenor</td></tr>'
            if current_song['Bass'] == '1':
                text += u'<tr style="background-color:blue; color:white;"><td style="padding: 10px;">Bass</td></tr>'
            text += u'</table>'
            html = html % text
        else:
            html = html % u'<h1>%s</h1>' % Global.CONST.SM.STATE_DICT.get(
                Global.Instances.state_machine.state, u'Unbekannt')
        return html

    def do_GET(self):
        """Serve a GET request."""

        decoded_url = urllib2.unquote(self.path).decode('utf-8')

        if decoded_url in ('', '/'):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(self.generate_html().encode('utf-8'))
        else:
            self.send_error(404, 'Nothing matches the given URI')
            self.wfile.write('Wolltest du mich testen oder was?')


class Server:
    def __init__(self):
        self.server = HTTPServer(("", Global.CONST.DS.PORT), DisplayRequestHandler)
        # todo https
        # self.server.socket = ssl.wrap_socket(self.server.socket, certfile='static/server.pem', server_side=True)
        self.__thread = None

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        return  # deactivated in this version
        if not self.is_running():
            self.__thread = threading.Thread(target=self.server.serve_forever, args=(), name='DisplayServer')
            self.__thread.start()

    def stop(self):
        if self.is_running():
            self.server.shutdown()
