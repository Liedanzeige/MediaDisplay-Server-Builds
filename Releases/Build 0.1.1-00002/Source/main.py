# -*- coding: utf-8 -*-

import os
import sys
from PySide import QtGui
from PySide import QtCore

import Global
import CommandServer
import DisplayServer
import WebServer
import ScreenHandler
import SessionHandler
import SongHandler
import AjaxInterface
import StateMachine
import OsInterface
import TestRunner
import Logger
import Watchdog
import MDSWidgets


class MainWindow(QtGui.QWidget):

    set_stack_index = QtCore.Signal(int)

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ids = dict()
        self.__current = 0
        self.stack = QtGui.QStackedLayout()
        self.set_stack_index.connect(self.__set_stack_index)
        self.init_gui()

    @property
    def current(self):
        return self.__current

    @current.setter
    def current(self, value):
        self.set_stack_index.emit(value)

    @QtCore.Slot(int)
    def __set_stack_index(self, index):
        self.stack.setCurrentIndex(index)
        self.__current = index

    def init_gui(self):

        # Start / 0
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('')
        label1.setStyleSheet("QLabel { background-color : black; color : white; }")
        label2 = MDSWidgets.StretchedLabel('Liedanzeige')
        label2.setStyleSheet("QLabel { background-color : black; color : white; }")
        self.ids['LabelStartLiedanzeige'] = label2
        label3 = MDSWidgets.StretchedLabel(u'   MediaDisplay-Server 0.1.1-00002   ')
        label3.setStyleSheet("QLabel { background-color : black; color : white; }")
        self.ids['LabelStartVersion'] = label3
        label4 = MDSWidgets.StretchedLabel(u'   \u00A9 2017 Liedanzeige Entwicklungsteam   ')
        label4.setStyleSheet("QLabel { background-color : black; color : white; }")
        self.ids['LabelStartCopyright'] = label4
        label5 = MDSWidgets.StretchedLabel('')
        label5.setStyleSheet("QLabel { background-color : black; color : white; }")
        grid.addWidget(label1, 1, 0, 1, 1)
        grid.addWidget(label2, 2, 0, 2, 1)
        grid.addWidget(label3, 4, 0, 1, 1)
        grid.addWidget(label4, 5, 0, 1, 1)
        grid.addWidget(label5, 6, 0, 1, 1)
        widget.setLayout(grid)

        self.stack.addWidget(widget)

        # Zeige Uhr / 1
        clock = MDSWidgets.AnalogClock()

        self.stack.addWidget(clock)

        # Zeige Lied / 2
        label1_1 = MDSWidgets.StretchedLabel('--')
        label1_1.setStyleSheet("QLabel { background-color : black; color : yellow; }")
        self.ids['BookTitle'] = label1_1
        label1_2 = MDSWidgets.StretchedLabel('---')
        label1_2.setStyleSheet("QLabel { background-color : black; color : white; }")
        self.ids['SongNumber'] = label1_2
        grid1_1 = QtGui.QGridLayout()
        grid1_1.setSpacing(0)
        grid1_1.setContentsMargins(0, 0, 0, 0)
        grid1_1.addWidget(label1_1, 0, 0, 1, 2)
        grid1_1.addWidget(label1_2, 0, 2, 1, 3)
        widget1_1 = QtGui.QWidget()
        widget1_1.setLayout(grid1_1)

        label2_1 = MDSWidgets.StretchedLabel('Sopran')
        label2_1.setStyleSheet("QLabel { background-color : red; color : white; }")
        self.ids['Sopran'] = label2_1
        label2_2 = MDSWidgets.StretchedLabel('Alt')
        label2_2.setStyleSheet("QLabel { background-color : yellow; color : black; }")
        self.ids['Alt'] = label2_2
        label2_3 = MDSWidgets.StretchedLabel('Tenor')
        label2_3.setStyleSheet("QLabel { background-color : green; color : black; }")
        self.ids['Tenor'] = label2_3
        label2_4 = MDSWidgets.StretchedLabel('Bass')
        label2_4.setStyleSheet("QLabel { background-color : blue; color : white; }")
        self.ids['Bass'] = label2_4
        grid2_1 = QtGui.QGridLayout()
        grid2_1.setSpacing(0)
        grid2_1.setContentsMargins(0, 0, 0, 0)
        grid2_1.addWidget(label2_1, 0, 0, 1, 1)
        grid2_1.addWidget(label2_2, 0, 1, 1, 1)
        grid2_1.addWidget(label2_3, 1, 0, 1, 1)
        grid2_1.addWidget(label2_4, 1, 1, 1, 1)
        widget2_1 = QtGui.QWidget()
        widget2_1.setLayout(grid2_1)

        label3_1 = MDSWidgets.DigitalClock('%H:%M')
        label3_1.setStyleSheet("QLabel { background-color : black; color : white; }")
        self.ids['SongScreenClock'] = label3_1
        grid3_1 = QtGui.QGridLayout()
        grid3_1.setSpacing(0)
        grid3_1.setContentsMargins(0, 0, 0, 0)
        grid3_1.addWidget(label3_1, 0, 0, 0, 0)
        widget3_1 = QtGui.QWidget()
        widget3_1.setLayout(grid3_1)

        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        grid.addWidget(widget1_1, 0, 0, 1, 2)
        grid.addWidget(widget2_1, 1, 0, 1, 1)
        grid.addWidget(widget3_1, 1, 1, 1, 1)
        widget = QtGui.QWidget()
        widget.setLayout(grid)

        self.stack.addWidget(widget)

        # Zeige Nummer / 3
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('---')
        label1.setStyleSheet("QLabel { background-color : black; color : white; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['Number'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Bluescreen / 4  # todo test
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel(u' Oops... ')
        label1.setStyleSheet("QLabel { background-color : blue; color : white; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['Oops'] = label1
        label2 = MDSWidgets.StretchedLabel(u' Das h\u00e4tte nicht passieren d\u00fcrfen ')
        label2.setStyleSheet("QLabel { background-color : blue; color : white; }")
        grid.addWidget(label2, 0, 2, 1, 1)
        self.ids['BluescreenLabel'] = label2
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Standby / 5
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('Standby')
        label1.setStyleSheet("QLabel { background-color : black; color : white; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['StandbyLabel'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Beenden / 6
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('Programm wird beendet...')
        label1.setStyleSheet("QLabel { background-color : black; color : white; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelQuit'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Bestätige Beenden / 7
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel(u'M\u00f6chten sie das Programm wirklich beenden?')  # todo show shutdown options
        label1.setStyleSheet("QLabel { background-color : black; color : white; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelConfirmQuit'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        #
        self.current = Global.CONST.SM.STATE_START

        self.setLayout(self.stack)

        self.stack.setCurrentIndex(0)
        # self.setGeometry(300, 300, 700, 500)
        self.setWindowTitle('Liedanzeige')

        self.showFullScreen()


class MediaDisplayServerApp(QtGui.QApplication):

    def __init__(self, *args, **kwargs):
        super(MediaDisplayServerApp, self).__init__(*args, **kwargs)
        Global.Instances.app = self
        self.font_database = QtGui.QFontDatabase()
        self.load_fonts()
        Global.Instances.main_window = MainWindow()
        # noinspection PyUnresolvedReferences
        self.aboutToQuit.connect(self.on_stop)

    def load_fonts(self):
        for dirname, dirs, files in os.walk(Global.CONST.FONTS_DIR):
            for file in files:
                if file.lower().endswith('.ttf'):
                    font_path = os.path.join(dirname, file)
                    self.font_database.addApplicationFont(font_path)

    def on_stop(self, *args):
        Global.Instances.watchdog.allow_stop = True
        Global.Instances.state_machine.stop()
        Global.Instances.screen_handler.stop()
        Global.Instances.display_server.stop()
        Global.Instances.web_server.stop()
        Global.Instances.command_server.stop()
        Global.Instances.os_interface.stop()
        try:
            Global.Instances.test_runner.stop()
        except AttributeError:
            pass
        Global.Instances.logger.stop()
        Global.Instances.watchdog.stop()

    def init_instances(self):
        self.init_dirs()
        Global.Instances.logger = Logger.Logger()
        Global.Instances.watchdog = Watchdog.Watchdog()
        Global.Instances.os_interface = OsInterface.OsInterface()
        Global.Instances.screen_handler = ScreenHandler.ScreenHandler()
        Global.Instances.session_handler = SessionHandler.SessionHandler()
        Global.Instances.song_handler = SongHandler.SongHandler()
        Global.Instances.state_machine = StateMachine.StateMachine()
        Global.Instances.ajax_interface = AjaxInterface.AjaxInterface()
        Global.Instances.command_server = CommandServer.Server()
        Global.Instances.display_server = DisplayServer.Server()
        Global.Instances.web_server = WebServer.Server()
        if Global.CONST.TEST:
            Global.Instances.test_runner = TestRunner.TestRunner()

        Global.Instances.logger.start()
        Global.Instances.state_machine.start()
        Global.Instances.screen_handler.start()
        if Global.CONST.TEST:
            Global.Instances.test_runner.start()

    @staticmethod
    def start_instances():
        Global.Instances.display_server.start()
        Global.Instances.command_server.start()
        Global.Instances.web_server.start()
        Global.Instances.os_interface.start()
        Global.Instances.watchdog.is_started = True

    @staticmethod
    def init_dirs():
        if not os.path.exists(Global.CONST.USER_DATA_DIR):
            os.makedirs(Global.CONST.USER_DATA_DIR)
        Global.CONST.DATA_DIR = os.path.join(Global.CONST.USER_DATA_DIR, 'data')
        if not os.path.exists(Global.CONST.DATA_DIR):
            os.makedirs(Global.CONST.DATA_DIR)
        Global.CONST.SETTINGS_DIR = os.path.join(Global.CONST.USER_DATA_DIR, 'settings')
        if not os.path.exists(Global.CONST.SETTINGS_DIR):
            os.makedirs(Global.CONST.SETTINGS_DIR)
        if not os.path.exists(Global.CONST.LOG.DIR):
            os.makedirs(Global.CONST.LOG.DIR)
            # todo password file

    def run(self):
        self.setFont(QtGui.QFont("DejaVu Sans", 20))
        self.init_instances()
        return self.exec_()


if __name__ == '__main__':
    App = MediaDisplayServerApp(sys.argv)
    exit_code = App.run()
    if Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN:
        Global.Instances.os_interface.shutdown()
    elif Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.REBOOT:
        Global.Instances.os_interface.reboot()
    sys.exit(exit_code)
