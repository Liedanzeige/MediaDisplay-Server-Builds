# -*- coding: utf-8 -*-

import time
import threading
from datetime import datetime as dt

import Global


class StateMachine(object):

    # command types
    #   * system
    #   * ir (infrared)
    #   * network

    def __init__(self):
        self.__run = False
        self.__commands = list()
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None
        self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN

    @property
    def state(self):
        return self.__state

    def init_values(self):
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None

    def __fetch_ir_commands(self):
        commands = Global.Instances.os_interface.get_ir_commands()
        for command in commands:
            self.__commands.append({'type': Global.CONST.SM.CMD_IR, 'data': command})

    def get_total_seconds_since_last_trans(self):
        delta = dt.now() - self.last_transition_time
        return delta.total_seconds()

    def get_total_seconds_since_last_command(self):
        delta = dt.now() - self.last_command_time
        return delta.total_seconds()

    def __set_command_time(self):
        self.__increments = 0
        self.last_command_time = dt.now()

    def pass_command(self, command):
        if isinstance(command, dict) and command.get('type', '') in (Global.CONST.SM.CMD_SYS,
                                                                     Global.CONST.SM.CMD_IR,
                                                                     Global.CONST.SM.CMD_NET):
            self.__commands.append(command)
        else:
            raise Exception(u'Command has to be an dict with key "type"')

    def __change_state(self, target_state, gui_params):
        self.__state = target_state
        self.last_transition_time = dt.now()
        self.__set_command_time()
        Global.Instances.screen_handler.set_page(target_state, gui_params)
        self.__state_changed = True
        Global.Instances.logger.log(u'State changed: %s' % Global.CONST.SM.STATE_DICT.get(target_state, u'Unbekannt'),
                                    __name__, Global.CONST.LOG.SEV.INFO)

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'StateMachine')
            self.__thread.start()

    def stop(self):
        self.__run = False

    def __update(self):
        # get command
        self.__fetch_ir_commands()
        if self.__commands:
            command = self.__commands.pop(0)
            command_type = command['type']
        else:
            command = None
            command_type = None

        self.__increments += 1

        # blink led if ir command
        if command_type == Global.CONST.SM.CMD_IR and (not self.state == Global.CONST.SM.STATE_STANDBY or
                                                               command['data'] == Global.CONST.SM.IRCMD.POWER):
            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.PURPLE, 0.2)

        # state machine
        if self.state == Global.CONST.SM.STATE_START:
            Global.Instances.os_interface.init_hardware()
            Global.Instances.os_interface.init_led()
            Global.Instances.app.start_instances()
            Global.Instances.os_interface.set_led(None)
            self.__change_state(Global.CONST.SM.STATE_IDLE, {})
        elif self.state == Global.CONST.SM.STATE_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_QUIT, command['data'])
                elif command['data'] in Global.CONST.SM.IRCMD.N1_9_LIST:
                    self.__change_state(Global.CONST.SM.STATE_NUMBER, command['data'])
            elif self.get_total_seconds_since_last_command() > Global.CONST.SM.TIME.CHANGE_1_5:
                self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
        elif self.state == Global.CONST.SM.STATE_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.ABORT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N4_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.toggle_solo(command['data'])
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_2_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_NUMBER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.ABORT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST + [Global.CONST.SM.IRCMD.DELIMITER]:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_number(command['data'])
                elif command['data'] in (Global.CONST.SM.IRCMD.MINUS, Global.CONST.SM.IRCMD.OK):
                    self.__change_state(Global.CONST.SM.STATE_SONG, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_3_2:
                self.__change_state(Global.CONST.SM.STATE_SONG, None)
        elif self.state == Global.CONST.SM.STATE_BLUESCREEN:
            pass
        elif self.state == Global.CONST.SM.STATE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_QUIT:
            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.WHITE, 2)
            time.sleep(2)
            Global.Instances.app.quit()
        elif self.state == Global.CONST.SM.STATE_CONFIRM_QUIT:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.ABORT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_SHUTDOWN:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_REBOOT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_QUIT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_7_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        else:
            Global.Instances.logger.log(u'Unknown state, state set to idle', __name__, Global.CONST.LOG.SEV.ERROR)
            # todo restart with bluescreen
            self.__change_state(Global.CONST.SM.STATE_IDLE, {})

    def __loop(self):
        while self.__run:
            self.__state_changed = True
            while self.__state_changed or self.__commands:
                self.__state_changed = False
                self.__update()
            time.sleep(Global.CONST.SM.TIME_INCREMENT)
