# -*- coding: utf-8 -*-

import time
from datetime import datetime as dt
import codecs
import threading
import os
from random import randint

import Global


class TestRunner(object):

    def __init__(self):
        self.thread = threading.Thread(target=self.run_tests, name="TestRunner")
        self.test_result = None

    def check_expected_gui_screen(self, expected_screen):
        screen = Global.Instances.main_window.current
        if screen == expected_screen:
            self.test_result.pass_('Screen "%s" is correct' % screen)
        else:
            self.test_result.fail('Screen should be "%s" but was "%s"' %
                                  (expected_screen, screen))

    def check_expected_label_text(self, label_id, expected_text):
        label = Global.Instances.main_window.ids[label_id]
        label_text = label.text()
        if label_text == expected_text:
            self.test_result.pass_('Labels "%s" text is correct' % label_id)
        else:
            self.test_result.fail('Labels "%s" text should be "%s" but was "%s"' %
                                  (label_id, expected_text, label_text))

    def assert_true(self, cond, message=u''):
        if cond:
            self.test_result.pass_(u'Assert passed ' + message)
        else:
            self.test_result.fail(u'Assert failed ' + message)

    def send_ir_command(self, ir_command):
        Global.Instances.os_interface.add_ir_command(ir_command)
        self.test_result.message(u'IR Command : %s' % ir_command)

    def run_tests(self):
        self.test_result = TestResult()
        self.test_result.message('Commit: %s' % get_commit())
        self.test_result.message('Maybe not everything is committed and pushed')
        with open('TestExecutionQueue.txt', 'r') as test_queue_file:
            test_queue = test_queue_file.readlines()
        for test in test_queue:
            test_func_str = test.strip()
            if hasattr(self, test_func_str):
                test_func = getattr(self, test_func_str)
                try:
                    test_func()
                except Exception as e:
                    self.test_result.fail('Test Failed with message: %s' % str(e))
                    self.test_result.finish_test()

    def sleep_test(self):
        time.sleep(180)

    def stop_test(self):
        self.test_result.start_test('Stop Test')
        self.send_ir_command('c')
        time.sleep(0.5)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(10)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.test_result.finish_test()

    def restart_server_test(self):
        self.test_result.start_test('Restart Server')

        time.sleep(180)
        self.assert_true(Global.Instances.command_server.is_running(), u'Command Server is running')
        Global.Instances.command_server.stop()
        self.assert_true(not Global.Instances.command_server.is_running(), u'Command Server not is running')
        time.sleep(6)
        self.assert_true(Global.Instances.command_server.is_running(), u'Command Server is running')
        time.sleep(1)

        self.assert_true(Global.Instances.display_server.is_running(), u'Display Server is running')
        Global.Instances.display_server.stop()
        self.assert_true(not Global.Instances.display_server.is_running(), u'Display Server not is running')
        time.sleep(6)
        self.assert_true(Global.Instances.display_server.is_running(), u'Display Server is running')
        time.sleep(1)
        self.test_result.finish_test()

    def ir_large_input_test(self):
        self.test_result.start_test('IR Large Input Test')

        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        # test song input
        song = ''
        book = ''
        third = ''
        song_list = [str(randint(0, 9)) for i in range(100)]
        book_list = [str(randint(0, 9)) for i in range(100)]
        third_list = [str(randint(0, 9)) for i in range(100)]

        for i in song_list:
            song += i
            self.send_ir_command(i)
            time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in book_list:
            book += i
            self.send_ir_command(i)
            time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in third_list:
            third += i
            self.send_ir_command(i)
            time.sleep(0.5)

        self.check_expected_label_text('Number', song + '+' + book + '+' + third)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', song)
        self.check_expected_label_text('BookTitle', book)

        time.sleep(30)
        self.send_ir_command('c')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.test_result.finish_test()

    def fsm_ir_commands_test(self):
        """
        This is to test the fsm with simulated ir commands
        :return:
        """
        # wait until the program has started
        self.test_result.start_test('FSM IR Commands Test')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        # test song input
        self.send_ir_command('0')
        time.sleep(3)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)
        self.send_ir_command('4')
        time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(0.5)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '4+123')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '123')
        self.check_expected_label_text('BookTitle', '4')

        # test change solos
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', 'Sopran')

        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('Alt', 'Alt')

        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_label_text('Tenor', 'Tenor')

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', '')

        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Bass', 'Bass')

        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Bass', '')

        time.sleep(30)
        self.send_ir_command('c')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(110)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('-')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(110)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('ok')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(110)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('c')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        time.sleep(3700)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(3800)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(0.5)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(35)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('c')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test quit
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(10)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.test_result.finish_test()

    def shutdown_test(self):
        # wait until the program has started
        self.test_result.start_test('FSM Shutdown Test')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def reboot_test(self):
        # wait until the program has started
        self.test_result.start_test('FSM Reboot Test')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('3')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')
        self.test_result.finish_test()

    def is_running(self):
        self.thread.is_alive()

    def start(self):
        self.thread.start()

    def stop(self):
        self.test_result.finish('TestResult.txt')


def get_time_str():
    return str(dt.now())[:23]


def get_commit():
    return os.popen('git rev-parse HEAD').read().strip()


class TestResult(object):
    """
    test verdicts can be 'Success' or 'Fail'
    """

    file_end_fail = '''[-------------------------------------------------------------------------------------------------]
[                                     FFFFF    A     I  L                                         ]
[                                     F       A A    I  L                                         ]
[                                     FFFF   AAAAA   I  L                                         ]
[                                     F     A     A  I  LLLLL                                     ]
[-------------------------------------------------------------------------------------------------]
'''

    file_end_success = '''[-------------------------------------------------------------------------------------------------]
[                         SSSS   U   U   CCC   CCC  EEEEE   SSSS    SSSS                          ]
[                        SS      U   U  CC    CC    E      SS      SS                             ]
[                         SSSS   U   U  C     C     EEEEE   SSSS    SSSS                          ]
[                            SS  U   U  CC    CC    E          SS      SS                         ]
[                         SSSS    UUU    CCC   CCC  EEEEE   SSSS    SSSS                          ]
[-------------------------------------------------------------------------------------------------]
'''

    def __init__(self):
        """

        """
        self.start_time = dt.now()
        self.finish_time = None
        self.tests_number = 0
        self.test_failed = 0
        self.tests_succeded = 0
        self.current_test_verdict = 'Success'
        self.__log_text = '''[-------------------------------------------------------------------------------------------------]
[                                                                                                 ]
[                                           TEST RESULT                                           ]
[                                                                                                 ]
[-------------------------------------------------------------------------------------------------]
'''

    def get_duration(self):
        if not self.finish_time:
            return (dt.now() - self.start_time).total_seconds()
        else:
            return (self.finish_time - self.start_time).total_seconds()

    def __log(self, message, severity=''):
        text = '[' + get_time_str() + '] [% 4s] ' % severity + message
        print text
        self.__log_text += text + '\n'

    def fail(self, message=''):
        self.current_test_verdict = 'Fail'
        self.__log(message, 'Fail')

    def pass_(self, message=''):
        self.__log(message)

    def message(self, message=''):
        self.__log(message, '~')

    def write(self, file_name):
        with codecs.open(file_name, 'w', 'utf-8') as result_file:
            result_file.write(self.__log_text)

    def start_test(self, test_name):
        self.current_test_verdict = None
        self.tests_number += 1
        self.__log('Test %s started' % test_name, '----')

    def finish_test(self):
        if self.current_test_verdict is None:
            self.tests_succeded += 1
            self.__log('Test finished', 'OK')
        else:
            self.test_failed += 1
            self.__log('Test finished', 'Fail')

    def finish(self, file_name):
        if self.finish_time:
            return
        self.finish_time = dt.now()
        if self.test_failed:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), 'Fail')
            self.__log_text += TestResult.file_end_fail
        else:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), 'OK')
            self.__log_text += TestResult.file_end_success
        self.write(file_name)
