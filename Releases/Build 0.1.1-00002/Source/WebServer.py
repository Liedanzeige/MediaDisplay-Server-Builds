# -*- coding: utf-8 -*-

from BaseHTTPServer import HTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
import threading
import urllib
import urllib2
import urlparse
import posixpath
import os
import Cookie
import ssl

import Global


class WebRequestHandler(SimpleHTTPRequestHandler):

    def generate_html(self):
        html = u'''<!DOCTYPE html>
    <html>
        <head>
            <title>MediaDisplay-Web</title>
            <meta charset="utf-8"/>
        </head>
        <body>
            <h1>Not Implemented</h1>
            <footer>
                <p>
                    MediaDisplay-Server Web Server
                </p>
                <p>
                    Version: ''' + Global.CONST.VERSION + u'''
                </p>
                <p>
                    Build: ''' + Global.CONST.BUILD + u'''
                </p>
            </footer>
        </body>
    </html>'''

        return html

    def do_GET(self):
        """Serve a GET request."""

        decoded_url = urllib2.unquote(self.path).decode('utf-8')
        parsed_url = urlparse.urlparse(decoded_url)
        parameters = urlparse.parse_qs(parsed_url.query)

        req_cookie = Cookie.SimpleCookie()
        try:
            cookie_str = self.headers.get('Cookie')
            req_cookie.load(cookie_str)
            temp_token = req_cookie['token'].value
        except Exception as e:
            if not parameters.has_key('token'):
                parameters['token'] = [None]
        else:
            if not parameters.has_key('token'):
                parameters['token'] = [temp_token]

        #'''
        if decoded_url in ('', '/'):
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(self.generate_html().encode('utf-8'))
        else:
            self.send_error(404, 'Nothing matches the given URI')
            self.wfile.write('Wolltest du mich testen oder was?')

        return
        #'''

        if parsed_url.path[:6] == u'/ajax/':
            json_answer, cookie = Global.Instances.ajax_interface.get_ajax(parsed_url, parameters)
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            if cookie is not None:
                self.wfile.write(cookie.output())
                self.wfile.write('\r\n')
            self.end_headers()
            self.wfile.write(json_answer)
        else:
            f = self.send_head()
            if f:
                try:
                    self.copyfile(f, self.wfile)
                finally:
                    f.close()
            #self.send_error(403, 'Kein Zugriff')
            #self.wfile.write('Wolltest du mich testen oder was?')

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.  (XXX They should
        probably be diagnosed.)

        """
        # abandon query parameters
        path = path.split('?', 1)[0]
        path = path.split('#', 1)[0]
        # Don't forget explicit trailing slash when normalizing. Issue17324
        trailing_slash = path.rstrip().endswith('/')
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)

        '''
        path = os.getcwd()
        '''
        path = os.path.join(Global.CONST.SCRIPT_DIR, u'WS_DocumentRoot')
        # '''

        for word in words:
            if os.path.dirname(word) or word in (os.curdir, os.pardir):
                # Ignore components that are not a simple file/directory name
                continue
            path = os.path.join(path, word)
        if trailing_slash:
            path += '/'
        return path

class Server:
    def __init__(self):
        self.server = HTTPServer(("", Global.CONST.WS.PORT), WebRequestHandler)
        # todo https
        # self.server.socket = ssl.wrap_socket(self.server.socket, certfile='static/server.pem', server_side=True)
        self.__thread = None

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        return  # deactivated in this version
        if not self.is_running():
            self.__thread = threading.Thread(target=self.server.serve_forever, args=(), name='WebServer')
            self.__thread.start()

    def stop(self):
        if self.is_running():
            self.server.shutdown()
