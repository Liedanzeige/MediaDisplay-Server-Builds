# -*- coding: utf-8 -*-
"""Das OsInterface stellt betriebssystemspezifische Funktionen zur Verfügung. Dieses Modul importiert, in Abhängigkeit
ob das Programm auf dem Raspberry ausgeführt wird oder nicht, entweder das Modul OsInterfaceRaspberry oder 
OsInterfaceNoRpi. Für die richtige Dokumentation siehe OsInterfaceRaspberry.
"""

import Global

if Global.CONST.ON_RPI:
    from OsInterfaceRaspberry import OsInterface
else:
    from OsInterfaceNoRpi import OsInterface
