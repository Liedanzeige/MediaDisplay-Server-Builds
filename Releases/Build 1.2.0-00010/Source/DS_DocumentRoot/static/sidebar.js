function toggle_menu() {
    var elem = document.getElementById("nav");
    if (elem.className === "nav") {
        elem.className = "nav responsive";
    } else {
        elem.className = "nav";
    }
}
