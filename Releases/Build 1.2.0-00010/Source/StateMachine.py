# -*- coding: utf-8 -*-
"""Stellt eine Klasse "StateMachine" zur Verfügung.
"""

import time
import threading
from datetime import datetime as dt

import Global
import UpdateHandler
import Logger
import DiagnosticModule
import RebootChecker


class StateMachine(object):
    """Implementiert eine StateMachine zur Steuerung der Applikation."""

    def __init__(self, application):
        self.__application = application
        self.__run = False
        self.__commands = list()
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None
        self.__time_display_message = Global.CONST.SM.TIME.DEFAULT_TIME_MESSAGE
        self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN
        self.__reboot_checker = RebootChecker.RebootChecker()

    @property
    def state(self):
        """Der aktuelle Zustand."""
        return self.__state

    def init_values(self):
        """Initialisiert alle Variablen der StateMachine."""
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None

    def __fetch_ir_commands(self):
        """Holt IR-Kommandos aus dem OsInterface."""
        commands = self.__application.os_interface.get_ir_commands()
        for command in commands:
            self.__commands.append({'type': Global.CONST.SM.CMD_IR, 'data': command})

    def add_network_command(self, action, action_data=None):
        """Fügt einen neuen Network-Command hinzu.

        :param action: unicode, die auszuführende Aktion
        :param action_data: obj, aktionsabhängige Daten wie z.B. das anzuzeigende Lied
        :return:
        """
        self.__commands.append({'type': Global.CONST.SM.CMD_NET, 'data': {'action': action,
                                                                          'action_data': action_data}})

    def get_total_seconds_since_last_trans(self):
        """Gibt die Zeit seit dem letzten Übergang in Sekunden zurück. Die Zeit orientiert sich dabei an der Systemzeit 
        und ist daher nicht geeignet wenn die Systemzeit verstellt wird.
        
        :return: Int Zeit seit dem letzten Übergang in Sekunden.
        """
        delta = dt.now() - self.last_transition_time
        return delta.total_seconds()

    def get_total_seconds_since_last_command(self):
        """Gibt die Zeit seit dem letzten akzeptierten Kommando in Sekunden zurück. Die Zeit orientiert sich dabei an 
        der Systemzeit und ist daher nicht geeignet wenn die Systemzeit verstellt wird.

        :return: Int Zeit seit dem letzten akzeptierten Kommando in Sekunden.
        """
        delta = dt.now() - self.last_command_time
        return delta.total_seconds()

    def get_total_seconds_increment(self):
        """Gibt die Zeit seit dem letzten akzeptierten Kommando in Sekunden zurück. Die Zeit orientiert sich dabei an 
        der Anzahl Updateschleifen und kann daher von der echten Zeit abweichen. Da sie aber unabhängig von der 
        Systemzeit ist kann sie verwendet werden, wenn die Systemzeit verstellt wird.

        :return: Int Zeit seit dem letzten akzeptierten Kommando in Sekunden.
        """
        return self.__increments * Global.CONST.SM.TIME_INCREMENT

    def __set_command_time(self):
        """Muss bei jedem akzeptieren Kommando aufgerufen werden, damit die Zeit seit dem letzten Kommando 
        zurückgesetzt wird."""
        self.__increments = 0
        self.last_command_time = dt.now()

    def pass_command(self, command):
        """Nimmt ein neues Kommando entgegen.
        
        :param command: Dict mit Key "type", das Kommando
        """
        if isinstance(command, dict) and command.get('type', '') in (Global.CONST.SM.CMD_SYS,
                                                                     Global.CONST.SM.CMD_IR,
                                                                     Global.CONST.SM.CMD_NET):
            self.__commands.append(command)
        else:
            raise Exception(u'Command has to be an dict with key "type"')

    def __change_state(self, target_state, gui_params):
        """Setzt den Zustand der StateMachine.
        
        :param target_state: Int Zustand der eingenommen werden soll.
        :param gui_params: Parameter für die GUI, werden dem ScreenHandler übergeben.
        """
        self.__state = target_state
        self.last_transition_time = dt.now()
        self.__set_command_time()
        self.__application.screen_handler.set_page(target_state, gui_params)
        self.__state_changed = True
        if target_state == Global.CONST.SM.STATE_STANDBY:
            self.__application.os_interface.switch_hdmi_off()
        self.__application.logger.log(u'State changed: %s' % Global.CONST.SM.STATE_DICT.get(target_state, u'Unbekannt'),
                                      __name__, Logger.SEVERITY_INFO)

    def is_running(self):
        """Gibt zurück, ob die Updateschleife läuft.

        :return: Bool Wahr, wenn die Updateschleife läuft
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet die Updateschleife wenn sie noch nicht läuft."""
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name='StateMachine')
            self.__thread.start()

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False
        if self.__thread is not None:
            self.__thread.join(2 * Global.CONST.SM.TIME_INCREMENT)

    def __set_time_display_message(self, display_time):
        if not isinstance(display_time, int) or display_time not in range(Global.CONST.SM.TIME.MIN_TIME_MESSAGE,
                                                                          Global.CONST.SM.TIME.MAX_TIME_MESSAGE):
            self.__time_display_message = Global.CONST.SM.TIME.DEFAULT_TIME_MESSAGE
        else:
            self.__time_display_message = display_time

    def __update(self):
        # get command
        self.__fetch_ir_commands()
        if self.__commands:
            command = self.__commands.pop(0)
            command_type = command['type']
            if command_type == Global.CONST.SM.CMD_IR:
                self.__application.logger.log('IR command "%s" received' % command['data'],
                                              __name__, Logger.SEVERITY_DEBUG)
            elif command_type == Global.CONST.SM.CMD_NET:
                self.__application.logger.log('Network command "%s" received',
                                              __name__, Logger.SEVERITY_DEBUG)
        else:
            command = None
            command_type = None

        self.__increments += 1

        # blink led if ir command
        if command_type == Global.CONST.SM.CMD_IR and (not self.state == Global.CONST.SM.STATE_STANDBY or
                                                       command['data'] == Global.CONST.SM.IRCMD.POWER):
            self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.PURPLE, 0.2)

        # state machine
        if self.state == Global.CONST.SM.STATE_START:
            self.__application.screen_handler.set_start_screen_message(u'Initialisiere Hardware...')
            self.__application.logger.log('Start init hardware...', __name__, Logger.SEVERITY_DEBUG)
            self.__application.os_interface.init_hardware()
            self.__application.logger.log('Hardware inited', __name__, Logger.SEVERITY_DEBUG)

            self.__application.screen_handler.set_start_screen_message(u'Teste LED...')
            self.__application.logger.log('Start init LED...', __name__, Logger.SEVERITY_DEBUG)
            self.__application.os_interface.init_led()
            self.__application.logger.log('LED inited', __name__, Logger.SEVERITY_DEBUG)

            self.__application.screen_handler.set_start_screen_message(u'Starte alle Instanzen...')
            self.__application.logger.log('App start instances...', __name__, Logger.SEVERITY_DEBUG)
            self.__application.start_instances()
            self.__application.logger.log('App instances start finished', __name__, Logger.SEVERITY_DEBUG)

            self.__application.os_interface.set_led(None)

            self.__application.screen_handler.set_start_screen_message(u'Prüfe ob Update vorhanden ist...')

            self.__application.logger.log('Start check update...', __name__, Logger.SEVERITY_DEBUG)
            if UpdateHandler.check_update_finish():
                self.__application.logger.log('Update finished detected. Delete update directory...', __name__,
                                              Logger.SEVERITY_DEBUG)
                try:
                    UpdateHandler.delete_update_dir()
                except Exception as e:
                    self.__application.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                  Logger.SEVERITY_ERROR)
                    self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                               u'nicht gelöscht werden.', True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return
                else:
                    self.__application.logger.log('Update dir deleted.', __name__, Logger.SEVERITY_DEBUG)

            self.__application.logger.log('Check update next start available...', __name__, Logger.SEVERITY_DEBUG)
            if UpdateHandler.check_nextstart_script_available() and not UpdateHandler.check_update_finish():
                self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN
                self.__application.logger.log('Next start update script available. Execute next start script.',
                                              __name__, Logger.SEVERITY_DEBUG)
                self.__application.screen_handler.set_start_screen_message(u'Update wird weiter ausgeführt...')
                time.sleep(10)
                self.__change_state(Global.CONST.SM.STATE_QUIT, {})
                UpdateHandler.start_nextstart_process()
                return

            self.__application.logger.log('Delete update directory if exists...', __name__,
                                          Logger.SEVERITY_DEBUG)
            try:
                UpdateHandler.delete_update_dir()
            except Exception as e:
                self.__application.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                              Logger.SEVERITY_ERROR)
                self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                           u'nicht gelöscht werden.', True)
                time.sleep(10)
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                return

            if UpdateHandler.update_dir_exists():
                self.__application.logger.log(u'Update dir still exists. Going to execute the program normally.',
                                              __name__,
                                              Logger.SEVERITY_ERROR)
                self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                           u'nicht gelöscht werden. Programm wird '
                                                                           u'normal ausgeführt.', True)
                time.sleep(10)
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                return

            # Update package in home dir
            if UpdateHandler.check_update_in_home_dir():
                self.__application.logger.log(u'Update package found in home directory. '
                                              u'Going to unpack update package.',
                                              __name__,
                                              Logger.SEVERITY_DEBUG)
                self.__application.screen_handler.set_start_screen_message(u'Update wurde im Home-Ordner gefunden.')
                time.sleep(10)
                self.__application.screen_handler.set_start_screen_message(u'Update wird entpackt...')
                try:
                    UpdateHandler.unpack_update_home_dir()
                except Exception as e:
                    self.__application.logger.log(u'Could not unpack update package: {0}'.format(unicode(e)), __name__,
                                                  Logger.SEVERITY_ERROR)
                    self.__application.screen_handler.set_start_screen_message(u'Fehler: Update konnte nicht '
                                                                               u'entpackt werden. Update kann nicht '
                                                                               u'ausgeführt werden.', True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                self.__application.logger.log(u'Check if update is possible', __name__,
                                              Logger.SEVERITY_DEBUG)
                self.__application.screen_handler.set_start_screen_message(u'Prüfe ob Update möglich ist...')
                time.sleep(10)
                try:
                    update_check = UpdateHandler.check_update_possible(self.__application.logger)
                except Exception as e:
                    self.__application.logger.log(u'Failed to check update: {0}'.format(unicode(e)), __name__,
                                                  Logger.SEVERITY_ERROR)
                    self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Prüfung schlug fehl. '
                                                                               u'Update kann nicht ausgeführt werden.',
                                                                               True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                if update_check[0]:  # update possible
                    self.__application.logger.log(u'Update possible: {0}'.format(update_check[1]), __name__,
                                                  Logger.SEVERITY_DEBUG)
                    self.__application.screen_handler.set_start_screen_message(u'Update kann ausgeführt werden.')
                    time.sleep(10)
                    try:
                        UpdateHandler.delete_home_update()
                    except Exception as e:
                        self.__application.logger.log(u'Failed to delete update package in home dir: {0}'
                                                      .format(unicode(e)),
                                                      __name__,
                                                      Logger.SEVERITY_ERROR)
                        self.__change_state(Global.CONST.SM.STATE_CONFIRM_UPDATE, {})
                        return
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_UPDATE, {})
                    return
                else:
                    self.__application.logger.log(u'Update not possible: {0}'.format(update_check[1]), __name__,
                                                  Logger.SEVERITY_DEBUG)
                    self.__application.screen_handler.set_start_screen_message(update_check[1], True)
                    time.sleep(10)
                    try:
                        UpdateHandler.delete_update_dir()
                    except Exception as e:
                        self.__application.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                      Logger.SEVERITY_ERROR)
                        self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                                   u'nicht gelöscht werden.',
                                                                                   True)
                        time.sleep(10)
                        self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                        return
                    else:
                        self.__application.logger.log('Update dir deleted.', __name__, Logger.SEVERITY_DEBUG)

            # Update package on usb device
            if UpdateHandler.check_update_on_usb_drive():
                self.__application.logger.log(u'Update package found on usb device. Going to unpack update package.',
                                              __name__,
                                              Logger.SEVERITY_DEBUG)
                self.__application.screen_handler.set_start_screen_message(u'Update wurde auf einem '
                                                                           u'USB-Gerät gefunden.')
                time.sleep(10)
                self.__application.screen_handler.set_start_screen_message(u'Update wird entpackt...')
                try:
                    UpdateHandler.unpack_update_usb_drive()
                except Exception as e:
                    self.__application.logger.log(u'Could not unpack update package: {0}'.format(unicode(e)), __name__,
                                                  Logger.SEVERITY_ERROR)
                    self.__application.screen_handler.set_start_screen_message(u'Fehler: Update konnte nicht '
                                                                               u'entpackt werden. Update kann nicht '
                                                                               u'ausgeführt werden.',
                                                                               True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                self.__application.logger.log(u'Check if update is possible', __name__,
                                              Logger.SEVERITY_DEBUG)
                self.__application.screen_handler.set_start_screen_message(u'Prüfe ob Update möglich ist...')
                time.sleep(10)
                try:
                    update_check = UpdateHandler.check_update_possible(self.__application.logger)
                except Exception as e:
                    self.__application.logger.log(u'Failed to check update: {0}'.format(unicode(e)), __name__,
                                                  Logger.SEVERITY_ERROR)
                    self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Prüfung schlug fehl. '
                                                                               u'Update kann nicht ausgeführt werden.',
                                                                               True)
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                    return

                if update_check[0]:  # update possible
                    self.__application.logger.log(u'Update possible: {0}'.format(update_check[1]), __name__,
                                                  Logger.SEVERITY_DEBUG)
                    self.__application.screen_handler.set_start_screen_message(u'Update kann ausgeführt werden.')
                    time.sleep(10)
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_UPDATE, {})
                    return
                else:
                    self.__application.logger.log(u'Update not possible: {0}'.format(update_check[1]), __name__,
                                                  Logger.SEVERITY_DEBUG)
                    self.__application.screen_handler.set_start_screen_message(update_check[1], True)
                    time.sleep(10)
                    try:
                        UpdateHandler.delete_update_dir()
                    except Exception as e:
                        self.__application.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                      Logger.SEVERITY_ERROR)
                        self.__application.screen_handler.set_start_screen_message(u'Fehler: Update-Ordner konnte '
                                                                                   u'nicht gelöscht werden.', True)
                        time.sleep(10)
                        self.__change_state(Global.CONST.SM.STATE_IDLE, {})
                        return
                    else:
                        self.__application.logger.log('Update dir deleted.', __name__, Logger.SEVERITY_DEBUG)

            if self.__reboot_checker.rebooted_from_standby():
                self.__change_state(Global.CONST.SM.STATE_STANDBY, {})
            else:
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
            self.__reboot_checker.remove_reboot_from_standby()
            self.__application.screen_handler.set_start_screen_message(u'Liedanzeige initialisiert')

        elif self.state == Global.CONST.SM.STATE_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_QUIT, command['data'])
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__change_state(Global.CONST.SM.STATE_NUMBER, command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    if self.__application.song_handler.has_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                    else:
                        self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                elif command['data'] == Global.CONST.SM.IRCMD.MENU:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME, None)
                elif command['data'] == Global.CONST.SM.IRCMD.GUIDE:
                    self.__change_state(Global.CONST.SM.STATE_IP, None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK and \
                        self.__application.general_settings.clock_idle == \
                        Global.CONST.STH.GENERAL.CLOCK_IDLE_LIVE_STUDIO:
                    self.__change_state(Global.CONST.SM.STATE_ON_AIR, None)
            elif command_type == Global.CONST.SM.CMD_NET:
                if command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG:
                    self.__change_state(Global.CONST.SM.STATE_SONG, command['data']['action_data'])
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG_AGAIN:
                    if self.__application.song_handler.has_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                    else:
                        self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                elif command['data']['action'] == Global.CONST.SM.ACTION_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHUTDOWN:
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_REBOOT:
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_MESSAGE:
                    self.__change_state(Global.CONST.SM.STATE_DISPLAY_MESSAGE, command['data']['action_data'][0])
                    self.__set_time_display_message(command['data']['action_data'][1])
            elif self.__application.general_settings.time_1_5 != 0.0 and \
                    self.get_total_seconds_since_last_command() >= self.__application.general_settings.time_1_5 * 60:
                self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
            elif self.__reboot_checker.check_reboot(self.get_total_seconds_since_last_command()):
                self.shutdown_mode = Global.CONST.SM.REBOOT
                self.__change_state(Global.CONST.SM.STATE_QUIT, None)
        elif self.state == Global.CONST.SM.STATE_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N4_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.toggle_solo(command['data'])
            elif command_type == Global.CONST.SM.CMD_NET:
                if command['data']['action'] == Global.CONST.SM.ACTION_SHOW_CLOCK:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG:
                    self.__change_state(Global.CONST.SM.STATE_SONG, command['data']['action_data'])
                elif command['data']['action'] == Global.CONST.SM.ACTION_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHUTDOWN:
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_REBOOT:
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_MESSAGE:
                    self.__change_state(Global.CONST.SM.STATE_DISPLAY_MESSAGE, command['data']['action_data'][0])
                    self.__set_time_display_message(command['data']['action_data'][1])
            elif command_type == Global.CONST.SM.CMD_NET:
                if command['data']['action'] == Global.CONST.SM.ACTION_SHOW_CLOCK:
                    self.__set_command_time()
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= self.__application.general_settings.time_2_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_NUMBER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] in (Global.CONST.SM.IRCMD.EXIT, Global.CONST.SM.IRCMD.LEFT):
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.DELIMITER:
                    if self.__application.screen_handler.song_completed():
                        if self.__application.screen_handler.has_valide_song():
                            self.__change_state(Global.CONST.SM.STATE_SONG, None)
                        else:
                            self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                            self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                    else:
                        self.__set_command_time()
                        self.__application.screen_handler.add_number(command['data'])
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_number(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.delete_number()
                elif command['data'] in (Global.CONST.SM.IRCMD.RIGHT, Global.CONST.SM.IRCMD.OK):
                    if self.__application.screen_handler.has_valide_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, None)
                    else:
                        self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                        self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= self.__application.general_settings.time_3_2:
                if self.__application.screen_handler.has_valide_song():
                    self.__change_state(Global.CONST.SM.STATE_SONG, None)
                else:
                    self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.RED, 0.5)
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_BLUESCREEN:
            pass
        elif self.state == Global.CONST.SM.STATE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    self.__application.os_interface.switch_hdmi_on()
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif command_type == Global.CONST.SM.CMD_NET:
                if command['data']['action'] == Global.CONST.SM.ACTION_SHOW_CLOCK:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG:
                    self.__change_state(Global.CONST.SM.STATE_SONG, command['data']['action_data'])
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG_AGAIN:
                    if self.__application.song_handler.has_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHUTDOWN:
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_REBOOT:
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_MESSAGE:
                    self.__change_state(Global.CONST.SM.STATE_DISPLAY_MESSAGE, command['data']['action_data'][0])
                    self.__set_time_display_message(command['data']['action_data'][1])
            elif self.__reboot_checker.check_reboot(self.get_total_seconds_since_last_command()):
                self.__reboot_checker.write_reboot_from_standby()
                self.shutdown_mode = Global.CONST.SM.REBOOT
                self.__change_state(Global.CONST.SM.STATE_QUIT, None)
        elif self.state == Global.CONST.SM.STATE_QUIT:
            # wait until slave has shutdown
            self.__application.os_interface.set_led_time(Global.CONST.OSI.COLORS.WHITE, 30)
            i = 30
            while i > 0:
                if not self.__application.display_server.is_running():
                    break
                if not self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN:
                    break
                time_delta = dt.now() - self.__application.display_server.last_slave_request
                if time_delta.total_seconds() > 10.0:
                    break
                time.sleep(1)
                i -= 1
            self.__application.quit()
        elif self.state == Global.CONST.SM.STATE_CONFIRM_QUIT:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.CONFIRM_SHUTDOWN, Global.CONST.SM.IRCMD.OK):
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_REBOOT:
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_QUIT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_7_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DATE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.PLUS:
                    self.__set_command_time()
                    self.__application.os_interface.hour_plus()
                elif command['data'] == Global.CONST.SM.IRCMD.MINUS:
                    self.__set_command_time()
                    self.__application.os_interface.hour_minus()
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_time_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.add_time_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_time()
            elif self.get_total_seconds_increment() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_DATE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_date_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.add_date_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_date()
            elif self.get_total_seconds_increment() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_CLOCK_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DATE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2, Global.CONST.SM.IRCMD.N3,
                                         Global.CONST.SM.IRCMD.N9):
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_CLOCK_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_ORDER_SOLO, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_ORDER_SOLO, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_SONG_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DISPLAY_SERVER, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SYSTEM_INFORMATION:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_IP:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif command_type == Global.CONST.SM.CMD_NET:
                if command['data']['action'] == Global.CONST.SM.ACTION_SHOW_CLOCK:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG:
                    self.__change_state(Global.CONST.SM.STATE_SONG, command['data']['action_data'])
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG_AGAIN:
                    if self.__application.song_handler.has_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                elif command['data']['action'] == Global.CONST.SM.ACTION_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHUTDOWN:
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_REBOOT:
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_MESSAGE:
                    self.__change_state(Global.CONST.SM.STATE_DISPLAY_MESSAGE, command['data']['action_data'][0])
                    self.__set_time_display_message(command['data']['action_data'][1])
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_17_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_DISPLAY_SERVER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DISPLAY_SERVER, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SYSTEM_INFORMATION, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.DELIMITER:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(Global.CONST.SCH.DELIMITER)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    self.__application.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_ORDER_SOLO:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2, Global.CONST.SM.IRCMD.N3,
                                         Global.CONST.SM.IRCMD.N4):
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    self.__application.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_CONFIRM_UPDATE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__application.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__application.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    if UpdateHandler.get_update_confirmation_number() == self.__application.screen_handler.get_cache():
                        self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN
                        UpdateHandler.start_update_process()
                        self.__change_state(Global.CONST.SM.STATE_QUIT, {})
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    try:
                        UpdateHandler.delete_update_dir()
                    except Exception as e:
                        self.__application.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                      Logger.SEVERITY_ERROR)

                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
            elif self.get_total_seconds_since_last_trans() > Global.CONST.SM.TIME.CHANGE_SETTINGS:
                try:
                    UpdateHandler.delete_update_dir()
                except Exception as e:
                    self.__application.logger.log(u'Could not delete update dir: {0}'.format(unicode(e)), __name__,
                                                  Logger.SEVERITY_ERROR)
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
        elif self.state == Global.CONST.SM.STATE_DISPLAY_MESSAGE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
            elif command_type == Global.CONST.SM.CMD_NET:
                if command['data']['action'] == Global.CONST.SM.ACTION_SHOW_CLOCK:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG:
                    self.__change_state(Global.CONST.SM.STATE_SONG, command['data']['action_data'])
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_SONG_AGAIN:
                    if self.__application.song_handler.has_song():
                        self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                elif command['data']['action'] == Global.CONST.SM.ACTION_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHUTDOWN:
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_REBOOT:
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                elif command['data']['action'] == Global.CONST.SM.ACTION_SHOW_MESSAGE:
                    self.__change_state(Global.CONST.SM.STATE_DISPLAY_MESSAGE, command['data']['action_data'][0])
                    self.__set_time_display_message(command['data']['action_data'][1])
            elif self.get_total_seconds_since_last_trans() > self.__time_display_message:
                self.__change_state(Global.CONST.SM.STATE_IDLE, {})
        elif self.state == Global.CONST.SM.STATE_ON_AIR:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] in (Global.CONST.SM.IRCMD.OK, Global.CONST.SM.IRCMD.EXIT):
                    self.__change_state(Global.CONST.SM.STATE_IDLE, {})
            # intentionally no timeout and no other commands
        else:
            self.__application.logger.log(u'Unknown state, restart with WatchDog', __name__, Logger.SEVERITY_ERROR)
            self.__application.watchdog.restart = True

    @DiagnosticModule.execute_with_exception_handling
    def __loop(self):
        while self.__run:
            self.__state_changed = True
            while self.__state_changed or self.__commands:
                self.__state_changed = False
                self.__update()
            time.sleep(Global.CONST.SM.TIME_INCREMENT)
