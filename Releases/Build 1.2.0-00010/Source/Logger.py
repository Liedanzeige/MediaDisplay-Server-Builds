# -*- coding: utf-8 -*-

from datetime import datetime as dt
import threading
import codecs
import os
import sys
import re
import time

import DiagnosticModule

# Severity der Log-Meldungen
SEVERITY_DICT = {
    0: 'Debug',
    1: 'Info',
    2: 'Warning',
    3: 'Error'
}

SEVERITY_DEBUG = 0
SEVERITY_INFO = 1
SEVERITY_WARNING = 2
SEVERITY_ERROR = 3

TIME_INCREMENT = 1.0


class Logger(object):
    """Stellt Funktionen für das Logging zu Verfügung."""

    def __init__(self, log_dir, log_file_prefix, expiration_days=31):
        self.__dir = os.path.normpath(os.path.abspath(log_dir))
        self.__expiration_days = max(expiration_days, 1)
        if not os.path.isdir(self.__dir):
            os.makedirs(self.__dir)
        self.__log_file_prefix = log_file_prefix
        self.__last_update_time = dt.now()
        self.__run = False
        self.__thread = None
        self.__find_regex = \
            r'\A{log_file_prefix} (\d{{4}})-(\d{{2}})-(\d{{2}})\.log\Z'.format(
                log_file_prefix=re.escape(log_file_prefix))

    def log(self, message, module_name, severity=SEVERITY_INFO):
        now = dt.now()
        now_str = str(now)
        if len(now_str) <= 19:
            now_str += '.000000'
        now_str = now_str[0:23]
        log_file_name = os.path.join(self.__dir,
                                     '{} {:04d}-{:02d}-{:02d}.log'.format(self.__log_file_prefix,
                                                                          now.year,
                                                                          now.month,
                                                                          now.day
                                                                          )
                                     )
        raw_log_message = u'{}\t{}\t{}\t{}'.format(now_str, module_name, SEVERITY_DICT[severity], message)
        log_message = u'{}{}'.format(raw_log_message.replace('\n', '').replace('\r', ''), os.linesep)
        try:
            log_file = codecs.open(log_file_name, 'a+', 'utf-8')
            log_file.write(log_message)
        except IOError:
            sys.stderr.write('Error writing to log file')
            sys.stderr.write(repr(log_message))

    def cleanup(self):
        now = dt.now()
        errors = 0
        deleted = 0
        for file_name in os.listdir(self.__dir):
            match = re.match(self.__find_regex, file_name)
            if match:
                date = dt(int(match.group(1)), int(match.group(2)), int(match.group(3)))
                delta = now - date
                if delta.days > self.__expiration_days:
                    try:
                        os.remove(os.path.join(self.__dir, file_name))
                        deleted += 1
                    except OSError:
                        errors += 1
        if errors > 0:
            log_sev = SEVERITY_WARNING
        else:
            log_sev = SEVERITY_INFO
        self.log('Cleaned log files with %i errors. %i files deleted.' % (errors, deleted),
                 __name__, log_sev)

    def is_running(self):
        """Gibt zurück, ob die Updateschleife läuft.

        :return: Bool Wahr, wenn die Updateschleife läuft
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet die Updateschleife wenn sie noch nicht läuft."""
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop,
                                             args=(),
                                             name='Logger {}'.format(self.__log_file_prefix))
            self.__thread.start()

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False
        if self.__thread is not None:
            self.__thread.join(2 * TIME_INCREMENT)

    def __update(self):
        if self.__last_update_time.day != dt.now().day:
            self.cleanup()
        self.__last_update_time = dt.now()

    @DiagnosticModule.execute_with_exception_handling
    def __loop(self):
        while self.__run:
            self.__update()
            time.sleep(TIME_INCREMENT)
