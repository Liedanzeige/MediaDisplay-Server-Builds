# -*- coding: utf-8 -*-
"""Dieses Modul wird momentan nicht genutzt.
"""

import json
import Cookie

import Global


class AjaxInterface(object):
    def get_ajax(self, parsed_url, parameters):
        json_answer = Global.CONST.AI.ERROR3
        cookie = None
        if parsed_url.path == '/ajax/system/login':
            name = parameters.get('name', [None])[0]
            password = parameters.get('password', [None])[0]
            if name is None or password is None:
                json_answer = Global.CONST.AI.ERROR0
            else:
                check = Global.Instances.session_handler.login(name, password)
                if check[0]:
                    json_answer = json.dumps({"ok": True, "token": check[1]})
                    cookie = Cookie.SimpleCookie()
                    cookie["token"] = check[1]
                    cookie["token"]["path"] = '/'
                else:
                    json_answer = check[1]
        elif parsed_url.path == '/ajax/system/logout':
            token = parameters.get('token', [None])[0]
            Global.Instances.session_handler.logout(token)
            json_answer = json.dumps({"ok": True})
        elif parsed_url.path == '/ajax/screen/setSong':
            token = parameters.get('token', [None])[0]
            check_token = Global.Instances.session_handler.check_session(token, True)
            if check_token:
                book = parameters.get('book', [None])[0]
                number = parameters.get('number', [None])[0]
                sopran = parameters.get('sopran', '0')[0]
                alt = parameters.get('alt', '0')[0]
                tenor = parameters.get('tenor', '0')[0]
                bass = parameters.get('bass', '0')[0]
                check = Global.Instances.screen_handler.checkSong(book, number)
                if not check[0]:
                    json_answer = check[1]
                else:
                    Global.Instances.screen_handler.setSongScreen(book, number, sopran, alt, tenor, bass)
                    json_answer = json.dumps({"ok": True})
            else:
                json_answer = Global.CONST.AI.ERROR2
        elif parsed_url.path == '/ajax/screen/setClock':
            token = parameters.get('token', [None])[0]
            check_token = Global.Instances.session_handler.check_session(token, True)
            if check_token:
                Global.Instances.screen_handler.setClockScreen()
                json_answer = json.dumps({"ok": True})
            else:
                json_answer = Global.CONST.AI.ERROR2
        elif parsed_url.path == '/ajax/screen/getSong':
            if Global.Instances.screen_handler.current_song is None:
                json_answer = Global.CONST.AI.ERROR6
            else:
                json_answer = json.dumps({"ok": True,
                                          "title": Global.Instances.screen_handler.get_current_song_str()})
        return json_answer, cookie
