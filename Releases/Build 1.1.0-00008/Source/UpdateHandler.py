# -*- coding: utf-8 -*-
"""Stellt Funktionen zum Update zur Verfügung"""

import sys
import subprocess
import os
import tarfile
import shutil
import random
import re

import Global

UPDATE_PACKAGE_NAME = 'UpdateMDS.tar.gz'

if Global.CONST.ON_RPI:
    UPDATE_DIR = '/home/pi/Update'
    UPDATE_PACKAGE_HOME_PATH = os.path.join('/home/pi', UPDATE_PACKAGE_NAME)
else:
    UPDATE_DIR = os.path.join(os.path.expanduser('~'), 'Update')
    UPDATE_PACKAGE_HOME_PATH = os.path.join(os.path.expanduser('~'), UPDATE_PACKAGE_NAME)

FINISH_FILE = os.path.join(UPDATE_DIR, 'finish')
VERSION_FILE = os.path.join(UPDATE_DIR, 'Version.txt')
NEXTSTART_SCRIPT = os.path.join(UPDATE_DIR, 'Nextstart.py')
UPDATE_SCRIPT = os.path.join(UPDATE_DIR, 'Update.py')
UPDATE_CHECK_SCRIPT = os.path.join(UPDATE_DIR, 'Check.py')
CREATE_NEW_CONSOLE = 0x00000010


def check_update_finish():
    """Prüft ob das Update abgeschlossen ist.

    :return: Boolean, True, wenn das Update abgeschlossen ist.
    """
    return os.path.exists(FINISH_FILE)


def update_dir_exists():
    """Prüft ob das Update-Verzeichnis existiert.

    :return: Boolean, True, wenn Update-Verzeichnis existiert.
    """
    return os.path.exists(UPDATE_DIR)


def delete_update_dir():
    """Löscht das Update-Verzeichnis."""
    if os.path.exists(UPDATE_DIR):
        shutil.rmtree(UPDATE_DIR)


def delete_home_update():
    """Löscht Update-Package aus Home-Verzeichnis."""
    os.remove(UPDATE_PACKAGE_HOME_PATH)


def check_nextstart_script_available():
    """Prüft ob das Skript zur Fortsetzung des Updates vorhanden ist.

    :return: Boolean, True, wenn das Skript vorhanden ist.
    """
    return os.path.isfile(NEXTSTART_SCRIPT)


def check_update_script_available():
    """Prüft ob das Skript zur Ausführung des Updates vorhanden ist.

    :return: Boolean, True, wenn das Skript vorhanden ist.
    """
    return os.path.isfile(UPDATE_SCRIPT)


def __start_python_process(script_path):
    if Global.CONST.ON_RPI:
        subprocess.Popen(['sudo', 'python', script_path, '&'])
    else:
        subprocess.Popen([sys.executable, script_path], creationflags=CREATE_NEW_CONSOLE)


def start_nextstart_process():
    """Startet die weitere Ausführung des Updates."""
    __start_python_process(NEXTSTART_SCRIPT)


def start_update_process():
    """Startet die Ausführung des Updates."""
    __start_python_process(UPDATE_SCRIPT)


def check_update_in_home_dir():
    """Prüft ob ein Update-Package im Home-Verzeichnis liegt."""
    return os.path.isfile(UPDATE_PACKAGE_HOME_PATH)


def check_update_on_usb_drive():
    """Prüft ob ein Update-Package auf einem angeschlossenem USB-Laufwerk liegt."""
    return os.path.isfile(get_usb_update_package_path())


def __unpack_update(update_package_path):
    with tarfile.open(update_package_path) as update_package:
        update_package.extractall(UPDATE_DIR)


def unpack_update_home_dir():
    """Entpackt das Update-Package aus dem Home-Verzeichnis."""
    __unpack_update(UPDATE_PACKAGE_HOME_PATH)


def unpack_update_usb_drive():
    """Entpackt das Update-Package von einem USB-Laufwerk."""
    __unpack_update(get_usb_update_package_path())


def check_version_greater(new_version, old_version):
    """Prüft ob new_version größer ist als old_version.

    :param new_version: String, Alte Versionsnummer
    :param old_version: String, Neue Versionsnummer
    :return: Boolean True, wenn new_version größer ist als old_version
    """
    if not Global.CONST.ON_RPI:
        # Just for testing purposes
        old_version = '0.5.0'
    if not re.match(r'\A\d+\.\d+\.\d+\Z', old_version):
        return False, u'Kann kein Update von Nightly-Build machen'
    if not re.match(r'\A\d+\.\d+\.\d+\Z', new_version):
        return False, u'Update kann nur auf ein Release-Build ausgeführt werden'
    if new_version == old_version:
        return False, u'Update kann nur auf eine neuere Version ausgeführt werden'
    old_version_numbers = [int(i) for i in old_version.split('.')]
    new_version_numbers = [int(i) for i in new_version.split('.')]
    for i in range(3):
        if new_version_numbers[i] > old_version_numbers[i]:
            return True, u'Version ist in Ordnung'
    return False, u'Update kann nur auf eine neuere Version ausgeführt werden'


def check_update_possible():
    """Prüft ob das Update ausgeführt werden kann.

    :return: Boolean, True, wenn das Update ausgeführt werden kann.
    """
    if not os.path.exists(UPDATE_SCRIPT):
        return False, u'Kein Update im Update-Package vorhanden'
    try:
        new_version = open(VERSION_FILE, 'r').read()
        answer = check_version_greater(new_version, Global.CONST.VERSION)
        if not answer[0]:
            return answer
    except Exception as e:
        message = u"Version des Update konnte nicht gelesen werden: " + unicode(e)
        Global.Instances.logger.log(message, __name__, Global.CONST.LOG.SEV.ERROR)
        return False, u"Version des Update konnte nicht gelesen werden"
    try:
        sys.path.insert(0, os.path.dirname(UPDATE_CHECK_SCRIPT))
        import Check
        return Check.Check(Global)
    except Exception as e:
        message = u'Failed to load or execute update check script: ' + unicode(e)
        Global.Instances.logger.log(message, __name__, Global.CONST.LOG.SEV.ERROR)
        return False, u'Failed to load or execute update check script.'


def get_update_confirmation_number():
    """Gibt eine zufällige vierstellige Nummer als String zurück. Zur Laufzeit ist diese Nummer immer gleich.

    :return: String, Vierstellige Nummer
    """
    if '__update_confirmation_number' in globals().keys():
        global __update_confirmation_number
        return __update_confirmation_number
    else:
        __update_confirmation_number = str(random.randint(0, 9999)).zfill(4)
        return __update_confirmation_number


def get_usb_update_package_path():
    """Gibt den Pfad eines Update-Package auf einem angeschlossenem USB-Laufwerk zurück.
    Leer, wenn kein USB-Laufwerk gefunden werden konnte.

    :return: String, Pfad zum Update-Package auf dem USB-Laufwerk
    """
    if not Global.CONST.ON_RPI:
        return u""
    try:
        lines = open("/proc/mounts", "r").readlines()
        path = None
        for line in lines:
            if line.startswith('/dev/sda1 '):
                path = line.split(" ")[1]
                break
        if path is None:
            return u""
        update_package_path = os.path.join(path, UPDATE_PACKAGE_NAME)
        if os.path.exists(update_package_path):
            return update_package_path
        else:
            return u""
    except Exception:
        return u""
