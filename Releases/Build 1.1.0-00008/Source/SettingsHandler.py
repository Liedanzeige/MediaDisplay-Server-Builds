# -*- coding: utf-8 -*-
"""Stellt eine Klassen zum Umgang mit Einstellungen zur Verfügung.
"""

import codecs
import json
import os

import Global
from SongHandler import Song


class GeneralSettingsHandler(object):
    """Handelt allgemeine Einstellungen. Auf die Einstellungen kann mit Properties zugegriffen werden. Sie werden 
    automatisch aus der Datei gelesen und gespeichert."""

    def __init__(self):
        self.__loaded = False
        if os.path.exists(Global.CONST.STH.GENERAL.FILEPATH):
            self.__change_time = os.path.getmtime(Global.CONST.STH.GENERAL.FILEPATH)
        else:
            self.__change_time = 0
        self.__settings = Global.CONST.STH.GENERAL.DEFAULT.copy()
        self.__load()

    def __load(self):
        """Einstellungen aus Datei laden."""
        write = False
        checked_settings = dict()
        if not os.path.exists(Global.CONST.STH.GENERAL.FILEPATH):
            self.__write()
        try:
            settings_file = codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'r', 'utf-8')
        except IOError:
            self.__loaded = False
            Global.Instances.logger.log("Error occurred reading general settings file.", __name__,
                                        Global.CONST.LOG.SEV.ERROR)
        else:
            unchecked_settings = Global.CONST.STH.GENERAL.DEFAULT.copy()
            try:
                unchecked_settings = json.loads(settings_file.read())
            except ValueError:
                Global.Instances.logger.log("Error occurred decoding general settings file.", __name__,
                                            Global.CONST.LOG.SEV.ERROR)
                unchecked_settings = self.__settings.copy()
                write = True
            finally:
                if not isinstance(unchecked_settings, dict):
                    unchecked_settings = dict()
                    write = True

                # time_1_5
                if 'time_1_5' in unchecked_settings:
                    checked_value = self.__check_time_1_5(unchecked_settings['time_1_5'])
                    if checked_value != unchecked_settings['time_1_5']:
                        write = True
                    checked_settings['time_1_5'] = checked_value
                else:
                    checked_settings['time_1_5'] = Global.CONST.STH.GENERAL.DEFAULT['time_1_5']
                    write = True

                # time_2_1
                if 'time_2_1' in unchecked_settings:
                    checked_value = self.__check_time_2_1(unchecked_settings['time_2_1'])
                    if checked_value != unchecked_settings['time_2_1']:
                        write = True
                    checked_settings['time_2_1'] = checked_value
                else:
                    checked_settings['time_2_1'] = Global.CONST.STH.GENERAL.DEFAULT['time_2_1']
                    write = True

                # time_3_2
                if 'time_3_2' in unchecked_settings:
                    checked_value = self.__check_time_3_2(unchecked_settings['time_3_2'])
                    if checked_value != unchecked_settings['time_3_2']:
                        write = True
                    checked_settings['time_3_2'] = checked_value
                else:
                    checked_settings['time_3_2'] = Global.CONST.STH.GENERAL.DEFAULT['time_3_2']
                    write = True

                # clock_idle
                if 'clock_idle' in unchecked_settings:
                    checked_value = self.__check_clock_idle(unchecked_settings['clock_idle'])
                    if checked_value != unchecked_settings['clock_idle']:
                        write = True
                    checked_settings['clock_idle'] = checked_value
                else:
                    checked_settings['clock_idle'] = Global.CONST.STH.GENERAL.DEFAULT['clock_idle']
                    write = True

                # clock_song
                if 'clock_song' in unchecked_settings:
                    checked_value = self.__check_clock_song(unchecked_settings['clock_song'])
                    if checked_value != unchecked_settings['clock_song']:
                        write = True
                    checked_settings['clock_song'] = checked_value
                else:
                    checked_settings['clock_song'] = Global.CONST.STH.GENERAL.DEFAULT['clock_song']
                    write = True

                # display_server
                if 'display_server' in unchecked_settings:
                    checked_value = self.__check_display_server(unchecked_settings['display_server'])
                    if checked_value != unchecked_settings['display_server']:
                        write = True
                    checked_settings['display_server'] = checked_value
                else:
                    checked_settings['display_server'] = Global.CONST.STH.GENERAL.DEFAULT['display_server']
                    write = True

                # order_solo
                if 'order_solo' in unchecked_settings:
                    checked_value = self.__check_order_solo(unchecked_settings['order_solo'])
                    if checked_value != unchecked_settings['order_solo']:
                        write = True
                    checked_settings['order_solo'] = checked_value
                else:
                    checked_settings['order_solo'] = Global.CONST.STH.GENERAL.DEFAULT['order_solo']
                    write = True

                # general_song_books
                if 'general_song_books' in unchecked_settings:
                    checked_value = self.__check_general_song_books(unchecked_settings['general_song_books'])
                    if checked_value != unchecked_settings['general_song_books']:
                        write = True
                    checked_settings['general_song_books'] = checked_value
                else:
                    checked_settings['general_song_books'] = Global.CONST.STH.GENERAL.DEFAULT['general_song_books']
                    write = True

                self.__settings = checked_settings
                self.__loaded = True

        if write:
            self.__write()

    def __write(self):
        """Einstellungen in Datei schreiben."""
        try:
            settings_file = codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8')
        except IOError:
            Global.Instances.logger.log("Error occurred writing general settings file.", __name__,
                                        Global.CONST.LOG.SEV.ERROR)
        else:
            settings_file.write(json.dumps(self.__settings))
            settings_file.close()
            self.__change_time = os.path.getmtime(Global.CONST.STH.GENERAL.FILEPATH)

    def __check_reload(self):
        """Prüft ob die Einstellungsdatei sich geändert hat und neu geladen werden muss."""
        if not os.path.exists(Global.CONST.STH.GENERAL.FILEPATH):
            return True
        return not self.__loaded or self.__change_time != os.path.getmtime(Global.CONST.STH.GENERAL.FILEPATH)

    def load_defaults(self):
        """Setzt alles auf Defaulteinstellungen zurück."""
        self.__settings = Global.CONST.STH.GENERAL.DEFAULT.copy()
        self.__write()

    @property
    def time_1_5(self):
        """Übergangszeit Idle -> Standby."""
        if self.__check_reload():
            self.__load()
        return self.__settings['time_1_5']

    @time_1_5.setter
    def time_1_5(self, value):
        checked_value = self.__check_time_1_5(value)
        if checked_value == self.__settings['time_1_5']:
            return
        else:
            self.__settings['time_1_5'] = checked_value
            self.__write()

    def __check_time_1_5(self, value):
        try:
            float_value = float(value)
        except ValueError:
            return self.__settings['time_1_5']
        else:
            if float_value != 0.0:
                float_value = max(float_value, Global.CONST.STH.GENERAL.TIME_1_5_MIN)
            float_value = min(float_value, Global.CONST.STH.GENERAL.TIME_1_5_MAX)
            return float_value

    @property
    def time_2_1(self):
        """Übergangszeit Liedanzeige -> Idle."""
        if self.__check_reload():
            self.__load()
        return self.__settings['time_2_1']

    @time_2_1.setter
    def time_2_1(self, value):
        checked_value = self.__check_time_2_1(value)
        if checked_value == self.__settings['time_2_1']:
            return
        else:
            self.__settings['time_2_1'] = checked_value
            self.__write()

    def __check_time_2_1(self, value):
        try:
            float_value = float(value)
        except ValueError:
            return self.__settings['time_2_1']
        else:
            float_value = max(float_value, Global.CONST.STH.GENERAL.TIME_2_1_MIN)
            float_value = min(float_value, Global.CONST.STH.GENERAL.TIME_2_1_MAX)
            return float_value

    @property
    def time_3_2(self):
        """Übergangszeit Nummereingabe -> Liedanzeige."""
        if self.__check_reload():
            self.__load()
        return self.__settings['time_3_2']

    @time_3_2.setter
    def time_3_2(self, value):
        checked_value = self.__check_time_3_2(value)
        if checked_value == self.__settings['time_3_2']:
            return
        else:
            self.__settings['time_3_2'] = checked_value
            self.__write()

    def __check_time_3_2(self, value):
        try:
            float_value = float(value)
        except ValueError:
            return self.__settings['time_3_2']
        else:
            float_value = max(float_value, Global.CONST.STH.GENERAL.TIME_3_2_MIN)
            float_value = min(float_value, Global.CONST.STH.GENERAL.TIME_3_2_MAX)
            return float_value

    @property
    def clock_idle(self):
        """Analog- oder Digitaluhr in Idle."""
        if self.__check_reload():
            self.__load()
        return self.__settings['clock_idle']

    @clock_idle.setter
    def clock_idle(self, value):
        checked_value = self.__check_clock_idle(value)
        if checked_value == self.__settings['clock_idle']:
            return
        else:
            self.__settings['clock_idle'] = checked_value
            self.__write()

    def __check_clock_idle(self, value):
        if value not in Global.CONST.STH.GENERAL.CLOCK_IDLE_ALLOWED:
            return self.__settings['clock_idle']
        else:
            return value

    @property
    def clock_song(self):
        """Uhranzeige in Liedanzeige (Aus/An)."""
        if self.__check_reload():
            self.__load()
        return self.__settings['clock_song']

    @clock_song.setter
    def clock_song(self, value):
        checked_value = self.__check_clock_song(value)
        if checked_value == self.__settings['clock_song']:
            return
        else:
            self.__settings['clock_song'] = checked_value
            self.__write()

    def __check_clock_song(self, value):
        if value not in Global.CONST.STH.GENERAL.CLOCK_SONG_ALLOWED:
            return self.__settings['clock_song']
        else:
            return value

    @property
    def display_server(self):
        """DisplayServer (Aus/An)."""
        if self.__check_reload():
            self.__load()
        return self.__settings['display_server']

    @display_server.setter
    def display_server(self, value):
        checked_value = self.__check_display_server(value)
        if checked_value == self.__settings['display_server']:
            return
        else:
            self.__settings['display_server'] = checked_value
            self.__write()

    def __check_display_server(self, value):
        if value not in Global.CONST.STH.GENERAL.DISPLAY_SERVER_ALLOWED:
            return self.__settings['display_server']
        else:
            return value

    @property
    def order_solo(self):
        """Reihenfolge der Solostimmen"""
        if self.__check_reload():
            self.__load()
        return self.__settings['order_solo']

    @order_solo.setter
    def order_solo(self, value):
        checked_value = self.__check_order_solo(value)
        if checked_value == self.__settings['order_solo']:
            return
        else:
            self.__settings['order_solo'] = checked_value
            self.__write()

    def __check_order_solo(self, value):
        if value not in Global.CONST.STH.GENERAL.ORDER_SOLO_ALLOWED:
            return self.__settings['order_solo']
        else:
            return value

    @property
    def general_song_books(self):
        """Allgemeine Liederbücher"""
        if self.__check_reload():
            self.__load()
        return self.__settings['general_song_books']

    @general_song_books.setter
    def general_song_books(self, value):
        checked_value = self.__check_general_song_books(value)
        if checked_value == self.__settings['general_song_books']:
            return
        else:
            self.__settings['general_song_books'] = checked_value
            self.__write()

    def __check_general_song_books(self, value):
        if isinstance(value, (str, unicode)):
            value = value.strip(Global.CONST.SCH.DELIMITER).split(Global.CONST.SCH.DELIMITER)
        if hasattr(value, '__iter__'):
            new_value = list()
            for raw_book in value:
                book, book_abbreviation, book_color, number, title, solos = \
                    Song.parse_string(raw_book + Global.CONST.SCH.DELIMITER + '1')
                if book not in (Global.CONST.SOH.INVALID_BOOK, ''):
                    new_value.append(book)
            return new_value
        else:
            return self.__settings['general_song_books']
