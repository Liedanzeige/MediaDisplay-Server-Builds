var DOCUMENTATION_OPTIONS = {
    URL_ROOT: '',
    VERSION: '1.1.0-00008',
    LANGUAGE: 'de',
    COLLAPSE_INDEX: false,
    FILE_SUFFIX: '.html',
    HAS_SOURCE: true,
    SOURCELINK_SUFFIX: '.txt'
};