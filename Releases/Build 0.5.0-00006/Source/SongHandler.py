# -*- coding: utf-8 -*-
"""Dieses Modul stellt einige Klassen zur Verwaltung von Liedern zur Verfügung.
"""

import Global

import codecs
import datetime
import re
import os


class SongHandler(object):
    """Klasse zur Verwaltung der angezeigten Lieder.
    """

    def __init__(self):
        self.sung_songs = list()
        if len(self.sung_songs) > 100:
            self.sung_songs = self.sung_songs[:100]

    def has_song(self):
        """Gibt zurück ob ein Lied nach dem Start des Programms angezeigt wurde.
        
        :return: Bool; True, wenn mindestens ein Lied angezeigt wurde.
        """
        return bool(self.sung_songs)

    def sing_song(self, song):
        """Fügt ein Lied der Liste der gesungenen Lieder hinzu.
        
        :param song: Lied das angezeigt wird.
        """
        self.sung_songs.append(song)

    def get_latest_song(self):
        """Gibt das zuletzt angezeigte Lied zurück.
        
        :return: Song; Zuletzt angezeigtes Lied oder None wenn noch kein Lied gesungen wurde.
        """
        if self.sung_songs:
            return self.sung_songs[-1]
        else:
            return None

    def update_latest_song(self, song):
        """Ändert das zuletzt angezeigte Lied. Kann verwendet werden, wenn z.B. die Solostimmen sich geändert haben.
        
        :param song: Lied das angezeigt wird.
        """
        self.sung_songs[-1] = song


class Song(object):
    """Repräsentiert ein Lied"""

    __toggle_dict = {'0': '1', '1': '0'}

    def __init__(self, data):
        self.number = ''
        self.title = ''
        self.book_id = ''
        self.book_abbreviation = ''
        self.book_color = Global.CONST.SCH.COLOR.BOOK
        self.sopran = '0'
        self.alt = '0'
        self.tenor = '0'
        self.bass = '0'
        self.sopran_name = u'Sopran'
        self.alt_name = u'Alt'
        self.tenor_name = u'Tenor'
        self.bass_name = u'Bass'
        self.general_song = False
        self.date = datetime.datetime.now()

        if isinstance(data, (str, unicode)):
            self.book_id, self.book_abbreviation, self.book_color, self.number, self.title, solos = \
                self.parse_string(data)
            self.sopran, self.alt, self.tenor, self.bass = solos
            if Global.Instances.general_settings is not None:
                self.general_song = self.book_id in Global.Instances.general_settings.general_song_books

    @classmethod
    def parse_string(cls, data):
        """Parst Daten für das Lied aus einem String.
        
        :param data: String, der geparst wird.
        :return: Tuple: (book, book_abbreviation, book_color, number, title, (sopran, alt, tenor, bass))
        """
        sopran = '0'
        alt = '0'
        tenor = '0'
        bass = '0'
        has_solos = False
        book_color = Global.CONST.SCH.COLOR.BOOK

        temp = data.split(Global.CONST.SCH.DELIMITER)

        if len(temp) == 1:
            number = temp[0]
            book = ''
        elif len(temp) == 2:
            if temp[1] == '':
                number = temp[0]
                book = ''
            else:
                number = temp[1]
                book = temp[0]
        elif len(temp) >= 3:
            has_solos = True
            if temp[1] == '':
                number = temp[0]
                book = ''
            else:
                number = temp[1]
                book = temp[0]
            if '1' in temp[2]:
                sopran = '1'
            if '2' in temp[2]:
                alt = '1'
            if '3' in temp[2]:
                tenor = '1'
            if '4' in temp[2]:
                bass = '1'
        else:
            number = ''
            book = ''

        # check book number
        if book != '':
            try:
                book_nr = int(book)
            except ValueError:
                book = Global.CONST.SOH.INVALID_BOOK
            else:
                if book_nr < Global.CONST.SCH.MIN_BOOK:
                    book = Global.CONST.SOH.INVALID_BOOK
                elif book_nr > Global.CONST.SCH.MAX_BOOK:
                    book = Global.CONST.SOH.INVALID_BOOK
                else:
                    if book.startswith('0'):
                        book = '0' + book.lstrip('0')
                    else:
                        book = book.lstrip('0')

        book_abbreviation = book
        title = u''

        if os.path.isfile(os.path.join(Global.CONST.BOOK_DIR, book + '.txt')):
            try:
                book_file = codecs.open(os.path.join(Global.CONST.BOOK_DIR, book + '.txt'), 'r', 'utf-8')
                lines = book_file.readlines()
                book_abbreviation = lines[0].strip().split(' ')[0]
                book_color = Global.CONST.SCH.COLOR.BOOK
                color = lines[0].strip().split(' ')[1]
                if cls.__check_color(color):
                    book_color = '#' + color
                for line in lines[1:]:
                    if line.startswith(number + ' '):
                        splitted_line = line.strip().split(' ')
                        number = splitted_line[1]
                        if not has_solos:
                            if '1' in splitted_line[2]:
                                sopran = '1'
                            else:
                                sopran = '0'
                            if '2' in splitted_line[2]:
                                alt = '1'
                            else:
                                alt = '0'
                            if '3' in splitted_line[2]:
                                tenor = '1'
                            else:
                                tenor = '0'
                            if '4' in splitted_line[2]:
                                bass = '1'
                            else:
                                bass = '0'
                        title = u' '.join(splitted_line[3:])
            except Exception:
                pass

        return book, book_abbreviation, book_color, number, title, (sopran, alt, tenor, bass)

    def is_valid(self):
        """Gibt zurück ob das Lied valide ist.
        
        :return: Boolean; Wahr, wenn das Lied entweder eine Nummer oder ein Buch hat.
        """
        return bool(self.book_abbreviation) or bool(self.number)

    @staticmethod
    def __check_color(color):
        """Gibt zurück ob die angegebene Farbe valider Hexcode ist.
        
        :param color: Farbe die überprüft wird.
        :return: Booolean; Wahr, wenn color eine valider Hexcode ist.
        """
        return bool(re.match(r'\A[0123456789abcdefABCDEF]{6}\Z', color))

    def get_label_text(self):
        """Gibt einen Text für ein Label der GUI zurück.
        
        :return: String; Text für ein Label der GUI.
        """
        if self.book_abbreviation:
            return u'<span style="color:' + self.book_color + u';">' + self.book_abbreviation + u'</span>' + u' ' + \
                   self.number
        else:
            return self.number

    def toggle_solo_sopran(self):
        """Schaltet um ob die Solostimme "Sopran" angezeigt wird."""
        self.sopran = self.__toggle_dict[self.sopran]

    def toggle_solo_alt(self):
        """Schaltet um ob die Solostimme "Alt" angezeigt wird."""
        self.alt = self.__toggle_dict[self.alt]

    def toggle_solo_tenor(self):
        """Schaltet um ob die Solostimme "Tenor" angezeigt wird."""
        self.tenor = self.__toggle_dict[self.tenor]

    def toggle_solo_bass(self):
        """Schaltet um ob die Solostimme "Bass" angezeigt wird."""
        self.bass = self.__toggle_dict[self.bass]
