# -*- coding: utf-8 -*-

import time
import threading

import Global


class ScreenHandler(object):
    voice_dict = {'1': 'Sopran',
                  '2': 'Alt',
                  '3': 'Tenor',
                  '4': 'Bass'
                  }

    color_dict = {'Sopran': 'white',
                  'Alt': 'black',
                  'Tenor': 'black',
                  'Bass': 'white',
                  'Sopran-BG': 'red',
                  'Alt-BG': 'yellow',
                  'Tenor-BG': 'green',
                  'Bass-BG': 'blue',
                  'Black': 'black'
                  }

    def __init__(self):
        self.__run = False
        self.__cache = ''
        self.__current_song = {
            'number': '-',
            'book_id': '-',
            'book_ab': '-',
            'book': '',
            'title': '',
            'Sopran': '0',
            'Alt': '0',
            'Tenor': '0',
            'Bass': '0',
            'SopranName': 'Sopran',
            'AltName': 'Alt',
            'TenorName': 'Tenor',
            'BassName': 'Bass'
        }
        self.__gui_inited = False
        self.__gui_init_finish = False
        self.__thread = None

    def init_values(self):
        self.__cache = ''
        self.__current_song = {
            'number': '-',
            'book_id': '-',
            'book_ab': '-',
            'book': '',
            'title': '',
            'Sopran': '0',
            'Alt': '0',
            'Tenor': '0',
            'Bass': '0',
            'SopranName': 'Sopran',
            'AltName': 'Alt',
            'TenorName': 'Tenor',
            'BassName': 'Bass'
        }
        self.__gui_inited = False
        self.__gui_init_finish = False
        self.__thread = None

    @staticmethod
    def __update_song_screen(data):
        book = data['book_ab']
        number = data['number']
        sopran = data['Sopran']
        alt = data['Alt']
        tenor = data['Tenor']
        bass = data['Bass']

        label_book = Global.Instances.main_window.ids['BookTitle']
        label_number = Global.Instances.main_window.ids['SongNumber']
        label_sopran = Global.Instances.main_window.ids['Sopran']
        label_alt = Global.Instances.main_window.ids['Alt']
        label_tenor = Global.Instances.main_window.ids['Tenor']
        label_bass = Global.Instances.main_window.ids['Bass']

        # label_book.set_text.emit(book)
        # label_book.fit_font_size.emit()
        label_number.set_text.emit('<span style="color:#ffff00;">' + book + '</span>' + ' ' + number)
        label_number.fit_font_size.emit()

        if sopran == '1':
            label_sopran.set_text.emit(data['SopranName'])
            label_sopran.set_style_sheet.emit("QLabel { background-color : " + ScreenHandler.color_dict['Sopran-BG'] +
                                              "; color: " + ScreenHandler.color_dict['Sopran'] + " }")
        else:
            label_sopran.set_text.emit('')
            label_sopran.set_style_sheet.emit("QLabel { background-color : black; }")
        label_sopran.fit_font_size.emit()

        if alt == '1':
            label_alt.set_text.emit(data['AltName'])
            label_alt.set_style_sheet.emit("QLabel { background-color : " + ScreenHandler.color_dict['Alt-BG'] +
                                           "; color: " + ScreenHandler.color_dict['Alt'] + " }")
        else:
            label_alt.set_text.emit('')
            label_alt.set_style_sheet.emit("QLabel { background-color : black; }")
        label_alt.fit_font_size.emit()

        if tenor == '1':
            label_tenor.set_text.emit(data['TenorName'])
            label_tenor.set_style_sheet.emit("QLabel { background-color : " + ScreenHandler.color_dict['Tenor-BG'] +
                                             "; color: " + ScreenHandler.color_dict['Tenor'] + " }")
        else:
            label_tenor.set_text.emit('')
            label_tenor.set_style_sheet.emit("QLabel { background-color : black; }")
        label_tenor.fit_font_size.emit()

        if bass == '1':
            label_bass.set_text.emit(data['BassName'])
            label_bass.set_style_sheet.emit("QLabel { background-color : " + ScreenHandler.color_dict['Bass-BG'] +
                                            "; color: " + ScreenHandler.color_dict['Bass'] + " }")
        else:
            label_bass.set_text.emit('')
            label_bass.set_style_sheet.emit("QLabel { background-color : black; }")
        label_bass.fit_font_size.emit()

    def add_number(self, number):
        if number in Global.CONST.SM.IRCMD.N_LIST:
            self.__cache += number
        else:
            self.__cache += '+'
        label = Global.Instances.main_window.ids['Number']
        label.set_text.emit(self.__cache)
        label.fit_font_size.emit()

    def delete_number(self):
        if self.__cache:
            self.__cache = self.__cache[:-1]
        label = Global.Instances.main_window.ids['Number']
        label.set_text.emit(self.__cache)
        label.fit_font_size.emit()

    def get_current_song(self):
        return self.__current_song.copy()

    def toggle_solo(self, voice):
        if Global.Instances.main_window.current == Global.CONST.SM.STATE_SONG:
            voice = ScreenHandler.voice_dict[voice]
            label = Global.Instances.main_window.ids[voice]

            if self.__current_song[voice] == '0':
                self.__current_song[voice] = '1'
            else:
                self.__current_song[voice] = '0'

            if self.__current_song[voice] == '1':
                label.set_text.emit(self.__current_song[voice + 'Name'])
                label.set_style_sheet.emit(
                    "QLabel { background-color : " + ScreenHandler.color_dict[voice + '-BG'] +
                    "; color: " + ScreenHandler.color_dict[voice] + " }")
            else:
                label.set_text.emit('')
                label.set_style_sheet.emit("QLabel { background-color : black; }")
            label.fit_font_size.emit()

    def set_page(self, target_state, gui_params):
        Global.Instances.main_window.current = target_state
        if target_state == Global.CONST.SM.STATE_START:
            label = Global.Instances.main_window.ids['LabelStartLiedanzeige']
            label.fit_font_size.emit()
            label = Global.Instances.main_window.ids['LabelStartVersion']
            label.fit_font_size.emit()
            label = Global.Instances.main_window.ids['LabelStartCopyright']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_IDLE:
            pass
        elif target_state == Global.CONST.SM.STATE_SONG:
            if gui_params is None:
                temp = self.__cache.split('+')
                if len(temp) == 1:
                    number = temp[0]
                    book = ''
                elif len(temp) >= 2:
                    number = temp[1]
                    book = temp[0]
                else:
                    number = ''
                    book = ''
                data = dict()
                data['book'] = book
                data['book_ab'] = book
                data['book_id'] = book
                data['number'] = number
                data['title'] = number
                data['Sopran'] = '0'
                data['Alt'] = '0'
                data['Tenor'] = '0'
                data['Bass'] = '0'
                data['SopranName'] = 'Sopran'
                data['AltName'] = 'Alt'
                data['TenorName'] = 'Tenor'
                data['BassName'] = 'Bass'
                self.__current_song = data
                self.__update_song_screen(data)
            elif gui_params == 'show_again':
                pass
            else:
                pass

            if 'SongScreenClock' in Global.Instances.main_window.ids:
                label = Global.Instances.main_window.ids['SongScreenClock']
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_NUMBER:
            self.__cache = gui_params
            label = Global.Instances.main_window.ids['Number']
            label.set_text.emit(self.__cache)
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_BLUESCREEN:
            label = Global.Instances.main_window.ids['Oops']
            label.fit_font_size.emit()
            label = Global.Instances.main_window.ids['BluescreenLabel']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_STANDBY:
            label = Global.Instances.main_window.ids['StandbyLabel']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_QUIT:
            label = Global.Instances.main_window.ids['LabelQuit']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_CONFIRM_QUIT:
            for label in Global.Instances.main_window.ids['LabelsConfirmQuitList']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_TIME:
            for label in Global.Instances.main_window.ids['LabelsSetTimeList']:
                label.fit_font_size.emit()
            self.__cache = ''
            self.show_time_str()
        elif target_state == Global.CONST.SM.STATE_SET_DATE:
            for label in Global.Instances.main_window.ids['LabelsSetDateList']:
                label.fit_font_size.emit()
            self.__cache = ''
            self.show_date_str()

    def add_time_char(self, char):
        if char:
            if len(self.__cache) < 6:
                self.__cache = self.__cache + char
        else:
            if self.__cache:
                self.__cache = self.__cache[:-1]
        self.show_time_str()

    def show_time_str(self):
        length = len(self.__cache)
        if length == 0:
            text = '--:--:--'
        elif length == 1:
            text = self.__cache + '-:--:--'
        elif length == 2:
            text = self.__cache + ':--:--'
        elif length == 3:
            text = self.__cache[:2] + ':' + self.__cache[2:] + '-:--'
        elif length == 4:
            text = self.__cache[:2] + ':' + self.__cache[2:] + ':--'
        elif length == 5:
            text = self.__cache[:2] + ':' + self.__cache[2:4] + ':' + self.__cache[4:] + '-'
        elif length == 6:
            text = self.__cache[:2] + ':' + self.__cache[2:4] + ':' + self.__cache[4:]
        else:
            text = self.__cache

        label = Global.Instances.main_window.ids['LabelSetTime']
        label.set_text.emit(text)
        label.fit_font_size.emit()

    def set_time(self):
        if len(self.__cache) == 4:
            time_str = self.__cache[:2] + ':' + self.__cache[2:4] + ':00'
            Global.Instances.os_interface.set_date_time(time_str)
        elif len(self.__cache) == 6:
            time_str = self.__cache[:2] + ':' + self.__cache[2:4] + ':' + self.__cache[4:]
            Global.Instances.os_interface.set_date_time(time_str)

    def add_date_char(self, char):
        if char:
            if len(self.__cache) < 8:
                self.__cache = self.__cache + char
        else:
            if self.__cache:
                self.__cache = self.__cache[:-1]
        self.show_date_str()

    def show_date_str(self):
        length = len(self.__cache)
        if length == 0:
            text = '--.--.----'
        elif length == 1:
            text = self.__cache + '-.--.----'
        elif length == 2:
            text = self.__cache + '.--.----'
        elif length == 3:
            text = self.__cache[:2] + '.' + self.__cache[2:] + '-.----'
        elif length == 4:
            text = self.__cache[:2] + '.' + self.__cache[2:] + '.----'
        elif length == 5:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:] + '---'
        elif length == 6:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:] + '--'
        elif length == 7:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:] + '-'
        elif length == 8:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:]
        else:
            text = self.__cache

        label = Global.Instances.main_window.ids['LabelSetDate']
        label.set_text.emit(text)
        label.fit_font_size.emit()

    def set_date(self):
        if len(self.__cache) == 8:
            time_str = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:]
            Global.Instances.os_interface.set_date_time(time_str)

    def is_running(self):
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        self.__gui_init_finish = True
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'ScreenHandler')
            self.__thread.start()

    def stop(self):
        self.__run = False

    def __update(self):
        if self.__gui_init_finish:
            # clock
            if Global.Instances.main_window.current == Global.CONST.SM.STATE_IDLE:
                pass
            # song
            elif Global.Instances.main_window.current == Global.CONST.SM.STATE_SONG:
                pass

    def __loop(self):
        while self.__run:
            self.__update()
            time.sleep(Global.CONST.SCH.TIME_INCREMENT)
