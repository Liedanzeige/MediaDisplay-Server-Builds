# -*- coding: utf-8 -*-

from datetime import datetime as dt
import random
import string

import Global


class SessionHandler(object):
    valid_token_chars = []
    valid_token_chars.extend([i for i in string.ascii_letters])
    valid_token_chars.extend([str(i) for i in range(10)])

    def __init__(self):
        self.sessions = []
        self.pws = PasswordSave()

    def login(self, name, password):
        self.update_sessions()
        if self.pws.check_password(name, password):
            session = Session(self)
            self.sessions.append(session)
            return True, session.token
        else:
            return False, Global.CONST.AI.ERROR0

    def check_session(self, token, action=False):
        self.update_sessions()
        if token is None:
            return False
        session = None
        for session_ in self.sessions:
            if session is None and session_.token == token:
                session = session_
            elif session is not None:
                pass
        if session is None:
            return False
        else:
            if action:
                session.action()
            return True

    def update_sessions(self):
        sessions = self.sessions
        now = dt.now()
        for session in sessions:
            session_time = now - session.last_action
            if session_time.total_seconds() > Global.CONST.SH.SESSION_LIFETIME:
                try:
                    self.sessions.remove(session)
                    del session
                except Exception as e:
                    print

    @classmethod
    def generate_token(cls, length=60):
        token = ''
        for i in range(int(length)):
            token += random.choice(SessionHandler.valid_token_chars)
        return token

    def has_token(self, token):
        if any([session.token == token for session in self.sessions]):
            return True
        return False

    def logout(self, token):
        self.update_sessions()
        sessions = self.sessions
        for session in sessions:
            if session.token == token:
                try:
                    self.sessions.remove(session)
                    del session
                except Exception as e:
                    print e


class Session(object):

    def __init__(self, master):
        self.master = master
        token = SessionHandler.generate_token()
        while master.has_token(token):
            token = SessionHandler.generate_token()
        self.token = token
        self.last_action = dt.now()

    def action(self):
        self.last_action = dt.now()


class PasswordSave(object):

    def __init__(self):
        self.users = {'admin': 'admin', 'dirigent': 'dirigent'}

    def check_password(self, name, password):
        for key, value in self.users.iteritems():
            if key == name and value == password:
                return True
        return False
