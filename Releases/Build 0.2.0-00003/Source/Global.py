# -*- coding: utf-8 -*-

import json
import inspect
import os


class CONST(object):
    VERSION = '0.2.0'
    BUILD = '00003'
    COMMIT = '5e94499ad9cb00d09e56f6b7ad92aae1893bc0db'
    SCRIPT_DIR = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    USER_DATA_DIR = os.path.join(SCRIPT_DIR, '..', 'user_data')
    DATA_DIR = os.path.join(SCRIPT_DIR, 'data')
    FONTS_DIR = os.path.join(DATA_DIR, 'Fonts')
    try:
        if os.uname()[4].startswith("arm"):
            ON_RPI = True
        else:
            ON_RPI = False
    except AttributeError:
        ON_RPI = False
    TEST = os.path.exists('TestExecutionQueue.txt')

    class OSI(object):  # os dependent functions (especially hardware stuff)
        LIRC_CONFIG_FILE_PATH = os.path.join(os.path.dirname(os.path.abspath(
            inspect.getfile(inspect.currentframe()))), u'data/lircrc')
        RED_LED = 33
        GREEN_LED = 35
        BLUE_LED = 37
        TIME_INCREMENT_LED = 0.01
        TIME_INCREMENT_UTIL = 1.0

        class COLORS(object):
            RED = [100, 0, 0]
            GREEN = [0, 100, 0]
            BLUE = [0, 0, 100]
            WHITE = [100, 100, 100]
            BLACK = [0, 0, 0]
            ORANGE = [100, 50, 0]
            PURPLE = [100, 0, 100]
            CYAN = [0, 100, 100]
            YELLOW = [100, 100, 0]
            OFF = BLACK

    class WS(object):  # web server
        PORT = 58080

    class DS(object):  # display server
        PORT = 48080

    class CS(object):  # command server
        PORT = 8080

    class AI(object):  # ajax interface
        ERROR0 = json.dumps({u'ok': False, u'errnr': 0, u'error': u'Invalid name or password.'})
        ERROR1 = json.dumps({u'ok': False, u'errnr': 1, u'error': u'User already logged in.'})
        ERROR2 = json.dumps({u'ok': False, u'errnr': 2, u'error': u'Not logged in.'})
        ERROR3 = json.dumps({u'ok': False, u'errnr': 3, u'error': u'No such command.'})
        ERROR4 = json.dumps({u'ok': False, u'errnr': 4, u'error': u'Book not found.'})
        ERROR5 = json.dumps({u'ok': False, u'errnr': 5, u'error': u'Song not found.'})
        ERROR6 = json.dumps({u'ok': False, u'errnr': 6, u'error': u'No song.'})

    class SH(object):  # session handler
        SESSION_LIFETIME = 300.0  # in seconds

    class SM(object):
        STATE_START = 0
        STATE_IDLE = 1
        STATE_SONG = 2
        STATE_NUMBER = 3
        STATE_BLUESCREEN = 4
        STATE_STANDBY = 5
        STATE_QUIT = 6
        STATE_CONFIRM_QUIT = 7
        STATE_SET_TIME = 8
        STATE_SET_DATE = 9
        STATE_SET_CLOCK_IDLE = 10
        STATE_SET_CLOCK_SONG = 11
        STATE_SET_TIME_NUMBER_SONG = 12
        STATE_SET_TIME_SONG_IDLE = 13
        STATE_SET_TIME_IDLE_STANDBY = 14
        STATE_SET_LOG_LEVEL = 15
        STATE_SYSTEM_INFORMATION = 16
        STATE_IP = 17
        STATE_DICT = {
            0: u'Start',
            1: u'Zeige Uhr',
            2: u'Zeige Lied',
            3: u'Zeige Zahl',
            4: u'Bluescreen',
            5: u'Standby',
            6: u'Beenden',
            7: u'Bestätige Beenden',
            8: u'Stelle Zeit',
            9: u'Stelle Datum',
            10: u'Stelle Uhranzeige Uhr',
            11: u'Stelle Uhranzeige Lied',
            12: u'Stelle Dauer Nummer->Lied',
            13: u'Stelle Dauer Lied->Uhr',
            14: u'Stelle Dauer Uhr->Standby',
            15: u'Stelle LogLevel',
            16: u'Systeminformationen',
            17: u'QR IP',
        }
        CMD_SYS = 0
        CMD_IR = 1
        CMD_NET = 2

        class IRCMD(object):
            N1 = '1'
            N2 = '2'
            N3 = '3'
            N4 = '4'
            N5 = '5'
            N6 = '6'
            N7 = '7'
            N8 = '8'
            N9 = '9'
            N0 = '0'
            N4_LIST = [N1,
                       N2,
                       N3,
                       N4
                       ]
            N_LIST = [N1,
                      N2,
                      N3,
                      N4,
                      N5,
                      N6,
                      N7,
                      N8,
                      N9,
                      N0
                      ]
            N1_9_LIST = [N1,
                         N2,
                         N3,
                         N4,
                         N5,
                         N6,
                         N7,
                         N8,
                         N9,
                         ]
            PLUS = '+'
            MINUS = '-'
            OK = 'ok'
            EXIT = 'exit'
            POWER = 'power'
            CONFIRM_STANDBY = '1'
            CONFIRM_SHUTDOWN = '2'
            CONFIRM_REBOOT = '3'
            CONFIRM_QUIT = '9'
            DELIMITER = '+'
            UP = 'up'
            DOWN = 'down'
            LEFT = 'left'
            RIGHT = 'right'
            BACK = 'back'
            MENU = 'menu'

        class TIME(object):
            CHANGE_1_5 = 3600.0
            CHANGE_2_1 = 120.0
            CHANGE_3_2 = 5.0
            CHANGE_7_1 = 30.0
            CHANGE_SETTINGS = 30.0

        TIME_INCREMENT = 0.01

        NO_SHUTDOWN = 0
        SHUTDOWN = 1
        REBOOT = 2

    LOG = None

    class SCH(object):
        TIME_INCREMENT = 0.001

    class WD(object):
        TIME_INCREMENT = 5


class __LOG(object):  # logger

    TIME_INCREMENT = 1.0
    DIR = os.path.join(CONST.USER_DATA_DIR, 'log')
    SEVERITY_DICT = {
        0: 'Debug',
        1: 'Info',
        2: 'Warning',
        3: 'Error'
    }

    class SEV(object):
        DEBUG = 0
        INFO = 1
        WARNING = 2
        ERROR = 3

CONST.LOG = __LOG


class Instances(object):
    app = None
    logger = None
    main_window = None
    screen_handler = None
    session_handler = None
    song_handler = None
    ajax_interface = None
    command_server = None
    display_server = None
    web_server = None
    state_machine = None
    os_interface = None
    test_runner = None
    watchdog = None
