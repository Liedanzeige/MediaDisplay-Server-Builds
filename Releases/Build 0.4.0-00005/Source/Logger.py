# -*- coding: utf-8 -*-

from datetime import datetime as dt
import threading
import time
import codecs
import os
import sys
import re

import Global


class Logger(object):
    """Stellt Funktionen für das Logging zu Verfügung."""

    def __init__(self):
        self.__last_update_time = dt.now()
        self.__run = False
        self.__thread = None

    @staticmethod
    def log(message, module_name, severity=Global.CONST.LOG.SEV.INFO):
        """Schreiben von Log-Meldungen
        
        :param message: String Nachricht, die geloggt werden soll.
        :param module_name: String Names des Modules, das die Log-Meldung schreibt.
        :param severity: int Severity der Log-Meldung
        """
        now_str = str(dt.now())
        if len(now_str) <= 19:
            now_str += '.000000'
        now_str = now_str[0:23]
        log_file_name = os.path.join(Global.CONST.LOG.DIR, 'MediaDisplay-Server %s.log' % now_str[:10])
        log_message = u'%s\t%s\t%s\t%s\n' % (now_str, module_name, Global.CONST.LOG.SEVERITY_DICT[severity],
                                             message.replace('\n', ''))
        try:
            log_file = codecs.open(log_file_name, 'a+', 'utf-8')
            log_file.write(log_message)
        except IOError:
            sys.stderr.write('Error writing to log file')
            sys.stderr.write(repr(log_message))

    def cleanup(self):
        """Räumt logfiles, die älter als 31 Tage, sind auf."""
        now = dt.now()
        errors = 0
        deleted = 0
        for i in os.listdir(Global.CONST.LOG.DIR):
            if re.match(r'MediaDisplay-Server \d\d\d\d-\d\d-\d\d\.log', i):
                name_list = i[20:30].split('-')
                date = dt(int(name_list[0]), int(name_list[1]), int(name_list[2]))
                delta = now - date
                if delta.days > 31:
                    try:
                        os.remove(os.path.join(Global.CONST.LOG.DIR, i))
                        deleted += 1
                    except OSError:
                        errors += 1
        if errors > 0:
            log_sev = Global.CONST.LOG.SEV.WARNING
        else:
            log_sev = Global.CONST.LOG.SEV.INFO
        self.log('Cleaned log files with %i errors. %i files deleted.' % (errors, deleted),
                 __name__, log_sev)

    def is_running(self):
        """Gibt zurück, ob die Updateschleife läuft.
        
        :return: Bool Wahr, wenn die Updateschleife läuft
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet die Updateschleife wenn sie noch nicht läuft."""
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'Logger')
            self.__thread.start()

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False

    def __update(self):
        if self.__last_update_time.day != dt.now().day:
            self.cleanup()
        self.__last_update_time = dt.now()

    def __loop(self):
        self.cleanup()
        while self.__run:
            self.__update()
            time.sleep(Global.CONST.LOG.TIME_INCREMENT)
