#!/usr/bin/env python
# coding=utf-8
"""Dieses Modul stellt einige Widgets für PySide zur Verfügung.
"""

#############################################################################
#
# Copyright (C) 2004-2005 Trolltech AS. All rights reserved.
#
# This file is part of the example classes of the Qt Toolkit.
#
# This file may be used under the terms of the GNU General Public
# License version 2.0 as published by the Free Software Foundation
# and appearing in the file LICENSE.GPL included in the packaging of
# this file.  Please review the following information to ensure GNU
# General Public Licensing requirements will be met:
# http://www.trolltech.com/products/qt/opensource.html
#
# If you are unsure which license is appropriate for your use, please
# review the following information:
# http://www.trolltech.com/products/qt/licensing.html or contact the
# sales department at sales@trolltech.com.
#
# This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
# WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
#
#############################################################################

from PySide import QtCore, QtGui
from datetime import datetime as dt
import re

import Global

TAG_RE = re.compile(r'<[^>]+>')


def remove_tags(text):
    """Entfernt alle Tags aus einem String.
    
    :param text: String
    :return: String ohne tags
    """
    return TAG_RE.sub('', text)


class AnalogClock(QtGui.QWidget):
    """ AnalogClock ist ein Widget mit einer analogen Uhr. Sie besitzt einen Stunden- und einen Minutenzeiger.
     Die Uhrzeit aktualisiert sich automatisch.   
    """
    hourHand = QtGui.QPolygon([
        QtCore.QPoint(7, 0),
        QtCore.QPoint(0, 7),
        QtCore.QPoint(-7, 0),
        QtCore.QPoint(0, -50)
    ])

    minuteHand = QtGui.QPolygon([
        QtCore.QPoint(5, 0),
        QtCore.QPoint(0, 5),
        QtCore.QPoint(-5, 0),
        QtCore.QPoint(0, -80)
    ])

    hourColor = QtGui.QColor(Global.CONST.SCH.COLOR.CLOCK_HOUR)
    minuteColor = QtGui.QColor(Global.CONST.SCH.COLOR.CLOCK_MINUTE)
    blackColor = QtGui.QColor(Global.CONST.SCH.COLOR.BG)

    def __init__(self, parent=None):
        super(AnalogClock, self).__init__(parent)

        timer = QtCore.QTimer(self)
        # noinspection PyUnresolvedReferences
        timer.timeout.connect(self.update)
        timer.start(1000)

    def paintEvent(self, event):
        side = min(self.width(), self.height())
        # noinspection PyArgumentList
        time = QtCore.QTime.currentTime()

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)

        painter.setPen(QtCore.Qt.NoPen)

        background_rectangle = QtGui.QPolygon([
            QtCore.QPoint(0, 0),
            QtCore.QPoint(self.width(), 0),
            QtCore.QPoint(self.width(), self.height()),
            QtCore.QPoint(0, self.height())
        ])

        painter.setBrush(AnalogClock.blackColor)
        painter.drawConvexPolygon(background_rectangle)

        painter.translate(self.width() / 2, self.height() / 2)
        painter.scale(side / 200.0, side / 200.0)

        painter.setBrush(AnalogClock.hourColor)

        painter.save()
        painter.rotate(30.0 * (time.hour() + time.minute() / 60.0))
        painter.drawConvexPolygon(AnalogClock.hourHand)
        painter.restore()

        painter.setPen(QtGui.QPen(AnalogClock.hourColor, 2))

        for i in range(12):
            painter.drawLine(88, 0, 96, 0)
            painter.rotate(30.0)

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(AnalogClock.minuteColor)

        painter.save()
        painter.rotate(6.0 * (time.minute() + time.second() / 60.0))
        painter.drawConvexPolygon(AnalogClock.minuteHand)
        painter.restore()

        painter.setPen(QtGui.QPen(AnalogClock.minuteColor, 1))

        for j in range(60):
            if (j % 5) != 0:
                painter.drawLine(92, 0, 96, 0)
            painter.rotate(6.0)


class StretchedLabel(QtGui.QLabel):
    """StretchedLabel ist ein Label dessen Schrift an die Größe des Labels anpasst.
    
    :param args: Wird an den Superkonstruktor weitergegeben.
    :param kwargs: Wird and den Superkonstruktor weitergegeben.
    """

    # Mit diesem Signal kann der Text des Labels gesetzt werden. Aufruf über StrectchedLabel.set_text.emit(unicode text)
    set_text = QtCore.Signal(unicode)

    # Mit diesem Signal wird die Schriftgröße an den zur Verfügung stehenden Platz angepasst. Aufruf über
    # StrectchedLabel.fit_font_size.emit()
    fit_font_size = QtCore.Signal()

    # Mit diesem Signal wird das StyleSheet des Labels gesetzt. Aufruf über
    # StrectchedLabel.set_style_sheet.emit(unicode text)
    set_style_sheet = QtCore.Signal(unicode)

    def __init__(self, *args, **kwargs):
        QtGui.QLabel.__init__(self, *args, **kwargs)
        self.setSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Ignored)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.set_text.connect(self.__set_text)
        self.fit_font_size.connect(self.__fit_font_size)
        self.set_style_sheet.connect(self.__set_style_sheet)

    @QtCore.Slot(unicode)
    def __set_style_sheet(self, style):
        self.setStyleSheet(style)

    @QtCore.Slot(unicode)
    def __set_text(self, text):
        self.setText(text)

    def resizeEvent(self, event):
        self.__fit_font_size()

    @QtCore.Slot()
    def __fit_font_size(self):

        text = remove_tags(self.text())

        if text == '':
            return

        font = self.font()
        text_width = self.fontMetrics().boundingRect(text).width()
        margins = self.getContentsMargins()
        width = self.width() - margins[0] - margins[2]
        text_height = self.fontMetrics().boundingRect(text).height()
        height = self.height() - margins[1] - margins[3]

        x_ratio = float(width) / float(text_width)
        y_ratio = float(height) / float(text_height)
        ratio = min(x_ratio, y_ratio) * 0.95
        old_font_size = float(font.pointSize())
        new_font_size = max(1.0, font.pointSize() * ratio)
        if new_font_size - old_font_size > old_font_size * 0.01 or new_font_size < old_font_size:
            font.setPointSize(new_font_size)
            self.setFont(font)


class DigitalClock(StretchedLabel):
    """DigitalClock ist ein Widget, das die Uhrzeit/Datum im Digitalformat anzeigt. Das Anzeigeformat muss beim 
    initialisieren angegeben werden
    
    :param time_format: Bestimmt das Anzeigeformat. Dokumentation unter https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
    :param args: Wird an den Superkonstruktor weitergegeben.
    :param kwargs: Wird and den Superkonstruktor weitergegeben.
    """
    def __init__(self, time_format, *args, **kwargs):
        StretchedLabel.__init__(self, *args, **kwargs)
        self.time_format = time_format
        self.setText(dt.now().strftime(self.time_format))

        timer = QtCore.QTimer(self)
        # noinspection PyUnresolvedReferences
        timer.timeout.connect(self.__update_time)
        timer.start(10)

    def __update_time(self):
        self.setText(dt.now().strftime(self.time_format))


class StretchedImage(QtGui.QLabel):
    """StretchedImage ist ein Widget, ein Bild anzeigt und dieses auf die Fläche des ganzen Bereiches ausdehnt.

    :param args: Wird an den Superkonstruktor weitergegeben.
    :param kwargs: Wird and den Superkonstruktor weitergegeben.
    """

    # Mit diesem Signal kann das Bild gesetzt werden. Aufruf über StretchedImage.set_image.emit(unicode text)
    set_image = QtCore.Signal(unicode)

    def __init__(self, *args, **kwargs):
        QtGui.QLabel.__init__(self, *args, **kwargs)
        self.setMinimumSize(1, 1)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.set_image.connect(self.__set_image)

    @QtCore.Slot(unicode)
    def __set_image(self, image_path):
        image = QtGui.QPixmap(image_path)
        min_size = min(self.height(), self.width())
        pic_size = QtCore.QSize(min_size, min_size)
        self.setPixmap(image.scaled(pic_size, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation))
