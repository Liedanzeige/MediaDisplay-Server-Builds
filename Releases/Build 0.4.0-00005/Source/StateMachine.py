# -*- coding: utf-8 -*-
"""Stellt eine Klasse "StateMachine" zur Verfügung.
"""

import time
import threading
from datetime import datetime as dt

import Global


class StateMachine(object):
    """Implementiert eine StateMachine zur Steuerung der Applikation."""
    def __init__(self):
        self.__run = False
        self.__commands = list()
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None
        self.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN

    @property
    def state(self):
        """Der aktuelle Zustand."""
        return self.__state

    def init_values(self):
        """Initialisiert alle Variablen der StateMachine."""
        self.__state = Global.CONST.SM.STATE_START
        self.__state_changed = False
        self.last_transition_time = dt.now()
        self.last_command_time = dt.now()
        self.__increments = 0
        self.__thread = None

    def __fetch_ir_commands(self):
        """Holt IR-Kommandos aus dem OsInterface."""
        commands = Global.Instances.os_interface.get_ir_commands()
        for command in commands:
            self.__commands.append({'type': Global.CONST.SM.CMD_IR, 'data': command})

    def get_total_seconds_since_last_trans(self):
        """Gibt die Zeit seit dem letzten Übergang in Sekunden zurück. Die Zeit orientiert sich dabei an der Systemzeit 
        und ist daher nicht geeignet wenn die Systemzeit verstellt wird.
        
        :return: Int Zeit seit dem letzten Übergang in Sekunden.
        """
        delta = dt.now() - self.last_transition_time
        return delta.total_seconds()

    def get_total_seconds_since_last_command(self):
        """Gibt die Zeit seit dem letzten akzeptierten Kommando in Sekunden zurück. Die Zeit orientiert sich dabei an 
        der Systemzeit und ist daher nicht geeignet wenn die Systemzeit verstellt wird.

        :return: Int Zeit seit dem letzten akzeptierten Kommando in Sekunden.
        """
        delta = dt.now() - self.last_command_time
        return delta.total_seconds()

    def get_total_seconds_increment(self):
        """Gibt die Zeit seit dem letzten akzeptierten Kommando in Sekunden zurück. Die Zeit orientiert sich dabei an 
        der Anzahl Updateschleifen und kann daher von der echten Zeit abweichen. Da sie aber unabhängig von der 
        Systemzeit ist kann sie verwendet werden, wenn die Systemzeit verstellt wird.

        :return: Int Zeit seit dem letzten akzeptierten Kommando in Sekunden.
        """
        return self.__increments * Global.CONST.SM.TIME_INCREMENT

    def __set_command_time(self):
        """Muss bei jedem akzeptieren Kommando aufgerufen werden, damit die Zeit seit dem letzten Kommando 
        zurückgesetzt wird."""
        self.__increments = 0
        self.last_command_time = dt.now()

    def pass_command(self, command):
        """Nimmt ein neues Kommando entgegen.
        
        :param command: Dict mit Key "type", das Kommando
        """
        if isinstance(command, dict) and command.get('type', '') in (Global.CONST.SM.CMD_SYS,
                                                                     Global.CONST.SM.CMD_IR,
                                                                     Global.CONST.SM.CMD_NET):
            self.__commands.append(command)
        else:
            raise Exception(u'Command has to be an dict with key "type"')

    def __change_state(self, target_state, gui_params):
        """Setzt den Zustand der StateMachine.
        
        :param target_state: Int Zustand der eingenommen werden soll.
        :param gui_params: Parameter für die GUI, werden dem ScreenHandler übergeben.
        """
        self.__state = target_state
        self.last_transition_time = dt.now()
        self.__set_command_time()
        Global.Instances.screen_handler.set_page(target_state, gui_params)
        self.__state_changed = True
        if target_state == Global.CONST.SM.STATE_STANDBY:
            Global.Instances.os_interface.switch_hdmi_off()
        Global.Instances.logger.log(u'State changed: %s' % Global.CONST.SM.STATE_DICT.get(target_state, u'Unbekannt'),
                                    __name__, Global.CONST.LOG.SEV.INFO)

    def is_running(self):
        """Gibt zurück, ob die Updateschleife läuft.

        :return: Bool Wahr, wenn die Updateschleife läuft
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet die Updateschleife wenn sie noch nicht läuft."""
        if not self.is_running():
            self.__run = True
            self.__thread = threading.Thread(target=self.__loop, args=(), name=u'StateMachine')
            self.__thread.start()

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False

    def __update(self):
        # get command
        self.__fetch_ir_commands()
        if self.__commands:
            command = self.__commands.pop(0)
            command_type = command['type']
            Global.Instances.logger.log('IR command "%s" received' % command['data'],
                                        __name__, Global.CONST.LOG.SEV.DEBUG)
        else:
            command = None
            command_type = None

        self.__increments += 1

        # blink led if ir command
        if command_type == Global.CONST.SM.CMD_IR and (not self.state == Global.CONST.SM.STATE_STANDBY or
                                                       command['data'] == Global.CONST.SM.IRCMD.POWER):
            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.PURPLE, 0.2)

        # state machine
        if self.state == Global.CONST.SM.STATE_START:
            Global.Instances.logger.log('Start init hardware...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.os_interface.init_hardware()
            Global.Instances.logger.log('Hardware inited', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.logger.log('Start init LED...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.os_interface.init_led()
            Global.Instances.logger.log('LED inited', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.logger.log('App start instances...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.app.start_instances()
            Global.Instances.logger.log('App instances start finished', __name__, Global.CONST.LOG.SEV.DEBUG)

            Global.Instances.os_interface.set_led(None)

            self.__change_state(Global.CONST.SM.STATE_IDLE, {})
        elif self.state == Global.CONST.SM.STATE_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    self.__change_state(Global.CONST.SM.STATE_CONFIRM_QUIT, command['data'])
                elif command['data'] in Global.CONST.SM.IRCMD.N1_9_LIST:
                    self.__change_state(Global.CONST.SM.STATE_NUMBER, command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SONG, 'show_again')
                elif command['data'] == Global.CONST.SM.IRCMD.MENU:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME, None)
                elif command['data'] == Global.CONST.SM.IRCMD.GUIDE:
                    self.__change_state(Global.CONST.SM.STATE_IP, None)
            elif self.get_total_seconds_since_last_command() >= Global.Instances.general_settings.time_1_5 * 60:
                self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
        elif self.state == Global.CONST.SM.STATE_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N4_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.toggle_solo(command['data'])
            elif self.get_total_seconds_since_last_command() >= Global.Instances.general_settings.time_2_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_NUMBER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] in (Global.CONST.SM.IRCMD.EXIT, Global.CONST.SM.IRCMD.LEFT):
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST + [Global.CONST.SM.IRCMD.DELIMITER]:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_number(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.delete_number()
                elif command['data'] in (Global.CONST.SM.IRCMD.RIGHT, Global.CONST.SM.IRCMD.OK):
                    self.__change_state(Global.CONST.SM.STATE_SONG, None)
            elif self.get_total_seconds_since_last_command() >= Global.Instances.general_settings.time_3_2:
                self.__change_state(Global.CONST.SM.STATE_SONG, None)
        elif self.state == Global.CONST.SM.STATE_BLUESCREEN:
            pass
        elif self.state == Global.CONST.SM.STATE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.POWER:
                    Global.Instances.os_interface.switch_hdmi_on()
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_QUIT:
            Global.Instances.os_interface.set_led_time(Global.CONST.OSI.COLORS.WHITE, 2)
            time.sleep(2)
            Global.Instances.app.quit()
        elif self.state == Global.CONST.SM.STATE_CONFIRM_QUIT:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_STANDBY:
                    self.__change_state(Global.CONST.SM.STATE_STANDBY, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.CONFIRM_SHUTDOWN, Global.CONST.SM.IRCMD.OK):
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                    self.shutdown_mode = Global.CONST.SM.SHUTDOWN
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_REBOOT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
                    self.shutdown_mode = Global.CONST.SM.REBOOT
                elif command['data'] == Global.CONST.SM.IRCMD.CONFIRM_QUIT:
                    self.__change_state(Global.CONST.SM.STATE_QUIT, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_7_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DATE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.PLUS:
                    self.__set_command_time()
                    Global.Instances.os_interface.hour_plus()
                elif command['data'] == Global.CONST.SM.IRCMD.MINUS:
                    self.__set_command_time()
                    Global.Instances.os_interface.hour_minus()
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_time_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_time_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_time()
            elif self.get_total_seconds_increment() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_DATE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_date_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_date_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_date()
            elif self.get_total_seconds_increment() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_CLOCK_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DATE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_CLOCK_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_CLOCK_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_SONG_IDLE:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DISPLAY_SERVER, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in Global.CONST.SM.IRCMD.N_LIST:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.BACK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.add_setting_char(None)
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SYSTEM_INFORMATION:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_DISPLAY_SERVER, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_IP:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_17_1:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        elif self.state == Global.CONST.SM.STATE_SET_DISPLAY_SERVER:
            if command_type == Global.CONST.SM.CMD_IR:
                if command['data'] == Global.CONST.SM.IRCMD.LEFT:
                    self.__change_state(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY, None)
                elif command['data'] == Global.CONST.SM.IRCMD.RIGHT:
                    self.__change_state(Global.CONST.SM.STATE_SYSTEM_INFORMATION, None)
                elif command['data'] == Global.CONST.SM.IRCMD.EXIT:
                    self.__change_state(Global.CONST.SM.STATE_IDLE, None)
                elif command['data'] in (Global.CONST.SM.IRCMD.N1, Global.CONST.SM.IRCMD.N2):
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting_char(command['data'])
                elif command['data'] == Global.CONST.SM.IRCMD.OK:
                    self.__set_command_time()
                    Global.Instances.screen_handler.set_setting()
            elif self.get_total_seconds_since_last_command() >= Global.CONST.SM.TIME.CHANGE_SETTINGS:
                self.__change_state(Global.CONST.SM.STATE_IDLE, None)
        else:
            Global.Instances.logger.log(u'Unknown state, restart with WatchDog', __name__, Global.CONST.LOG.SEV.ERROR)
            Global.Instances.watchdog.restart = True

    def __loop(self):
        while self.__run:
            self.__state_changed = True
            while self.__state_changed or self.__commands:
                self.__state_changed = False
                self.__update()
            time.sleep(Global.CONST.SM.TIME_INCREMENT)
