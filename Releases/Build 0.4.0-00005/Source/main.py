# -*- coding: utf-8 -*-

import os
import sys
from PySide import QtGui
from PySide import QtCore

import Global
import CommandServer
import DisplayServer
import WebServer
import ScreenHandler
import SessionHandler
import AjaxInterface
import StateMachine
import OsInterface
import TestRunner
import Logger
import Watchdog
import MDSWidgets
import SettingsHandler


class MainWindow(QtGui.QWidget):
    """GUI der Anwendung."""

    # Mit diesem Signal wird die Seite der GUI gesetzt. Aufruf über
    # MainWindow.set_stack_index.emit(unicode text)
    set_stack_index = QtCore.Signal(int)

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ids = dict()
        self.__current = 0
        self.stack = QtGui.QStackedLayout()
        self.set_stack_index.connect(self.__set_stack_index)
        self.init_gui()

    @property
    def current(self):
        """Der Index der aktuellen Seite. Typ int."""
        return self.__current

    @current.setter
    def current(self, value):
        self.set_stack_index.emit(value)

    @QtCore.Slot(int)
    def __set_stack_index(self, index):
        self.stack.setCurrentIndex(index)
        self.__current = index

    def init_gui(self):
        """Initialisiert die Widgets der GUI."""

        # Start / 0
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        label2 = MDSWidgets.StretchedLabel('Liedanzeige')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartLiedanzeige'] = label2
        label3 = MDSWidgets.StretchedLabel(u'   MediaDisplay-Server 0.4.0-00005   ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartVersion'] = label3
        label4 = MDSWidgets.StretchedLabel(u'   \u00A9 2017 Liedanzeige Entwicklungsteam   ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelStartCopyright'] = label4
        label5 = MDSWidgets.StretchedLabel('')
        label5.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 1, 0, 1, 1)
        grid.addWidget(label2, 2, 0, 2, 1)
        grid.addWidget(label3, 4, 0, 1, 1)
        grid.addWidget(label4, 5, 0, 1, 1)
        grid.addWidget(label5, 6, 0, 1, 1)
        widget.setLayout(grid)

        self.stack.addWidget(widget)

        # Zeige Uhr / 1
        if Global.Instances.general_settings.clock_idle == Global.CONST.STH.GENERAL.CLOCK_IDLE_ANALOG:
            clock = MDSWidgets.AnalogClock()
        else:
            clock = MDSWidgets.DigitalClock('%H:%M')
            clock.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                Global.CONST.SCH.COLOR.FG + u"; }")

        self.stack.addWidget(clock)

        # Zeige Lied / 2
        if Global.Instances.general_settings.clock_song == Global.CONST.STH.GENERAL.CLOCK_SONG_NO:
            label1_1 = MDSWidgets.StretchedLabel('--')
            label1_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
            self.ids['BookTitle'] = label1_1
            label1_2 = MDSWidgets.StretchedLabel('---')
            label1_2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
            self.ids['SongNumber'] = label1_2
            grid1_1 = QtGui.QGridLayout()
            grid1_1.setSpacing(0)
            grid1_1.setContentsMargins(0, 0, 0, 0)
            # grid1_1.addWidget(label1_1, 0, 0, 1, 1)
            grid1_1.addWidget(label1_2, 0, 0, 1, 4)
            widget1_1 = QtGui.QWidget()
            widget1_1.setLayout(grid1_1)

            label2_1 = MDSWidgets.StretchedLabel('Sopran')
            label2_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_FG + u"; }")
            self.ids['Sopran'] = label2_1
            label2_2 = MDSWidgets.StretchedLabel('Alt')
            label2_2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_ALT_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_ALT_FG + u"; }")
            self.ids['Alt'] = label2_2
            label2_3 = MDSWidgets.StretchedLabel('Tenor')
            label2_3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_FG + u"; }")
            self.ids['Tenor'] = label2_3
            label2_4 = MDSWidgets.StretchedLabel('Bass')
            label2_4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_BASS_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_BASS_FG + u"; }")
            self.ids['Bass'] = label2_4
            grid2_1 = QtGui.QGridLayout()
            grid2_1.setSpacing(0)
            grid2_1.setContentsMargins(0, 0, 0, 0)
            grid2_1.addWidget(label2_1, 0, 3, 1, 1)
            grid2_1.addWidget(label2_2, 0, 0, 1, 1)
            grid2_1.addWidget(label2_3, 0, 2, 1, 1)
            grid2_1.addWidget(label2_4, 0, 1, 1, 1)
            widget2_1 = QtGui.QWidget()
            widget2_1.setLayout(grid2_1)

            grid = QtGui.QGridLayout()
            grid.setSpacing(0)
            grid.setContentsMargins(0, 0, 0, 0)
            grid.addWidget(widget1_1, 0, 0, 4, 1)
            grid.addWidget(widget2_1, 4, 0, 1, 1)
            widget = QtGui.QWidget()
            widget.setLayout(grid)

        else:
            label1_1 = MDSWidgets.StretchedLabel('--')
            label1_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.BOOK + u"; }")
            self.ids['BookTitle'] = label1_1
            label1_2 = MDSWidgets.StretchedLabel('---')
            label1_2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
            self.ids['SongNumber'] = label1_2
            grid1_1 = QtGui.QGridLayout()
            grid1_1.setSpacing(0)
            grid1_1.setContentsMargins(0, 0, 0, 0)
            # grid1_1.addWidget(label1_1, 0, 0, 1, 1)
            grid1_1.addWidget(label1_2, 0, 0, 1, 4)
            widget1_1 = QtGui.QWidget()
            widget1_1.setLayout(grid1_1)

            label2_1 = MDSWidgets.StretchedLabel('Sopran')
            label2_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_FG + u"; }")
            self.ids['Sopran'] = label2_1
            label2_2 = MDSWidgets.StretchedLabel('Alt')
            label2_2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_ALT_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_ALT_FG + u"; }")
            self.ids['Alt'] = label2_2
            label2_3 = MDSWidgets.StretchedLabel('Tenor')
            label2_3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_FG + u"; }")
            self.ids['Tenor'] = label2_3
            label2_4 = MDSWidgets.StretchedLabel('Bass')
            label2_4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_BASS_BG +
                                   u"; color : " + Global.CONST.SCH.COLOR.SOLO_BASS_FG + u"; }")
            self.ids['Bass'] = label2_4
            grid2_1 = QtGui.QGridLayout()
            grid2_1.setSpacing(0)
            grid2_1.setContentsMargins(0, 0, 0, 0)
            grid2_1.addWidget(label2_1, 1, 1, 1, 1)
            grid2_1.addWidget(label2_2, 1, 0, 1, 1)
            grid2_1.addWidget(label2_3, 0, 1, 1, 1)
            grid2_1.addWidget(label2_4, 0, 0, 1, 1)
            widget2_1 = QtGui.QWidget()
            widget2_1.setLayout(grid2_1)

            label3_1 = MDSWidgets.DigitalClock('%H:%M')
            label3_1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                   Global.CONST.SCH.COLOR.FG + u"; }")
            self.ids['SongScreenClock'] = label3_1
            grid3_1 = QtGui.QGridLayout()
            grid3_1.setSpacing(0)
            grid3_1.setContentsMargins(0, 0, 0, 0)
            grid3_1.addWidget(label3_1, 0, 0, 0, 0)
            widget3_1 = QtGui.QWidget()
            widget3_1.setLayout(grid3_1)

            grid = QtGui.QGridLayout()
            grid.setSpacing(0)
            grid.setContentsMargins(0, 0, 0, 0)
            grid.addWidget(widget1_1, 0, 0, 2, 2)
            grid.addWidget(widget2_1, 2, 0, 1, 1)
            grid.addWidget(widget3_1, 2, 1, 1, 1)
            widget = QtGui.QWidget()
            widget.setLayout(grid)

        self.stack.addWidget(widget)

        # Zeige Nummer / 3
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('---')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['Number'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Bluescreen / 4
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel(u' Oops... ')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BLUESCREEN + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['Oops'] = label1
        label2 = MDSWidgets.StretchedLabel(u' Das h\u00e4tte nicht passieren d\u00fcrfen ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BLUESCREEN + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label2, 0, 2, 1, 1)
        self.ids['BluescreenLabel'] = label2
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Standby / 5
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('Standby')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['StandbyLabel'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Beenden / 6
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel('Beenden...')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelQuit'] = label1
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Bestätige Beenden / 7
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsConfirmQuitList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Beenden?')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label1, 0, 0, 2, 1)
        self.ids['LabelsConfirmQuitList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'                  1 - Standby                  ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label2, 2, 0, 1, 1)
        self.ids['LabelsConfirmQuitList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'          2 oder OK - Herunterfahren           ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label3, 3, 0, 1, 1)
        self.ids['LabelsConfirmQuitList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'                  3 - Neustart                 ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        grid.addWidget(label4, 4, 0, 1, 1)
        self.ids['LabelsConfirmQuitList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Uhr / 8
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTimeList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Stelle Uhr')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsSetTimeList'].append(label1)
        label3 = MDSWidgets.DigitalClock('%H:%M:%S')
        label3.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
        grid.addWidget(label3, 2, 0, 1, 1)
        self.ids['LabelsSetTimeList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'--:--:--')
        label4.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        self.ids['LabelSetTime'] = label4
        grid.addWidget(label4, 3, 0, 1, 1)
        self.ids['LabelsSetTimeList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Datum / 9
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetDateList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Stelle Datum')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        self.ids['LabelsSetDateList'].append(label1)
        label3 = MDSWidgets.DigitalClock('%d.%m.%Y')
        label3.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
        grid.addWidget(label3, 2, 0, 1, 1)
        self.ids['LabelsSetDateList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'--.--.----')
        label4.setFont(QtGui.QFont("DejaVu Sans Mono", 20))
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        self.ids['LabelSetDate'] = label4
        grid.addWidget(label4, 3, 0, 1, 1)
        self.ids['LabelsSetDateList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Uhranzeige Idle / 10
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetClockIdleList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Uhranzeige')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 3)
        self.ids['LabelsSetClockIdleList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' 1 - analog ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 1, 1, 2)
        self.ids['LabelsSetClockIdleList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u' 2 - digital ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 1, 2)
        self.ids['LabelsSetClockIdleList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetClockIdle'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetClockIdleList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Uhranzeige Lied / 11
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetClockSongList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Liedanzeige')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 3)
        self.ids['LabelsSetClockSongList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' 1 - ohne Uhr ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 1, 1, 2)
        self.ids['LabelsSetClockSongList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u' 2 - mit Uhr  ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 1, 2)
        self.ids['LabelsSetClockSongList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetClockSong'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetClockSongList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Übergangszeit Nummer -> Lied / 12
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTime_3_2_List'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Übergangszeit')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['LabelsSetTime_3_2_List'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'Nummer -> Lied')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 0, 1, 2)
        self.ids['LabelsSetTime_3_2_List'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'Sekunden')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 2, 1)
        self.ids['LabelsSetTime_3_2_List'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetTime_3_2'] = label4
        grid.addWidget(label4, 2, 0, 2, 1)
        self.ids['LabelsSetTime_3_2_List'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Übergangszeit Lied -> Uhr / 13
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTime_2_1_List'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Übergangszeit')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['LabelsSetTime_2_1_List'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' Lied -> Uhr ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 0, 1, 2)
        self.ids['LabelsSetTime_2_1_List'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'Sekunden')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 2, 1)
        self.ids['LabelsSetTime_2_1_List'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetTime_2_1'] = label4
        grid.addWidget(label4, 2, 0, 2, 1)
        self.ids['LabelsSetTime_2_1_List'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle Übergangszeit Uhr -> Standby / 14
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetTime_1_5_List'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Übergangszeit')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 2)
        self.ids['LabelsSetTime_1_5_List'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' Uhr -> Standby ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 0, 1, 2)
        self.ids['LabelsSetTime_1_5_List'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'Minuten')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 2, 1)
        self.ids['LabelsSetTime_1_5_List'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetTime_1_5'] = label4
        grid.addWidget(label4, 2, 0, 2, 1)
        self.ids['LabelsSetTime_1_5_List'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle LogLevel / 15
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedLabel(u'Stelle LogLevel')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 1)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Seite Systeminformationen / 16
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSysteminformation'] = list()
        label1 = MDSWidgets.StretchedLabel(u'Systeminformationen')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 2, 1)
        self.ids['LabelsSysteminformation'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u'Version: ' + Global.CONST.VERSION)
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 2, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u'Build: ' + Global.CONST.BUILD)
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 3, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'Commit: ' + Global.CONST.COMMIT)
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label4, 4, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label4)
        label5 = MDSWidgets.StretchedLabel(u'Seriennummer des Prozessors: ' +
                                           Global.Instances.os_interface.serial_number)
        label5.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label5, 5, 0, 1, 1)
        self.ids['LabelsSysteminformation'].append(label5)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Zeige IP / 17
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = MDSWidgets.StretchedImage()
        label1.set_image.emit(Global.CONST.SCH.ERROR_QR_CODE_PATH)
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.BG + u"; }")
        self.ids['LabelNetworkQrCode'] = label1
        grid.addWidget(label1, 0, 0, 8, 1)
        label2 = MDSWidgets.StretchedLabel(u'Netzwerkadresse')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                             Global.CONST.SCH.COLOR.FG + u"; }")
        self.ids['LabelNetworkAddress'] = label2
        grid.addWidget(label2, 8, 0, 1, 1)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        # Stelle DisplayServer
        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        self.ids['LabelsSetDisplayServerList'] = list()
        label1 = MDSWidgets.StretchedLabel(u'DisplayServer')
        label1.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label1, 0, 0, 1, 3)
        self.ids['LabelsSetDisplayServerList'].append(label1)
        label2 = MDSWidgets.StretchedLabel(u' 1 - Ein ')
        label2.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label2, 1, 1, 1, 2)
        self.ids['LabelsSetDisplayServerList'].append(label2)
        label3 = MDSWidgets.StretchedLabel(u' 2 - Aus ')
        label3.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        grid.addWidget(label3, 2, 1, 1, 2)
        self.ids['LabelsSetDisplayServerList'].append(label3)
        label4 = MDSWidgets.StretchedLabel(u'  ---  ')
        label4.setStyleSheet(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                             u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG + u"; }")
        self.ids['LabelSetDisplayServer'] = label4
        grid.addWidget(label4, 1, 0, 2, 1)
        self.ids['LabelsSetDisplayServerList'].append(label4)
        widget.setLayout(grid)
        self.stack.addWidget(widget)

        self.current = Global.CONST.SM.STATE_START

        self.setLayout(self.stack)

        self.stack.setCurrentIndex(0)
        # self.setGeometry(300, 300, 700, 500)
        self.setWindowTitle(u'Liedanzeige')

        self.showFullScreen()


class MediaDisplayServerApp(QtGui.QApplication):
    """MediaDisplay-Server Anwendung."""

    def __init__(self, *args, **kwargs):
        super(MediaDisplayServerApp, self).__init__(*args, **kwargs)
        Global.Instances.app = self
        Global.Instances.logger = Logger.Logger()
        Global.Instances.general_settings = SettingsHandler.GeneralSettingsHandler()
        self.font_database = QtGui.QFontDatabase()
        self.load_fonts()
        self.init_instances()
        Global.Instances.main_window = MainWindow()
        # noinspection PyUnresolvedReferences
        self.aboutToQuit.connect(self.on_stop)

    def load_fonts(self):
        """Lädt die Schriftarten für die GUI."""
        for dirname, dirs, files in os.walk(Global.CONST.FONTS_DIR):
            for file in files:
                if file.lower().endswith('.ttf'):
                    font_path = os.path.join(dirname, file)
                    self.font_database.addApplicationFont(font_path)

    @staticmethod
    def on_stop():
        """Stoppt alle Instanzen."""
        Global.Instances.logger.log('Stop instances...', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Disable watchdog ...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.watchdog.allow_stop = True
        Global.Instances.logger.log('Watchdog disabled ...', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop StateMachine...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.state_machine.stop()
        Global.Instances.logger.log('StateMachine stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop DisplayServer...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.display_server.stop()
        Global.Instances.logger.log('DisplayServer stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop WebServer...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.web_server.stop()
        Global.Instances.logger.log('WebServer stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop CommandServer...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.command_server.stop()
        Global.Instances.logger.log('CommandServer stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop OsInterface...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.os_interface.stop()
        Global.Instances.logger.log('OsInterface stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        try:
            Global.Instances.logger.log('Stop TestRunner...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.test_runner.stop()
        except AttributeError:
            pass
        finally:
            Global.Instances.logger.log('TestRunner stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop Logger...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.logger.stop()
        Global.Instances.logger.log('Logger stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Stop Watchdog...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.watchdog.stop()
        Global.Instances.logger.log('Watchdog stopped', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('All instances stopped', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.logger.log('Exit Program', __name__, Global.CONST.LOG.SEV.DEBUG)

    def init_instances(self):
        """Initialisiert alle Instanzen."""
        self.init_dirs()
        Global.Instances.logger.log('MediaDisplay-Server %s %s' % (Global.CONST.VERSION, Global.CONST.BUILD),
                                    __name__, Global.CONST.LOG.SEV.INFO)
        Global.Instances.logger.log('Start init instances...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.watchdog = Watchdog.Watchdog()
        Global.Instances.os_interface = OsInterface.OsInterface()
        Global.Instances.screen_handler = ScreenHandler.ScreenHandler()
        Global.Instances.session_handler = SessionHandler.SessionHandler()
        Global.Instances.state_machine = StateMachine.StateMachine()
        Global.Instances.ajax_interface = AjaxInterface.AjaxInterface()
        Global.Instances.command_server = CommandServer.Server()
        Global.Instances.display_server = DisplayServer.DisplayServer(("", Global.CONST.DS.PORT),
                                                                      DisplayServer.DisplayRequestHandler)
        Global.Instances.web_server = WebServer.Server()
        if Global.CONST.TEST:
            Global.Instances.test_runner = TestRunner.TestRunner()

        Global.Instances.logger.start()
        Global.Instances.state_machine.start()
        Global.Instances.logger.log('Instances inited', __name__, Global.CONST.LOG.SEV.DEBUG)

    @staticmethod
    def start_instances():
        """Startet alle Instanzen."""
        Global.Instances.logger.log('Start DisplayServer...', __name__, Global.CONST.LOG.SEV.DEBUG)
        if Global.Instances.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_ON:
            Global.Instances.display_server.start()
        Global.Instances.logger.log('DisplayServer started', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Start CommandServer...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.command_server.start()
        Global.Instances.logger.log('CommandServer started', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Start WebServer...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.web_server.start()
        Global.Instances.logger.log('WebServer started', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.logger.log('Start OsInterface...', __name__, Global.CONST.LOG.SEV.DEBUG)
        Global.Instances.os_interface.start()
        Global.Instances.logger.log('OsInterface started', __name__, Global.CONST.LOG.SEV.DEBUG)

        Global.Instances.watchdog.is_started = True

        if Global.CONST.TEST:
            Global.Instances.logger.log('Start TestRunner...', __name__, Global.CONST.LOG.SEV.DEBUG)
            Global.Instances.test_runner.start()
            Global.Instances.logger.log('TestRunner started', __name__, Global.CONST.LOG.SEV.DEBUG)

    @staticmethod
    def init_dirs():
        """Erstellt notwendige Ordner."""
        if not os.path.exists(Global.CONST.USER_DATA_DIR):
            os.makedirs(Global.CONST.USER_DATA_DIR)
        if not os.path.exists(Global.CONST.STH.DIR):
            os.makedirs(Global.CONST.STH.DIR)
        if not os.path.exists(Global.CONST.LOG.DIR):
            os.makedirs(Global.CONST.LOG.DIR)

    def run(self):
        """Startet die Applikation.
        
        :return: Rückgabewert der Applikation.
        """
        self.setFont(QtGui.QFont("DejaVu Sans", 20))
        return self.exec_()


if __name__ == '__main__':
    App = MediaDisplayServerApp(sys.argv)
    exit_code = App.run()
    if Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN:
        Global.Instances.os_interface.shutdown()
    elif Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.REBOOT:
        Global.Instances.os_interface.reboot()
    sys.exit(exit_code)
