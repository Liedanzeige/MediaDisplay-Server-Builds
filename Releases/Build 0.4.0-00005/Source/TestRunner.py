# -*- coding: utf-8 -*-
"""Stellt Klassen zum Test der Applikation zur Verfügung-"""

import time
from datetime import datetime as dt
import codecs
import threading
import os
from random import randint
import socket
import urllib2

import Global


class TestRunner(object):
    """Dient zum Test der Applikation."""

    def __init__(self):
        self.thread = threading.Thread(target=self.run_tests, name="TestRunner")
        self.test_result = None

    def check_expected_gui_screen(self, expected_screen):
        """Prüft ob die GUI die erwartete Seite anzeigt und loggt das Ergebnis.
        
        :param expected_screen: Int, Index der erwarteten Seite.
        :return: 
        """
        screen = Global.Instances.main_window.current
        if screen == expected_screen:
            self.test_result.pass_('Screen "%s" is correct' % screen)
        else:
            self.test_result.fail('Screen should be "%s" but was "%s"' %
                                  (expected_screen, screen))

    def check_expected_label_text(self, label_id, expected_text):
        """Prüft ob ein Label den erwarteten Text enthält und loggt das Ergebnis.
        
        :param label_id: String, ID des zu prüfenden Labels.
        :param expected_text: String, der erwartete Text des Labels.
        """
        label = Global.Instances.main_window.ids[label_id]
        label_text = label.text()
        if label_text == expected_text:
            self.test_result.pass_(u'Labels "%s" text is correct' % label_id)
        else:
            self.test_result.fail(u'Labels "%s" text should be "%s" but was "%s"' %
                                  (label_id, expected_text, label_text))

    def check_url(self, url, expected_answer):
        """Prüft ob eine gegebene URL die erwartete Antwort gibt und loggt das Ergebnis.
        
        :param url: String, URL die abgefragt wird.
        :param expected_answer:  String, die erwartete Antwort.
        """
        request = urllib2.urlopen(url)
        answer = request.read()
        if answer == expected_answer:
            self.test_result.pass_(u'URL "%s" answer is correct' % url)
        else:
            self.test_result.fail(u'URL "%s" answer should be "%s" but was "%s"' %
                                  (url, expected_answer, answer))

    def assert_true(self, cond, message=u''):
        """Prüft ob die Bedingung wahr ist und loggt das Ergebnis.
        
        :param cond: Bedingung die geprüft wird. Muss nach Bool konvertierbar sein.
        :param message: Unioode, Meldung die geloggt wird.
        """
        if cond:
            self.test_result.pass_(u'Assert passed ' + message)
        else:
            self.test_result.fail(u'Assert failed ' + message)

    def send_ir_command(self, ir_command):
        """Simuliert ein IR-Kommando.
        
        :param ir_command: Sting, IR-Kommando das simuliert wird.
        """
        Global.Instances.os_interface.add_ir_command(ir_command)
        self.test_result.message(u'IR Command : %s' % ir_command)

    def run_tests(self):
        """Führt alle Tests aus der Datei "TestExecutionQueue.txt" aus."""
        self.test_result = TestResult()
        self.test_result.message(u'Commit: %s' % get_commit())
        self.test_result.message(u'Maybe not everything is committed and pushed')
        with open('TestExecutionQueue.txt', 'r') as test_queue_file:
            test_queue = test_queue_file.readlines()
        for test in test_queue:
            test_func_str = test.strip()
            if hasattr(self, test_func_str):
                test_func = getattr(self, test_func_str)
                try:
                    test_func()
                except Exception as e:
                    self.test_result.fail(u'Test Failed with message: %s' % unicode(e))
                    self.test_result.finish_test()
                    Global.Instances.app.quit()

    def restart_server_test(self):
        """Testet ob der CommandServer und der DisplayServer automatisch starten, nachdem sie abgestürtzt sind."""
        self.test_result.start_test('Restart Server')

        time.sleep(180)
        self.assert_true(Global.Instances.command_server.is_running(), u'Command Server is running')
        Global.Instances.command_server.stop()
        self.assert_true(not Global.Instances.command_server.is_running(), u'Command Server not is running')
        time.sleep(6)
        self.assert_true(Global.Instances.command_server.is_running(), u'Command Server is running')
        time.sleep(1)

        self.assert_true(Global.Instances.display_server.is_running(), u'Display Server is running')
        Global.Instances.display_server.stop()
        self.assert_true(not Global.Instances.display_server.is_running(), u'Display Server not is running')
        time.sleep(6)
        self.assert_true(Global.Instances.display_server.is_running(), u'Display Server is running')
        time.sleep(1)
        self.test_result.finish_test()

    def ir_large_input_test(self):
        """Testet das Verhalten wenn viele IR-Eingaben bei der Nummerneingabe kommen."""
        self.test_result.start_test('IR Large Input Test')

        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        # test song input
        song = ''
        book = ''
        third = ''
        song_list = [str(randint(0, 9)) for i in range(100)]
        book_list = [str(randint(0, 9)) for i in range(100)]
        third_list = [str(randint(0, 9)) for i in range(100)]

        for i in book_list:
            book += i
            self.send_ir_command(i)
            time.sleep(0.1)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in song_list:
            song += i
            self.send_ir_command(i)
            time.sleep(0.1)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in third_list:
            third += i
            self.send_ir_command(i)
            time.sleep(0.1)

        self.check_expected_label_text('Number', '<span style="color:#ffff00;">' + book + '</span> ' + song + ' ' +
                                       third)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '<span style="color:#ffff00;">X</span> ' + song)
        self.check_expected_label_text('BookTitle', '--')

        time.sleep(30)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def fsm_ir_commands_test(self):
        """Testet weite Teile der StateMachine."""
        # wait until the program has started
        self.test_result.start_test('FSM IR Commands Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # set settings for faster progress
        Global.Instances.general_settings.time_1_5 = 2
        Global.Instances.general_settings.time_2_1 = 15
        Global.Instances.general_settings.time_3_2 = 5

        # test song input
        self.send_ir_command('0')
        time.sleep(3)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)
        self.send_ir_command('4')
        time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(0.5)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;">4</span> 123')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '<span style="color:#ffff00;">4</span> 123')
        self.check_expected_label_text('BookTitle', '--')

        # test change solos
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', 'Sopran')

        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('Alt', 'Alt')

        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_label_text('Tenor', 'Tenor')

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', '')

        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Bass', 'Bass')

        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Bass', '')

        time.sleep(30)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        # test back button
        self.send_ir_command('left')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(10)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        # test button left on state number
        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('left')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        # test button exit on state number
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('right')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('ok')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(119)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(118)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(0.5)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(35)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # restore default settings
        Global.Instances.general_settings.load_defaults()

        # test quit
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(10)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.test_result.finish_test()

    def fsm_delete_number_test(self):
        """Testet ob Ziffern bei der Nummerneingabe gelöscht werden können."""
        # wait until the program has started
        self.test_result.start_test('FSM Delete Number Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test delete number
        self.send_ir_command('8')
        time.sleep(0.5)
        self.send_ir_command('9')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '89')
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '8')
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '')
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # quit
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(10)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.test_result.finish_test()

    def fsm_shutdown_test(self):
        """Prüft ob das Programm das Betriebssystem herunterfährt."""
        # wait until the program has started
        self.test_result.start_test('FSM Shutdown Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def fsm_reboot_test(self):
        """Prüft ob das Programm das Betriebssystem neu startet."""
        # wait until the program has started
        self.test_result.start_test('FSM Reboot Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('3')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')
        self.test_result.finish_test()

    def fsm_shutdown_ok_test(self):
        """Prüft ob das Programm das Betriebssystem mit dem "OK"-Kommando herunterfährt."""
        # wait until the program has started
        self.test_result.start_test('FSM Shutdown OK Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def fsm_ir_settings_test(self):
        """Testet den Einstellungsbereich der StateMachine."""
        # wait until the program has started
        self.test_result.start_test('FSM IR Settings Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        Global.Instances.general_settings.load_defaults()

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(0.1)
        self.send_ir_command('1')
        time.sleep(0.1)
        self.send_ir_command('2')
        time.sleep(0.1)
        self.send_ir_command('3')
        time.sleep(0.1)
        self.send_ir_command('4')
        time.sleep(0.1)
        self.send_ir_command('5')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime', '12:34:5-')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime', '12:34:--')
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(0.1)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(0.1)
        self.send_ir_command('6')
        time.sleep(0.1)
        self.send_ir_command('7')
        time.sleep(0.1)
        self.send_ir_command('8')
        time.sleep(0.1)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDate', '67.89.0---')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDate', '67.89.----')
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # check all left / right
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set clock idle
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.assert_true(Global.Instances.general_settings.clock_idle == '1', 'Check setting clock idle')
        self.check_expected_label_text('LabelSetClockIdle', '1')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockIdle', '2')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check setting clock idle')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockIdle', '1')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.clock_idle == '1', 'Check setting clock idle')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set clock song
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.assert_true(Global.Instances.general_settings.clock_song == '1', 'Check setting clock song')
        self.check_expected_label_text('LabelSetClockSong', '1')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockSong', '2')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.clock_song == '2', 'Check setting clock song')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockSong', '1')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.clock_song == '1', 'Check setting clock song')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set time 3 2
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check setting time 3 2')
        self.check_expected_label_text('LabelSetTime_3_2', '5')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_3_2', '10')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 10.0, 'Check setting time 3 2')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_3_2', '29')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 29.0, 'Check setting time 3 2')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set time 2 1
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 60.0, 'Check setting time 2 1')
        self.check_expected_label_text('LabelSetTime_2_1', '60')
        self.send_ir_command('3')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_2_1', '30')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 30.0, 'Check setting time 2 1')
        self.send_ir_command('4')
        time.sleep(0.1)
        self.send_ir_command('5')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_2_1', '45')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 45.0, 'Check setting time 2 1')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set time 1 5
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 60.0, 'Check setting time 1 5')
        self.check_expected_label_text('LabelSetTime_1_5', '60')
        self.send_ir_command('7')
        time.sleep(0.1)
        self.send_ir_command('8')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_1_5', '78')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 78.0, 'Check setting time 1 5')
        self.send_ir_command('9')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_1_5', '90')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 90.0, 'Check setting time 1 5')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set display server
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.assert_true(Global.Instances.general_settings.display_server == '2', 'Check setting display server')
        self.assert_true(not Global.Instances.display_server.is_running(), 'Check display server running')
        self.check_expected_label_text('LabelSetDisplayServer', '2')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDisplayServer', '1')
        self.assert_true(Global.Instances.general_settings.display_server == '2', 'Check setting display server')
        self.assert_true(not Global.Instances.display_server.is_running(), 'Check display server running')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check setting display server')
        time.sleep(5)
        self.assert_true(Global.Instances.display_server.is_running(), 'Check display server running')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDisplayServer', '2')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(Global.Instances.general_settings.display_server == '2', 'Check setting display server')
        time.sleep(5)
        self.assert_true(not Global.Instances.display_server.is_running(), 'Check display server running')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state show system information
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        Global.Instances.general_settings.load_defaults()

        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def general_settings_test(self):
        """Testet den GeneralSettingsHandler."""
        # wait until the program has started
        self.test_result.start_test('General Settings Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        Global.Instances.general_settings.load_defaults()

        # check default settings
        self.test_result.message('Check default settings')
        self.assert_true(Global.Instances.general_settings.time_1_5 == 60.0, 'Check default time_1_5')
        self.assert_true(Global.Instances.general_settings.time_2_1 == 60.0, 'Check default time_2_1')
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check default time_3_2')
        self.assert_true(Global.Instances.general_settings.clock_idle == '1', 'Check default clock_idle')
        self.assert_true(Global.Instances.general_settings.clock_song == '1', 'Check default clock_song')
        self.assert_true(Global.Instances.general_settings.display_server == '2', 'Check default display_server')

        # check missing file
        self.test_result.message('Check missing file')
        os.remove(Global.CONST.STH.GENERAL.FILEPATH)
        time.sleep(2)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 60.0, 'Check default time_1_5')
        self.assert_true(Global.Instances.general_settings.time_2_1 == 60.0, 'Check default time_2_1')
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check default time_3_2')
        self.assert_true(Global.Instances.general_settings.clock_idle == '1', 'Check default clock_idle')
        self.assert_true(Global.Instances.general_settings.clock_song == '1', 'Check default clock_song')
        self.assert_true(Global.Instances.general_settings.display_server == '2', 'Check default display_server')

        # check modify file
        self.test_result.message('Check modify file')
        time.sleep(2)
        codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8').write('{"clock_idle": "2", "time_2_1": 62.0, '
                                                                           '"clock_song": "2", "time_3_2": 6.0, '
                                                                           '"time_1_5": 61.0, "display_server": "1"}')
        time.sleep(2)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 61.0, 'Check settings time_1_5')
        self.assert_true(Global.Instances.general_settings.time_2_1 == 62.0, 'Check settings time_2_1')
        self.assert_true(Global.Instances.general_settings.time_3_2 == 6.0, 'Check settings time_3_2')
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.assert_true(Global.Instances.general_settings.clock_song == '2', 'Check settings clock_song')
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check settings display_server')

        # check modify corrupt file
        self.test_result.message('Check modify corrupt file')
        time.sleep(2)
        codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8').write('{"clock_idle": "3", "time_2_1": 1000.0, '
                                                                           '"clock_song": "3", "time_3_2": 2.0, '
                                                                           '"time_1_5": 1.0, "display_server": "5"}')
        time.sleep(2)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        self.assert_true(Global.Instances.general_settings.time_2_1 == 900.0, 'Check settings time_2_1')
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.assert_true(Global.Instances.general_settings.clock_song == '2', 'Check settings clock_song')
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check settings display_server')
        # to write to file
        Global.Instances.general_settings.clock_song = '1'

        # check modify defect file
        self.test_result.message('Check modify defect file')
        temp_file = codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8')
        temp_file.write('{"clock_idle": "2000", "time_2_1": 60.0, "clock_song": "1", "time_3_2": 5.0, "time_1_5": 60.0')
        temp_file.close()
        self.assert_true(Global.Instances.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        self.assert_true(Global.Instances.general_settings.time_2_1 == 900.0, 'Check settings time_2_1')
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.assert_true(Global.Instances.general_settings.clock_song == '1', 'Check settings clock_song')
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check settings display_server')

        # check modify empty file
        self.test_result.message('Check empty corrupt file')
        Global.Instances.general_settings.load_defaults()
        time.sleep(2)
        codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8').write('')
        time.sleep(2)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 60.0, 'Check settings time_1_5')
        self.assert_true(Global.Instances.general_settings.time_2_1 == 60.0, 'Check settings time_2_1')
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.assert_true(Global.Instances.general_settings.clock_idle == '1', 'Check settings clock_idle')
        self.assert_true(Global.Instances.general_settings.clock_song == '1', 'Check settings clock_song')
        self.assert_true(Global.Instances.general_settings.display_server == '2', 'Check settings display_server')
        Global.Instances.general_settings.load_defaults()

        # check set settings
        self.test_result.message('Check set settings')
        Global.Instances.general_settings.time_1_5 = '64'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 64.0, 'Check settings time_1_5')
        Global.Instances.general_settings.time_2_1 = '65'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 65.0, 'Check settings time_2_1')
        Global.Instances.general_settings.time_3_2 = '10'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 10.0, 'Check settings time_3_2')
        Global.Instances.general_settings.clock_idle = '2'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check settings clock_idle')
        Global.Instances.general_settings.clock_song = '2'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.clock_song == '2', 'Check settings clock_song')
        Global.Instances.general_settings.display_server = '1'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check settings display_server')

        # check min limits
        self.test_result.message('Check min limits')
        Global.Instances.general_settings.time_1_5 = '1'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        Global.Instances.general_settings.time_1_5 = '-60'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        Global.Instances.general_settings.time_2_1 = '1'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 15.0, 'Check settings time_2_1')
        Global.Instances.general_settings.time_2_1 = '-20'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 15.0, 'Check settings time_2_1')
        Global.Instances.general_settings.time_3_2 = '1'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        Global.Instances.general_settings.time_3_2 = '-10'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')

        # check max limits
        self.test_result.message('Check max limits')
        Global.Instances.general_settings.time_1_5 = '10000'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_1_5 == 180.0, 'Check settings time_1_5')
        Global.Instances.general_settings.time_2_1 = '20000'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_2_1 == 900.0, 'Check settings time_2_1')
        Global.Instances.general_settings.time_3_2 = '30000'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.time_3_2 == 60.0, 'Check settings time_3_2')

        # check possible values
        self.test_result.message('Check possible values')
        Global.Instances.general_settings.clock_idle = '3'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check settings clock_idle')
        Global.Instances.general_settings.clock_idle = 1
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.clock_idle == '2', 'Check settings clock_idle')
        Global.Instances.general_settings.clock_song = '3'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.clock_song == '2', 'Check settings clock_song')
        Global.Instances.general_settings.clock_song = 1
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.clock_song == '2', 'Check settings clock_song')
        Global.Instances.general_settings.display_server = '3'
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check settings display_server')
        Global.Instances.general_settings.display_server = 1
        time.sleep(0.01)
        self.assert_true(Global.Instances.general_settings.display_server == '1', 'Check settings display_server')

        # restore default settings
        Global.Instances.general_settings.load_defaults()

        # exit application
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def fsm_show_ip_test(self):
        """Prüft ob die Seite mit der Netzwerkadresse richtig angezeigt wird."""
        # wait until the program has started
        self.test_result.start_test('FSM Show IP Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.check_expected_label_text('LabelNetworkAddress', u'http://' + socket.getfqdn() + u':48080')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('guide')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        time.sleep(59)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def display_server_test(self):
        """Prüft ob der DisplayServer korrekt funktioniert."""
        # wait until the program has started
        self.test_result.start_test('DisplayServer Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        Global.Instances.general_settings.load_defaults()

        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.assert_true(not Global.Instances.display_server.is_running(), 'Check DisplayServer running')
        Global.Instances.general_settings.display_server = '1'
        time.sleep(5)

        self.assert_true(Global.Instances.display_server.is_running(), 'Check DisplayServer running')
        self.check_url('http://localhost:48080/erstetelefonnachricht', 'daspferdfrisstkeinengurkensalat')
        self.check_url('http://localhost:48080/', '''<!DOCTYPE html>
<html>
    <head>
        <title>MediaDisplay-Display</title>
        <meta charset="utf-8"/>
        <meta http-equiv="refresh" content="3">
    </head>
    <body style="background-color:#000000; color:#ffffff;">
        <h1>Zeige Uhr</h1>
        <footer>
            <p>
                MediaDisplay-Server Display Server
            </p>
            <p>
                Version: ''' + Global.CONST.VERSION + ''' Build: ''' + Global.CONST.BUILD + '''
            </p>
        </footer>
    </body>
</html>''')
        self.check_url('http://localhost:48080', '''<!DOCTYPE html>
<html>
    <head>
        <title>MediaDisplay-Display</title>
        <meta charset="utf-8"/>
        <meta http-equiv="refresh" content="3">
    </head>
    <body style="background-color:#000000; color:#ffffff;">
        <h1>Zeige Uhr</h1>
        <footer>
            <p>
                MediaDisplay-Server Display Server
            </p>
            <p>
                Version: ''' + Global.CONST.VERSION + ''' Build: ''' + Global.CONST.BUILD + '''
            </p>
        </footer>
    </body>
</html>''')

        self.send_ir_command('4')
        self.send_ir_command('+')
        self.send_ir_command('5')
        self.send_ir_command('9')
        time.sleep(6)
        self.check_url('http://localhost:48080', '''<!DOCTYPE html>
<html>
    <head>
        <title>MediaDisplay-Display</title>
        <meta charset="utf-8"/>
        <meta http-equiv="refresh" content="3">
    </head>
    <body style="background-color:#000000; color:#ffffff;">
        <h1> Zeige Lied </h1>
        <h3>Liederbuch: 4</h3>
        <h3>Titel: 59</h3>
        <table style="padding: 10px;">
        </table>

        <footer>
            <p>
                MediaDisplay-Server Display Server
            </p>
            <p>
                Version: ''' + Global.CONST.VERSION + ''' Build: ''' + Global.CONST.BUILD + '''
            </p>
        </footer>
    </body>
</html>''')

        self.send_ir_command('1')
        self.send_ir_command('2')
        self.send_ir_command('3')
        self.send_ir_command('4')
        time.sleep(0.1)
        self.check_url('http://localhost:48080', '''<!DOCTYPE html>
<html>
    <head>
        <title>MediaDisplay-Display</title>
        <meta charset="utf-8"/>
        <meta http-equiv="refresh" content="3">
    </head>
    <body style="background-color:#000000; color:#ffffff;">
        <h1> Zeige Lied </h1>
        <h3>Liederbuch: 4</h3>
        <h3>Titel: 59</h3>
        <table style="padding: 10px;">
            <tr style="background-color:#ff0000; color:#000000;"><td style="padding: 10px;">Sopran</td></tr>
            <tr style="background-color:#ffff00; color:#000000;"><td style="padding: 10px;">Alt</td></tr>
            <tr style="background-color:#67bd5a; color:#000000;"><td style="padding: 10px;">Tenor</td></tr>
            <tr style="background-color:#0000ff; color:#ffffff;"><td style="padding: 10px;">Bass</td></tr>
        </table>

        <footer>
            <p>
                MediaDisplay-Server Display Server
            </p>
            <p>
                Version: ''' + Global.CONST.VERSION + ''' Build: ''' + Global.CONST.BUILD + '''
            </p>
        </footer>
    </body>
</html>''')

        self.send_ir_command('exit')
        time.sleep(0.1)

        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        Global.Instances.general_settings.load_defaults()
        time.sleep(5)
        self.assert_true(not Global.Instances.display_server.is_running(), 'Check DisplayServer running')

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(Global.Instances.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.test_result.finish_test()

    def is_running(self):
        """Gibt zurück ob ein Test ausgeführt wird."""
        self.thread.is_alive()

    def start(self):
        """Start die Testausführung."""
        self.thread.start()

    def stop(self):
        """Beendet die Testausführung und speichet das Ergebnis."""
        self.test_result.finish('TestResult.txt')


def get_time_str():
    """Gibt die aktuelle Uhrzeit zurück.
    
    :return: String, Datum und Uhrzeit
    """
    temp = str(dt.now())[:23]
    if len(temp) < 23:
        temp += '.000'
    return temp


def get_commit():
    """Liefet die ID des aktuellen Kommits zurüc.k
    
    :return: Unicode, ID des aktuellen Kommits.
    """
    return unicode(os.popen('git rev-parse HEAD').read().strip())


class TestResult(object):
    """Implementiert ein Testergebniss. Kann 'Success' oder 'Fail' sein.
    """

    file_end_fail = '''[-------------------------------------------------------------------------------------------------]
[                                     FFFFF    A     I  L                                         ]
[                                     F       A A    I  L                                         ]
[                                     FFFF   AAAAA   I  L                                         ]
[                                     F     A     A  I  LLLLL                                     ]
[-------------------------------------------------------------------------------------------------]
'''

    file_end_success = '''[-------------------------------------------------------------------------------------------------]
[                         SSSS   U   U   CCC   CCC  EEEEE   SSSS    SSSS                          ]
[                        SS      U   U  CC    CC    E      SS      SS                             ]
[                         SSSS   U   U  C     C     EEEEE   SSSS    SSSS                          ]
[                            SS  U   U  CC    CC    E          SS      SS                         ]
[                         SSSS    UUU    CCC   CCC  EEEEE   SSSS    SSSS                          ]
[-------------------------------------------------------------------------------------------------]
'''

    def __init__(self):
        self.start_time = dt.now()
        self.finish_time = None
        self.tests_number = 0
        self.test_failed = 0
        self.tests_succeded = 0
        self.current_test_verdict = 'Success'
        self.__log_text = '''[-------------------------------------------------------------------------------------------------]
[                                                                                                 ]
[                                           TEST RESULT                                           ]
[                                                                                                 ]
[-------------------------------------------------------------------------------------------------]
'''

    def get_duration(self):
        """Gibt zurück wie viel Zeit seit dem Start des Tests vergangen ist.
        
        :return: Int Testdauer in Sekunden
        """
        if not self.finish_time:
            return (dt.now() - self.start_time).total_seconds()
        else:
            return (self.finish_time - self.start_time).total_seconds()

    def __log(self, message, severity=u''):
        """Loggt eine Meldung und gibt sie auf der Konsole aus.
        
        :param message: Unicode Meldung, die geloggt werden soll.
        :param severity: Unicode Gewichtung der Meldung.
        :return: 
        """
        text = u'[' + get_time_str() + u'] [% 4s] ' % severity + message
        print text
        self.__log_text += text + '\n'

    def fail(self, message=u''):
        """Loggt eine Meldung mit "Fail" als Gewichtung. Der Test gilt danach als "Fail".
        
        :param message: Unicode Meldung, die geloggt werden soll.
        """
        self.current_test_verdict = u'Fail'
        self.__log(message, u'Fail')

    def pass_(self, message=u''):
        """Loggt eine Meldung mit "pass" als Gewichtung.
        
        :param message: Unicode Meldung, die geloggt werden soll.
        """
        self.__log(message)

    def message(self, message=u''):
        """Loggt eine Meldung ohne Gewichtung.

        :param message: Unicode Meldung, die geloggt werden soll.
        """
        self.__log(message, u'~')

    def write(self, file_name):
        """Schreibt das Testergebiss in eine Datei.
        
        :param file_name: String, Pfad unter dem gespeichert wird.
        """
        with codecs.open(file_name, 'w', 'utf-8') as result_file:
            result_file.write(self.__log_text)

    def start_test(self, test_name):
        """Muss zum Starten des Tests aufgerufen werden.
        
        :param test_name: Name des Tests
        """
        self.current_test_verdict = None
        self.tests_number += 1
        self.__log(u'Test %s started' % test_name, u'----')

    def finish_test(self):
        """Muss zum Beenden des Tests aufgerufen werden."""
        if self.current_test_verdict is None:
            self.tests_succeded += 1
            self.__log(u'Test finished', u'OK')
        else:
            self.test_failed += 1
            self.__log(u'Test finished', u'Fail')

    def finish(self, file_name):
        """Schließt eine Testausführugn ab und speichert das Log.
        
        :param file_name: Unicode, Pfad unter dem das Ergebniss gespeichert wird.
        :return: 
        """
        if self.finish_time:
            return
        self.finish_time = dt.now()
        if self.test_failed:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), u'Fail')
            self.__log_text += TestResult.file_end_fail
        else:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), u'OK')
            self.__log_text += TestResult.file_end_success
        self.write(file_name)
