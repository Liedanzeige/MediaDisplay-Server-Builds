#! /bin/bash

echo "change to home dir"
cd /home/pi/

echo "apt-get update"
sudo apt-get update
sudo apt-get upgrade

echo "install pyside"
sudo apt-get -y install python-pyside

echo "Install LIRC"
sudo apt-get -y install lirc
sudo apt-get -y install python-lirc
