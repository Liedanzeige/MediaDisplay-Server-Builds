# -*- coding: utf-8 -*-
"""Stellt eine Klasse zur Interaktion mit der GUI zur Verfügung.
"""

import Global
import SongHandler
import NetworkQrCodeHandler


class ScreenHandler(object):
    """Klasse zur Interaktion mit der GUI."""

    def __init__(self, application):
        self.__application = application
        self.__cache = ''
        self.__song_factory = SongHandler.SongFactory(application.general_settings)
        self.__current_song = SongHandler.Song()
        self.__qr_handler = NetworkQrCodeHandler.NetworkQtCodeHandler(application)

    def init_values(self):
        """Initialisiert alle Variablen des ScreenHandlers."""
        self.__cache = ''
        self.__current_song = SongHandler.Song()

    def __load_song(self, song):
        """Setzt den Inhalt der \"Zeige Lied\"-Seite"""

        label_number = self.__application.main_window.ids['SongNumber']
        label_sopran = self.__application.main_window.ids['Sopran']
        label_alt = self.__application.main_window.ids['Alt']
        label_tenor = self.__application.main_window.ids['Tenor']
        label_bass = self.__application.main_window.ids['Bass']
        label_title = self.__application.main_window.ids['Title']

        label_number.set_text.emit(song.get_label_text())
        label_number.fit_font_size.emit()

        if song.sopran:
            label_sopran.set_text.emit(song.sopran_name)
            label_sopran.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_BG +
                                              u"; color: " + Global.CONST.SCH.COLOR.SOLO_SOPRAN_FG + u" }")
        else:
            label_sopran.set_text.emit('')
            label_sopran.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG +
                                              u"; color : " + Global.CONST.SCH.COLOR.FG + u"; }")
        label_sopran.fit_font_size.emit()

        if song.alt:
            label_alt.set_text.emit(song.alt_name)
            label_alt.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_ALT_BG +
                                           u"; color: " + Global.CONST.SCH.COLOR.SOLO_ALT_FG + u" }")
        else:
            label_alt.set_text.emit('')
            label_alt.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG +
                                           u"; color : " + Global.CONST.SCH.COLOR.FG + u"; }")
        label_alt.fit_font_size.emit()

        if song.tenor:
            label_tenor.set_text.emit(song.tenor_name)
            label_tenor.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_TENOR_BG +
                                             u"; color: " + Global.CONST.SCH.COLOR.SOLO_TENOR_FG + u" }")
        else:
            label_tenor.set_text.emit('')
            label_tenor.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG +
                                             u"; color : " + Global.CONST.SCH.COLOR.FG + u"; }")
        label_tenor.fit_font_size.emit()

        if song.bass:
            label_bass.set_text.emit(song.bass_name)
            label_bass.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SOLO_BASS_BG +
                                            u"; color: " + Global.CONST.SCH.COLOR.SOLO_BASS_FG + u" }")
        else:
            label_bass.set_text.emit('')
            label_bass.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG +
                                            u"; color : " + Global.CONST.SCH.COLOR.FG + u"; }")
        label_bass.fit_font_size.emit()

        label_title.set_text.emit(song.title)
        label_title.fit_font_size.emit()

    def has_valide_song(self):
        """Gibt zurück ob ein gültiges Lied eingegeben wurde.
        
        :return: Bool; True, wenn ein gültiges Lied eingegeben wurde.
        """
        song = self.__song_factory.createSong(self.__cache)
        return song.is_valid()

    def song_completed(self):
        """Gibt zurück ob die Eingabe des Liedes vollständig ist.
        
        :return: Bool; True, wenn die Eingabe des Liedes vollständig ist.
        """
        return self.__cache.count(Global.CONST.SCH.DELIMITER) >= 2

    def add_number(self, number):
        """Fügt auf der Seite \"Zeige Nummer\" eine Nummer der Anzeige hinzu.
        
        :param number: String Nummer, die der Anzeige hinzugefügt wird.
        """
        if number in Global.CONST.SM.IRCMD.N_LIST:
            self.__cache += number
        else:
            self.__cache += Global.CONST.SCH.DELIMITER
        self.__show_number()

    def delete_number(self):
        """Löscht auf der Seite \"Zeige Nummer\" eine Nummer."""
        if self.__cache:
            self.__cache = self.__cache[:-1]
        self.__show_number()

    def __show_number(self):
        """Zeit die eingegebene Nummer auf dem Bildschirm an."""
        number_str = self.__cache
        if Global.CONST.SCH.DELIMITER in self.__cache:
            temp_list = self.__cache.split(Global.CONST.SCH.DELIMITER)
            temp_list[0] = u'<span style="color:' + Global.CONST.SCH.COLOR.BOOK + u';">' + temp_list[0] + u'</span>'
            number_str = Global.CONST.SCH.DELIMITER.join(temp_list)
        label = self.__application.main_window.ids['Number']
        label.set_text.emit(number_str)
        label.fit_font_size.emit()

    def toggle_solo(self, voice):
        """Schaltet die Anzeige der Solostimmen um.
        
        :param voice: String '1' - '4' Stimme die umgeschaltet werden soll.
        """
        if self.__application.main_window.current == Global.CONST.SM.STATE_SONG:

            if voice == '1':
                self.__current_song.sopran = not self.__current_song.sopran
            elif voice == '2':
                self.__current_song.alt = not self.__current_song.alt
            elif voice == '3':
                self.__current_song.tenor = not self.__current_song.tenor
            elif voice == '4':
                self.__current_song.bass = not self.__current_song.bass

            self.__application.song_handler.update_latest_song(self.__current_song)
            self.__load_song(self.__current_song)

    def set_page(self, target_state, gui_params):
        """Zeigt eine Zeit der GUI an.
        
        :param target_state: Int Index des Seite die angezeigt werden soll.
        :param gui_params: Obj Parameter für die Anzeige.
        """
        if target_state == Global.CONST.SM.STATE_START:
            label = self.__application.main_window.ids['LabelStartLiedanzeige']
            label.fit_font_size.emit()
            label = self.__application.main_window.ids['LabelStartVersion']
            label.fit_font_size.emit()
            label = self.__application.main_window.ids['LabelStartCopyright']
            label.fit_font_size.emit()
            label = self.__application.main_window.ids['LabelStartState']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_IDLE:
            pass
        elif target_state == Global.CONST.SM.STATE_SONG:
            if gui_params is None:
                song = self.__song_factory.createSong(self.__cache)
                self.__current_song = song
                self.__application.song_handler.sing_song(song)
                self.__load_song(song)
            # Für Taste '<-' in IDLE
            elif gui_params == 'show_again':
                song = self.__application.song_handler.get_latest_song()
                self.__load_song(song)
            elif isinstance(gui_params, SongHandler.Song):
                self.__current_song = gui_params
                self.__application.song_handler.sing_song(gui_params)
                self.__load_song(gui_params)
            else:
                pass

            if 'SongScreenClock' in self.__application.main_window.ids:
                label = self.__application.main_window.ids['SongScreenClock']
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_NUMBER:
            self.__cache = gui_params
            self.__show_number()
        elif target_state == Global.CONST.SM.STATE_BLUESCREEN:
            label = self.__application.main_window.ids['Oops']
            label.fit_font_size.emit()
            label = self.__application.main_window.ids['BluescreenLabel']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_STANDBY:
            label = self.__application.main_window.ids['StandbyLabel']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_QUIT:
            label = self.__application.main_window.ids['LabelQuit']
            label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_CONFIRM_QUIT:
            for label in self.__application.main_window.ids['LabelsConfirmQuitList']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_TIME:
            for label in self.__application.main_window.ids['LabelsSetTimeList']:
                label.fit_font_size.emit()
            self.__cache = ''
            self.show_time_str()
        elif target_state == Global.CONST.SM.STATE_SET_DATE:
            for label in self.__application.main_window.ids['LabelsSetDateList']:
                label.fit_font_size.emit()
            self.__cache = ''
            self.show_date_str()
        elif target_state == Global.CONST.SM.STATE_SET_CLOCK_IDLE:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetClockIdle']
            label.set_text.emit(self.__application.general_settings.clock_idle)
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetClockIdleList']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_CLOCK_SONG:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetClockSong']
            label.set_text.emit(self.__application.general_settings.clock_song)
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetClockSongList']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetTime_3_2']
            label.set_text.emit(str(int(self.__application.general_settings.time_3_2)))
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetTime_3_2_List']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_TIME_SONG_IDLE:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetTime_2_1']
            label.set_text.emit(str(int(self.__application.general_settings.time_2_1)))
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetTime_2_1_List']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetTime_1_5']
            if self.__application.general_settings.time_1_5 == 0.0:
                label.set_text.emit(u'\u221E')
            else:
                label.set_text.emit(str(int(self.__application.general_settings.time_1_5)))
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetTime_1_5_List']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SYSTEM_INFORMATION:
            for label in self.__application.main_window.ids['LabelsSysteminformation']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_IP:
            label = self.__application.main_window.ids['LabelNetworkAddress']
            url_text = self.__qr_handler.get_url_text()
            label.set_text.emit(url_text)
            label.fit_font_size.emit()
            self.__application.main_window.current = target_state
            label_qr = self.__application.main_window.ids['LabelNetworkQrCode']
            image_path = self.__qr_handler.get_image_path(url_text)
            label_qr.set_image.emit(image_path)
        elif target_state == Global.CONST.SM.STATE_SET_DISPLAY_SERVER:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetDisplayServer']
            label.set_text.emit(self.__application.general_settings.display_server)
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetDisplayServerList']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS:
            self.__cache = Global.CONST.SCH.DELIMITER.join(self.__application.general_settings.general_song_books)
            label = self.__application.main_window.ids['LabelSetGeneralSongBooks']
            label.set_text.emit(Global.CONST.SCH.DELIMITER.join(self.__application.general_settings.general_song_books))
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetGeneralSongBooks']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_SET_ORDER_SOLO:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelSetOrderSolo']
            label.set_text.emit(self.__application.general_settings.order_solo)
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                       u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
            for label in self.__application.main_window.ids['LabelsSetOrderSolo']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_CONFIRM_UPDATE:
            self.__cache = ''
            label = self.__application.main_window.ids['LabelConfirmUpdate']
            label.set_text.emit(u'')
            for label in self.__application.main_window.ids['LabelsConfirmUpdate']:
                label.fit_font_size.emit()
        elif target_state == Global.CONST.SM.STATE_DISPLAY_MESSAGE:
            label = self.__application.main_window.ids['Message']
            label.set_text.emit(gui_params)
            label.fit_font_size.emit()
        self.__application.main_window.current = target_state

    def add_time_char(self, char):
        """Fügt auf der Seite "Stelle Uhrzeit" ein Zeichen der neuen Uhrzeit hinzu oder löscht das letzte Zeichen.
        
        :param char: None oder String '0' - '9' wird der Anzeige hinzugefügt. Bei None wird das letzte Zeichen gelöscht.
        """
        if char:
            if len(self.__cache) < 6:
                self.__cache = self.__cache + char
        else:
            if self.__cache:
                self.__cache = self.__cache[:-1]
        self.show_time_str()

    def show_time_str(self):
        """Zeigt die eingegebene, neue Uhrzeit auf der Seite "Stelle Uhrzeit" an."""
        length = len(self.__cache)
        if length == 0:
            text = '--:--:--'
        elif length == 1:
            text = self.__cache + '-:--:--'
        elif length == 2:
            text = self.__cache + ':--:--'
        elif length == 3:
            text = self.__cache[:2] + ':' + self.__cache[2:] + '-:--'
        elif length == 4:
            text = self.__cache[:2] + ':' + self.__cache[2:] + ':--'
        elif length == 5:
            text = self.__cache[:2] + ':' + self.__cache[2:4] + ':' + self.__cache[4:] + '-'
        elif length == 6:
            text = self.__cache[:2] + ':' + self.__cache[2:4] + ':' + self.__cache[4:]
        else:
            text = self.__cache

        label = self.__application.main_window.ids['LabelSetTime']
        label.set_text.emit(text)
        label.fit_font_size.emit()

    def set_time(self):
        """Setzt die eingegebene Uhrzeit als aktuelle Uhrzeit."""
        if len(self.__cache) == 4:
            time_str = self.__cache[:2] + ':' + self.__cache[2:4] + ':00'
            self.__application.os_interface.set_date_time(time_str)
        elif len(self.__cache) == 6:
            time_str = self.__cache[:2] + ':' + self.__cache[2:4] + ':' + self.__cache[4:]
            self.__application.os_interface.set_date_time(time_str)

    def add_date_char(self, char):
        """Fügt auf der Seite "Stelle Datum" ein Zeichen dem neuen Datum hinzu oder löscht das letzte Zeichen.

        :param char: None oder String '0' - '9' wird der Anzeige hinzugefügt. Bei None wird das letzte Zeichen gelöscht.
        """
        if char:
            if len(self.__cache) < 8:
                self.__cache = self.__cache + char
        else:
            if self.__cache:
                self.__cache = self.__cache[:-1]
        self.show_date_str()

    def show_date_str(self):
        """Zeigt das eingegebene, neue Datum auf der Seite "Stelle Datum" an."""
        length = len(self.__cache)
        if length == 0:
            text = '--.--.----'
        elif length == 1:
            text = self.__cache + '-.--.----'
        elif length == 2:
            text = self.__cache + '.--.----'
        elif length == 3:
            text = self.__cache[:2] + '.' + self.__cache[2:] + '-.----'
        elif length == 4:
            text = self.__cache[:2] + '.' + self.__cache[2:] + '.----'
        elif length == 5:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:] + '---'
        elif length == 6:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:] + '--'
        elif length == 7:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:] + '-'
        elif length == 8:
            text = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:]
        else:
            text = self.__cache

        label = self.__application.main_window.ids['LabelSetDate']
        label.set_text.emit(text)
        label.fit_font_size.emit()

    def set_date(self):
        """Setzt das eingegebene Datum als aktuelles Datum."""
        if len(self.__cache) == 8:
            time_str = self.__cache[:2] + '.' + self.__cache[2:4] + '.' + self.__cache[4:]
            self.__application.os_interface.set_date_time(time_str)

    def set_setting_char(self, char):
        """Zeit ein neues Zeichen auf den Einstellungsseiten an.
        
        :param char: String '0' - '9' wird angezeigt.
        """
        if self.__application.state_machine.state == Global.CONST.SM.STATE_SET_CLOCK_IDLE:
            label = self.__application.main_window.ids['LabelSetClockIdle']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_CLOCK_SONG:
            label = self.__application.main_window.ids['LabelSetClockSong']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_DISPLAY_SERVER:
            label = self.__application.main_window.ids['LabelSetDisplayServer']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_ORDER_SOLO:
            label = self.__application.main_window.ids['LabelSetOrderSolo']
        else:
            return
        self.__cache = char
        label.set_text.emit(self.__cache)
        label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                   u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        label.fit_font_size.emit()

    def add_setting_char(self, char):
        """Fügt auf den Einstellungsseiten ein Zeichen der Eingabe hinzu oder löscht das letzte Zeichen.
        
        :param char: None oder String '0' - '9' wird der Anzeige hinzugefügt. Bei None wird das letzte Zeichen gelöscht.
        """
        if self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG:
            label = self.__application.main_window.ids['LabelSetTime_3_2']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_SONG_IDLE:
            label = self.__application.main_window.ids['LabelSetTime_2_1']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY:
            label = self.__application.main_window.ids['LabelSetTime_1_5']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS:
            label = self.__application.main_window.ids['LabelSetGeneralSongBooks']
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_CONFIRM_UPDATE:
            label = self.__application.main_window.ids['LabelConfirmUpdate']
        else:
            return
        if char:
            self.__cache = self.__cache + char
        else:
            if self.__cache:
                self.__cache = self.__cache[:-1]
        if self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY and \
                self.__cache == '0':
            label.set_text.emit(u'\u221E')
        else:
            label.set_text.emit(self.__cache)
        label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                   u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_EDIT + u"; }")
        label.fit_font_size.emit()

    def set_setting(self):
        """Setzt eine Einstellung in Abhängigkeit der angezeigten Seite."""
        if self.__application.state_machine.state == Global.CONST.SM.STATE_SET_CLOCK_IDLE:
            self.__application.general_settings.clock_idle = self.__cache
            label = self.__application.main_window.ids['LabelSetClockIdle']
            label.set_text.emit(self.__application.general_settings.clock_idle)
            self.__cache = ''
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_CLOCK_SONG:
            self.__application.general_settings.clock_song = self.__cache
            label = self.__application.main_window.ids['LabelSetClockSong']
            label.set_text.emit(self.__application.general_settings.clock_song)
            self.__cache = ''
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG:
            self.__application.general_settings.time_3_2 = self.__cache
            label = self.__application.main_window.ids['LabelSetTime_3_2']
            label.set_text.emit(str(int(self.__application.general_settings.time_3_2)))
            self.__cache = ''
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_SONG_IDLE:
            self.__application.general_settings.time_2_1 = self.__cache
            label = self.__application.main_window.ids['LabelSetTime_2_1']
            label.set_text.emit(str(int(self.__application.general_settings.time_2_1)))
            self.__cache = ''
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY:
            self.__application.general_settings.time_1_5 = self.__cache
            label = self.__application.main_window.ids['LabelSetTime_1_5']
            if self.__application.general_settings.time_1_5 == 0.0:
                label.set_text.emit(u'\u221E')
            else:
                label.set_text.emit(str(int(self.__application.general_settings.time_1_5)))
            self.__cache = ''
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_DISPLAY_SERVER:
            self.__application.general_settings.display_server = self.__cache
            label = self.__application.main_window.ids['LabelSetDisplayServer']
            label.set_text.emit(self.__application.general_settings.display_server)
            self.__cache = ''
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS:
            self.__application.general_settings.general_song_books = self.__cache
            label = self.__application.main_window.ids['LabelSetGeneralSongBooks']
            label.set_text.emit(Global.CONST.SCH.DELIMITER.join(self.__application.general_settings.general_song_books))
            self.__cache = Global.CONST.SCH.DELIMITER.join(self.__application.general_settings.general_song_books)
        elif self.__application.state_machine.state == Global.CONST.SM.STATE_SET_ORDER_SOLO:
            self.__application.general_settings.order_solo = self.__cache
            label = self.__application.main_window.ids['LabelSetOrderSolo']
            label.set_text.emit(self.__application.general_settings.order_solo)
            self.__cache = ''
        else:
            return
        label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.SETTINGS_BG + u"; "
                                   u"color : " + Global.CONST.SCH.COLOR.SETTINGS_FG_OK + u"; }")
        label.fit_font_size.emit()

    def get_cache(self):
        """Gibt den Inhalt des Cache zurück.

        :return: Sting, Cache
        """
        return self.__cache

    def set_start_screen_message(self, message, error=False):
        """Zeigt eine Meldung auf dem Startbildschirm an

        :param message: Meldung die angezeigt werden soll,
        :param error: Gibt an, ob es eine Fehlermeldung ist
        """
        label = self.__application.main_window.ids['LabelStartState']
        label.set_text.emit(message)
        label.fit_font_size.emit()
        if error:
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.ERROR_BG +
                                       u"; color: " + Global.CONST.SCH.COLOR.FG + u" }")
        else:
            label.set_style_sheet.emit(u"QLabel { background-color : " + Global.CONST.SCH.COLOR.BG + u"; color : " +
                                       Global.CONST.SCH.COLOR.FG + u"; }")
