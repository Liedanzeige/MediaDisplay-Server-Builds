# -*- coding: utf-8 -*-
"""Dieses Modul stellt einige Klassen zur Verwaltung von Liedern zur Verfügung."""

import Global

import codecs
import datetime
import re
import os


class SongHandler(object):
    """Klasse zur Verwaltung der angezeigten Lieder."""

    def __init__(self, application):
        self.__application = application
        self.__sung_songs = list()

    def has_song(self):
        """Gibt zurück ob ein Lied nach dem Start des Programms angezeigt wurde.
        
        :return: Bool; True, wenn mindestens ein Lied angezeigt wurde.
        """
        return bool(self.__sung_songs)

    def sing_song(self, song):
        """Fügt ein Lied der Liste der gesungenen Lieder hinzu.
        
        :param song: Lied das angezeigt wird.
        """
        self.__sung_songs.append(song)
        if len(self.__sung_songs) > 100:
            self.__sung_songs = self.__sung_songs[-100:]

    def get_latest_song(self):
        """Gibt das zuletzt angezeigte Lied zurück.
        
        :return: Song; Zuletzt angezeigtes Lied oder None wenn noch kein Lied gesungen wurde.
        """
        if self.__sung_songs:
            return self.__sung_songs[-1]
        else:
            return None

    def update_latest_song(self, song):
        """Ändert das zuletzt angezeigte Lied. Kann verwendet werden, wenn z.B. die Solostimmen sich geändert haben.
        
        :param song: Lied das angezeigt wird.
        """
        self.__sung_songs[-1] = song

    def get_sung_songs(self):
        return tuple(self.__sung_songs)


class SongFactory(object):

    def __init__(self, settings_handler):
        self.__settings_handler = settings_handler

    def createSong(self, data):
        song = Song()
        if isinstance(data, (str, unicode)):
            song.book_id, song.book_abbreviation, song.book_color, song.number, song.title, solos = \
                self.parse_string(data)
            song.sopran, song.alt, song.tenor, song.bass = solos
        song.general_song = self.__settings_handler is not None and \
                            song.book_id in self.__settings_handler.general_song_books
        return song

    @staticmethod
    def parse_string(data):
        """Parst Daten für das Lied aus einem String.

        :param data: String, der geparst wird.
        :return: Tuple: (book, book_abbreviation, book_color, number, title, (sopran, alt, tenor, bass))
        """
        sopran = False
        alt = False
        tenor = False
        bass = False
        has_solos = False
        book_color = Global.CONST.SCH.COLOR.BOOK

        temp = data.split(Global.CONST.SCH.DELIMITER)

        if len(temp) == 1:
            number = temp[0]
            book = ''
        elif len(temp) == 2:
            if temp[1] == '':
                number = temp[0]
                book = ''
            else:
                number = temp[1]
                book = temp[0]
        elif len(temp) >= 3:
            has_solos = True
            if temp[1] == '':
                number = temp[0]
                book = ''
            else:
                number = temp[1]
                book = temp[0]
            if '1' in temp[2]:
                sopran = True
            if '2' in temp[2]:
                alt = True
            if '3' in temp[2]:
                tenor = True
            if '4' in temp[2]:
                bass = True
        else:
            number = ''
            book = ''

        # check book number
        if book != '':
            try:
                book_nr = int(book)
            except ValueError:
                book = Global.CONST.SOH.INVALID_BOOK
            else:
                if book_nr < Global.CONST.SCH.MIN_BOOK:
                    book = Global.CONST.SOH.INVALID_BOOK
                elif book_nr > Global.CONST.SCH.MAX_BOOK:
                    book = Global.CONST.SOH.INVALID_BOOK
                else:
                    if book.startswith('0'):
                        book = '0' + book.lstrip('0')
                    else:
                        book = book.lstrip('0')

        book_abbreviation = book
        title = u''

        if os.path.isfile(os.path.join(Global.CONST.BOOK_DIR, book + '.txt')):
            try:
                book_file = codecs.open(os.path.join(Global.CONST.BOOK_DIR, book + '.txt'), 'r', 'utf-8')
                lines = book_file.readlines()
                book_abbreviation = lines[0].strip().split(' ')[0]
                book_color = Global.CONST.SCH.COLOR.BOOK
                color = lines[0].strip().split(' ')[1]
                if SongFactory.__check_color(color):
                    book_color = '#' + color
                for line in lines[1:]:
                    if line.startswith(number + ' '):
                        splitted_line = line.strip().split(' ')
                        number = splitted_line[1]
                        if not has_solos:
                            if '1' in splitted_line[2]:
                                sopran = True
                            else:
                                sopran = False
                            if '2' in splitted_line[2]:
                                alt = True
                            else:
                                alt = False
                            if '3' in splitted_line[2]:
                                tenor = True
                            else:
                                tenor = False
                            if '4' in splitted_line[2]:
                                bass = True
                            else:
                                bass = False
                        title = u' '.join(splitted_line[3:])
            except Exception:
                pass

        return book, book_abbreviation, book_color, number, title, (sopran, alt, tenor, bass)

    @classmethod
    def __check_color(cls, color):
        """Gibt zurück ob die angegebene Farbe valider Hexcode ist.

        :param color: Farbe die überprüft wird.
        :return: Booolean; Wahr, wenn color eine valider Hexcode ist.
        """
        return bool(re.match(r'\A[0-9a-fA-F]{6}\Z', color))


class Song(object):
    """Repräsentiert ein Lied"""

    def __init__(self):
        self.number = ''
        self.title = ''
        self.book_id = ''
        self.book_abbreviation = ''
        self.book_color = Global.CONST.SCH.COLOR.BOOK
        self.sopran = False
        self.alt = False
        self.tenor = False
        self.bass = False
        self.sopran_name = u'Sopran'
        self.alt_name = u'Alt'
        self.tenor_name = u'Tenor'
        self.bass_name = u'Bass'
        self.general_song = False
        self.date = datetime.datetime.now()

    def is_valid(self):
        """Gibt zurück ob das Lied valide ist.
        
        :return: Boolean; Wahr, wenn das Lied entweder eine Nummer oder ein Buch hat.
        """
        return bool(self.book_abbreviation) or bool(self.number)

    def get_label_text(self):
        """Gibt einen Text für ein Label der GUI zurück.
        
        :return: String; Text für ein Label der GUI.
        """
        if self.book_abbreviation:
            return u'<span style="color:' + self.book_color + u';">' + self.book_abbreviation + u'</span>' + u' ' + \
                   self.number
        else:
            return self.number
