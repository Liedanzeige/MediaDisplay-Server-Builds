function load_weather() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200){
                data = JSON.parse(this.responseText);
                document.getElementById("temp").innerHTML = parseFloat(Math.round(data.temp * 10) / 10).toFixed(1);
                document.getElementById("pressure").innerHTML = parseFloat(Math.round(data.pressure * 10) / 10).toFixed(1);
                document.getElementById("humidity").innerHTML = parseFloat(Math.round(data.humidity * 10) / 10).toFixed(1);
                var velocity_element = document.getElementById("velocity");
                if (velocity_element != null){
                    velocity_element.innerHTML = parseFloat(Math.round((332.5 + 0.6 * data.temp) * 10) / 10).toFixed(1);
                }
                document.getElementById("date").innerHTML = data.date.slice(0, 19);
                document.getElementById("message").innerHTML = '';
                document.getElementById("message").style.padding = '0em';
            }
            else {
                document.getElementById("message").innerHTML = 'Fehler bei Serverabfrage';
                document.getElementById("message").style.padding = '0.5em';
            }
        }
    };
    xhttp.open("GET", "weather.json", true);
    xhttp.send();
}

load_weather()
setInterval(load_weather, 1000);
