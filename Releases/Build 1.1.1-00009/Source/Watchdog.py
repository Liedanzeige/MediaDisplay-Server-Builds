# -*- coding: utf-8 -*-
"""Stellt eine Klasse \"WatchDog\" zum Überwachen der Module zur Verfügung."""

import time
import threading

import Global
import Logger
import DiagnosticModule


class Watchdog(object):
    """Klasse um ausgefallene Instanzen neu zu Starten."""

    def __init__(self, application):
        self.__application = application
        self.is_started = False
        self.allow_stop = False
        self.__run = True
        self.__thread = threading.Thread(target=self.__loop, args=(), name='Watchdog')
        self.__thread.start()
        self.restart = False

    def __restart_instances(self):
        """Starte im Bedarfsfall alle Instanzen neu."""
        self.is_started = False
        self.__application.os_interface.set_led(Global.CONST.OSI.COLORS.BLUE)

        self.__application.os_interface.switch_hdmi_on()

        # stop all instances
        self.__application.logger.stop()
        self.__application.state_machine.stop()
        self.__application.display_server.stop()
        self.__application.state_machine.shutdown_mode = Global.CONST.SM.NO_SHUTDOWN

        # set bluescreen
        self.__application.main_window.current = Global.CONST.SM.STATE_DICT[Global.CONST.SM.STATE_BLUESCREEN]
        time.sleep(5)
        time.sleep(2 * max(Global.CONST.SM.TIME_INCREMENT, Global.CONST.OSI.TIME_INCREMENT_LED))
        self.__application.os_interface.stop()
        # wait unit all instances are stopped
        while self.__application.logger.is_running() \
                or self.__application.state_machine.is_running() \
                or self.__application.display_server.is_running() \
                or self.__application.os_interface.is_running():
            time.sleep(0.01)

        self.__application.logger.start()
        self.__application.state_machine.init_values()
        self.__application.screen_handler.init_values()
        self.__application.state_machine.start()
        self.restart = False

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False
        self.allow_stop = True
        if self.__thread is not None:
            self.__thread.join(2 * Global.CONST.WD.TIME_INCREMENT)

    def __update(self):
        if not self.is_started or self.allow_stop:
            return

        if self.__application.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_ON and \
                not self.__application.display_server.is_running():
            self.__application.logger.log(u'Start Display Server', __name__, Logger.SEVERITY_INFO)
            self.__application.display_server.start()
        if self.__application.general_settings.display_server == Global.CONST.STH.GENERAL.DISPLAY_SERVER_OFF and \
                self.__application.display_server.is_running():
            self.__application.logger.log(u'Stop Display Server', __name__, Logger.SEVERITY_INFO)
            self.__application.display_server.stop()

        if not self.__application.logger.is_running():
            self.__application.logger.log(u'Restart Logger', __name__, Logger.SEVERITY_ERROR)
            self.__application.logger.start()
        if not (not self.restart and self.__application.state_machine.is_running() and
                self.__application.os_interface.is_running()):
            if self.is_started and not self.allow_stop:  # check again because of multi threading
                self.__application.logger.log(u'Restart all instances', __name__, Logger.SEVERITY_ERROR)
                self.__restart_instances()

    @DiagnosticModule.execute_with_exception_handling
    def __loop(self):
        while self.__run:
            try:
                self.__update()
            except Exception as e:
                self.__application.logger.log(u'Exception occurred during update Watchdog: %s' % e,
                                              __name__, Logger.SEVERITY_ERROR)
            time.sleep(Global.CONST.WD.TIME_INCREMENT)
