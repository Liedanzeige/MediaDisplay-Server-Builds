# -*- coding: utf-8 -*-
"""Stellt eine Klasse zur Interaktion mit dem Betriebssystem zu Verfügung.
"""

try:
    import lirc
    import RPi.GPIO as GPIO
    import neopixel
    import Adafruit_BME280 as BME280
except ImportError:
    pass  # this is a workaround for Sphinx documentation
import threading
import time
from datetime import datetime, timedelta
import os
import subprocess

import Global
import Logger
import DiagnosticModule


class OsInterface(object):
    """Stellt einige Funktionen zur Interaktion mit dem Betriebssystem zu Verfügung."""
    HOUR = timedelta(hours=1)
    MINUTE = timedelta(minutes=1)

    def __init__(self, application):
        self.__application = application
        self.__run = False
        self.__led_thread = None
        self.__util_thread = None
        self.red_led = None
        self.green_led = None
        self.blue_led = None
        self.env_sensor = None
        self.__commands = list()
        self.__time_color = Global.CONST.OSI.COLORS.BLACK
        self.__color_time_counter = 0
        self.__static_color = None
        self.__serial_number = None
        self.__led_strip = None

    def init_hardware(self):
        """Initialisiert die Hardware (lirc und GPIO)."""

        self.__application.logger.log('Start init lirc...', __name__, Logger.SEVERITY_DEBUG)
        try:
            lirc.init("ir_remote", Global.CONST.OSI.LIRC_CONFIG_FILE_PATH, blocking=False)
        except lirc.InitError:
            self.__application.logger.log('InitError occurred initialising lirc', __name__, Logger.SEVERITY_ERROR)
        except lirc.ConfigLoadError:
            self.__application.logger.log('ConfigLoadError occurred initialising lirc', __name__,
                                          Logger.SEVERITY_ERROR)
        self.__application.logger.log('lirc inited', __name__, Logger.SEVERITY_DEBUG)

        self.__application.logger.log('Start init GPIO...', __name__, Logger.SEVERITY_DEBUG)
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(Global.CONST.OSI.RED_LED, GPIO.OUT)
        GPIO.setup(Global.CONST.OSI.GREEN_LED, GPIO.OUT)
        GPIO.setup(Global.CONST.OSI.BLUE_LED, GPIO.OUT)

        self.red_led = GPIO.PWM(Global.CONST.OSI.RED_LED, 1000)
        self.green_led = GPIO.PWM(Global.CONST.OSI.GREEN_LED, 1000)
        self.blue_led = GPIO.PWM(Global.CONST.OSI.BLUE_LED, 1000)

        self.red_led.start(0)
        self.green_led.start(0)
        self.blue_led.start(0)
        self.__application.logger.log('GPIO inited', __name__, Logger.SEVERITY_DEBUG)

        # led strip
        self.__led_strip = neopixel.Adafruit_NeoPixel(Global.CONST.OSI.LED_STRIP.COUNT,
                                                      Global.CONST.OSI.LED_STRIP.GPIO_PIN,
                                                      Global.CONST.OSI.LED_STRIP.FREQ,
                                                      Global.CONST.OSI.LED_STRIP.DMA,
                                                      Global.CONST.OSI.LED_STRIP.INVERT,
                                                      Global.CONST.OSI.LED_STRIP.BRIGHTNESS)
        self.__led_strip.begin()
        self.init_environmental_sensor()

    def init_led(self):
        """Lässt die RGB-LED in allen Farben leuchten."""
        for color_name in dir(Global.CONST.OSI.COLORS):
            if not color_name.isupper():
                continue
            color = getattr(Global.CONST.OSI.COLORS, color_name)
            self.__application.os_interface.__write_led_color(color)
            time.sleep(.3)
        self.__application.os_interface.__write_led_color(Global.CONST.OSI.COLORS.WHITE)

    def init_environmental_sensor(self):
        """Initialisiert die Verbindung zum BME280"""
        self.__application.logger.log('Start init BME280...', __name__, Logger.SEVERITY_DEBUG)
        try:
            self.env_sensor = BME280.BME280(t_mode=BME280.BME280_OSAMPLE_8, p_mode=BME280.BME280_OSAMPLE_8,
                                            h_mode=BME280.BME280_OSAMPLE_8, address=Global.CONST.OSI.BME280_ADDRESS_1)
        except Exception as e:
            self.__application.logger.log('Error occurred initialising BME280 in the first attempt: %s' % str(e),
                                          __name__, Logger.SEVERITY_WARNING)
            try:
                self.env_sensor = BME280.BME280(t_mode=BME280.BME280_OSAMPLE_8, p_mode=BME280.BME280_OSAMPLE_8,
                                                h_mode=BME280.BME280_OSAMPLE_8,
                                                address=Global.CONST.OSI.BME280_ADDRESS_2)
            except Exception as e:
                self.__application.logger.log('Error occurred initialising BME280 in the second attempt: %s' % str(e),
                                              __name__, Logger.SEVERITY_WARNING)
        self.__application.logger.log('BME280 inited', __name__, Logger.SEVERITY_DEBUG)

    def get_ir_commands(self):
        """Gibt die IR-Kommandos von lirc zurück.
        
        :return: Liste von Strings
        """
        try:
            ir_command = lirc.nextcode()
        except lirc.NextCodeError:
            self.__application.logger.log('Error occurred calling lirc nextcode', __name__, Logger.SEVERITY_ERROR)
        except lirc.InitError:
            self.__application.logger.log('InitError occurred calling lirc nextcode', __name__,
                                          Logger.SEVERITY_ERROR)
        else:
            if ir_command:
                return [ir_command[0]]
            if Global.CONST.TEST:
                temp_commands = self.__commands[:]
                self.__commands = list()
                return temp_commands
        return []

    def add_ir_command(self, command):
        """Zur Simulation von IR-Kommandos.
        
        :param command: String IR-Kommando
        """
        # Very important! This is a workaround for ir_large_input_test. For more information look for "Python GIL"
        open('TestExecutionQueue.txt', 'r')

        self.__commands.append(command)

    def __write_led_color(self, color):
        """Setzt die Farbe der RGB-LED auf Hardwareebene
        
        :param color: Liste mit drei Int-Elementen im Bereich von 0-100 oder None. Siehe auch unter Global.CONST.OSI.COLORS.
        """
        if not color:
            color = Global.CONST.OSI.COLORS.BLACK
        red = min(max(int(color[0]), 0), 100)
        green = min(max(int(color[1]), 0), 100)
        blue = min(max(int(color[2]), 0), 100)
        self.red_led.ChangeDutyCycle(red)
        self.green_led.ChangeDutyCycle(green)
        self.blue_led.ChangeDutyCycle(blue)

    def set_led(self, color):
        """Setzt die Farbe der RGB-LED dauerhaft. Wenn None übergeben wird geht die LED aus.
        Wird von set_led_time überschrieben, aber nicht gelöscht.
        
        :param color: Liste mit drei Int-Elementen im Bereich von 0-100 oder None. Siehe auch unter Global.CONST.OSI.COLORS.
        """
        self.__static_color = color

    def set_led_time(self, color, duration):
        """Setzt die Farbe der RGB-LED für eine bestimmte Zeit. Wenn None übergeben wird geht die LED aus.
        Überschreibt set_led, löschte es aber nicht.
        
        :param color: Liste mit drei Int-Elementen im Bereich von 0-100 oder None. Siehe auch unter Global.CONST.OSI.COLORS.
        :param duration: Int oder Float Leuchtdauer der RGB-LED in Sekunden.
        """
        self.__time_color = color
        self.__color_time_counter = int(duration / Global.CONST.OSI.TIME_INCREMENT_LED)

    @staticmethod
    def hour_plus():
        """Erhöht die Uhrzeit um eine Stunde."""
        time.sleep(0.5)  # workaround for lirc
        now = datetime.now()
        set_time = now + OsInterface.HOUR
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))
        os.system('sudo hwclock -w')

    @staticmethod
    def hour_minus():
        """Erniedrigt die Uhrzeit um eine Stunde."""
        time.sleep(0.5)  # workaround for lirc
        now = datetime.now()
        set_time = now - OsInterface.HOUR
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))
        os.system('sudo hwclock -w')

    @staticmethod
    def minute_plus():
        """Erhöht die Uhrzeit um eine Minute."""
        time.sleep(0.5)  # workaround for lirc
        now = datetime.now()
        set_time = now + OsInterface.MINUTE
        set_time -= timedelta(seconds=set_time.second, microseconds=set_time.microsecond)
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))
        os.system('sudo hwclock -w')

    @staticmethod
    def minute_minus():
        """Erniedrigt die Uhrzeit um eine Minute."""
        time.sleep(0.5)  # workaround for lirc
        now = datetime.now()
        set_time = now - OsInterface.MINUTE
        set_time -= timedelta(seconds=set_time.second, microseconds=set_time.microsecond)
        os.system('sudo date -s "%s" +%%T' % set_time.strftime('%H:%M:%S'))
        os.system('sudo hwclock -w')

    @staticmethod
    def set_date_time(time_str):
        """Setzt das Datum oder die Uhrzeit auf "time_str". Wenn das Datum gesetzt wird, wird die Uhrzeit nicht 
        verändert.
        
        :param time_str: String %H:%M:%S für die Uhrzeit oder %d.%m.%Y für das Datum.
        """
        time.sleep(0.5)  # workaround for lirc
        if len(time_str) == 8:  # set time
            os.system('sudo date --set %s' % time_str)
        elif len(time_str) == 10:  # set date
            splitted = time_str.split('.')
            now = datetime.now()
            os.system('sudo timedatectl set-time %s-%s-%s%s' % (splitted[2], splitted[1], splitted[0],
                                                                now.strftime('%H:%M:%S')))
        os.system('sudo hwclock -w')

    @staticmethod
    def switch_hdmi_off():
        """Schaltet den HDMI-Ausgang aus."""
        os.system('vcgencmd display_power 0')

    @staticmethod
    def switch_hdmi_on():
        """Schaltet den HDMI-Ausgang ein."""
        os.system('vcgencmd display_power 1')

    @staticmethod
    def __create_certificate():
        """Erstellt ein SSL-Zertifikat für die HTTPS-Server."""
        process = subprocess.Popen(["openssl", "req", "-x509", "-newkey", "rsa:4096",
                                    "-keyout", "/home/pi/user_data/cert/key.pem",
                                    "-out", "/home/pi/user_data/cert/cert.pem", "-days", "365"],
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   stdin=subprocess.PIPE)
        process.wait()

    @staticmethod
    def __check_certificate():
        """Prüft ob das SSL-Zertifikat noch gültig ist.
        
        :return: True, weil es noch nicht implementiert ist.
        """
        return True

    @staticmethod
    def reboot():
        """Startet das Betriebssystem neu."""
        os.system('sudo shutdown -r now')

    @staticmethod
    def shutdown():
        """Fährt das Betriebssystem herunter."""
        os.system('sudo shutdown -h now')

    @property
    def serial_number(self):
        """Die Seriennummer des Prozessors."""
        if self.__serial_number:
            return self.__serial_number
        try:
            content = open('/proc/cpuinfo', 'r').read()
        except IOError:
            return u'Fehler'
        else:
            for line in content.split('\n'):
                if line.startswith('Serial\t'):
                    serial = line.split(':')[1].strip()
                    self.__serial_number = serial
                    return serial
        return u'Fehler'

    @property
    def temperature(self):
        """Temperatur in °C"""
        try:
            return self.env_sensor.read_temperature()
        except Exception as e:
            self.__application.logger.log('Error occurred reading temperature from BME280: %s' % str(e), __name__,
                                          Logger.SEVERITY_ERROR)
        return -100

    @property
    def humidity(self):
        """Relative Luftfeuchtigkeit in %"""
        try:
            return self.env_sensor.read_humidity()
        except Exception as e:
            self.__application.logger.log('Error occurred reading humidity from BME280: %s' % str(e), __name__,
                                          Logger.SEVERITY_ERROR)
        return -100

    @property
    def pressure(self):
        """Luftdruck in hPa"""
        try:
            return self.env_sensor.read_pressure() / 100
        except Exception as e:
            self.__application.logger.log('Error occurred reading pressure from BME280: %s' % str(e), __name__,
                                          Logger.SEVERITY_ERROR)
        return -100

    @staticmethod
    def stop_service():
        """Beendet den Liedanzeige-Service"""
        os.system('sudo systemctl stop liedanzeige.service')

    @staticmethod
    def get_ip():
        """Gibt die IP der Liedanzeige zurück"""
        return os.popen('hostname -I').read().strip().split(' ')[0]

    def is_running(self):
        """Gibt zurück, ob alle Updateschleifen laufen.

        :return: Bool Wahr, wenn die Updateschleifen laufen
        """
        if self.__led_thread is None or self.__util_thread is None:
            return False
        else:
            return self.__led_thread.is_alive() and self.__util_thread.is_alive()

    def start(self):
        """Startet die Updateschleifen wenn sie noch nicht laufen."""
        if not self.is_running():
            self.__run = True
            self.__led_thread = threading.Thread(target=self.__loop_led, args=(), name='OsInterfaceLED')
            self.__led_thread.start()
            self.__util_thread = threading.Thread(target=self.__loop_util, args=(), name='OsInterfaceUtils')
            self.__util_thread.start()

    def stop(self):
        """Beendet die Updateschleifen."""
        self.__run = False
        if self.__led_thread is not None:
            self.__led_thread.join(2 * Global.CONST.OSI.TIME_INCREMENT_LED)
        if self.__util_thread is not None:
            self.__util_thread.join(2 * Global.CONST.OSI.TIME_INCREMENT_UTIL)

    def __update_led(self):
        second = datetime.now().second
        microsecond = datetime.now().microsecond

        # RGB LED
        if self.__application.state_machine.state != Global.CONST.SM.STATE_STANDBY:
            if self.__color_time_counter > 0:
                self.__color_time_counter -= 1
                self.__write_led_color(self.__time_color)
            elif self.__static_color:
                self.__write_led_color(self.__static_color)
            else:
                self.__write_led_color(Global.CONST.OSI.COLORS.BLACK)
        else:
            self.__color_time_counter = 0
            if second % 5 == 0:
                self.__write_led_color(Global.CONST.OSI.COLORS.GREEN)
            else:
                self.__write_led_color(Global.CONST.OSI.COLORS.BLACK)

        # LED Strip
        if self.__application.state_machine.state == Global.CONST.SM.STATE_NUMBER and microsecond < 500000 or \
           self.__application.state_machine.state == Global.CONST.SM.STATE_SONG:
            self.__led_strip.setPixelColor(0, Global.CONST.OSI.LED_STRIP.COLOR.WHITE)
            self.__led_strip.setPixelColor(1, Global.CONST.OSI.LED_STRIP.COLOR.WHITE)
        else:
            self.__led_strip.setPixelColor(0, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(1, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
        song = self.__application.song_handler.get_latest_song()
        if song and self.__application.state_machine.state == Global.CONST.SM.STATE_SONG and song.sopran == '1':
            self.__led_strip.setPixelColor(2, Global.CONST.OSI.LED_STRIP.COLOR.RED)
        else:
            self.__led_strip.setPixelColor(2, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
        if song and self.__application.state_machine.state == Global.CONST.SM.STATE_SONG and song.alt == '1':
            self.__led_strip.setPixelColor(3, Global.CONST.OSI.LED_STRIP.COLOR.YELLOW)
        else:
            self.__led_strip.setPixelColor(3, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
        if song and self.__application.state_machine.state == Global.CONST.SM.STATE_SONG and song.tenor == '1':
            self.__led_strip.setPixelColor(4, Global.CONST.OSI.LED_STRIP.COLOR.GREEN)
        else:
            self.__led_strip.setPixelColor(4, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
        if song and self.__application.state_machine.state == Global.CONST.SM.STATE_SONG and song.bass == '1':
            self.__led_strip.setPixelColor(5, Global.CONST.OSI.LED_STRIP.COLOR.BLUE)
        else:
            self.__led_strip.setPixelColor(5, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
        self.__led_strip.setPixelColor(6, Global.CONST.OSI.LED_STRIP.COLOR.WHITE)
        if self.__application.display_server.is_running():
            self.__led_strip.setPixelColor(7, Global.CONST.OSI.LED_STRIP.COLOR.WHITE)
        else:
            self.__led_strip.setPixelColor(7, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
        if self.__application.state_machine.state == Global.CONST.SM.STATE_QUIT:
            self.__led_strip.setPixelColor(0, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(1, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(2, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(3, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(4, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(5, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(6, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)
            self.__led_strip.setPixelColor(7, Global.CONST.OSI.LED_STRIP.COLOR.BLACK)

        self.__led_strip.show()

    def __update_util(self):
        if not self.__check_certificate():
            self.__create_certificate()

    @DiagnosticModule.execute_with_exception_handling
    def __loop_led(self):
        while self.__run:
            self.__update_led()
            time.sleep(Global.CONST.OSI.TIME_INCREMENT_LED)
        try:
            lirc.deinit()
        except lirc.DeinitError:
            self.__application.logger.log('DeinitError occurred deinitialising lirc', __name__,
                                          Logger.SEVERITY_ERROR)
        GPIO.cleanup()

    @DiagnosticModule.execute_with_exception_handling
    def __loop_util(self):
        while self.__run:
            self.__update_util()
            time.sleep(Global.CONST.OSI.TIME_INCREMENT_UTIL)
