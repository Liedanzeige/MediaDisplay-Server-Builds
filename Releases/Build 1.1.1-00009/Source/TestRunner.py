# -*- coding: utf-8 -*-
"""Stellt Klassen zum Test der Applikation zur Verfügung"""

import time
from datetime import datetime
import codecs
import threading
import os
from random import randint
import socket
import urllib2
import json
import shutil

import Global
import UpdateHandler
import SongHandler

CurrDir = os.path.abspath(os.path.dirname(__file__))


class TestRunner(object):
    """Dient zum Test der Applikation."""

    def __init__(self, application):
        self.__application = application
        self.thread = threading.Thread(target=self.run_tests, name="TestRunner")
        self.test_result = None

    def check_expected_gui_screen(self, expected_screen):
        """Prüft ob die GUI die erwartete Seite anzeigt und loggt das Ergebnis.
        
        :param expected_screen: Int, Index der erwarteten Seite.
        :return: 
        """
        screen = self.__application.main_window.current
        if screen == expected_screen:
            self.test_result.pass_('Screen "%s" is correct' % screen)
        else:
            self.test_result.fail('Screen should be "%s" but was "%s"' %
                                  (expected_screen, screen))

    def check_expected_label_text(self, label_id, expected_text):
        """Prüft ob ein Label den erwarteten Text enthält und loggt das Ergebnis.
        
        :param label_id: String, ID des zu prüfenden Labels.
        :param expected_text: String, der erwartete Text des Labels.
        """
        label = self.__application.main_window.ids[label_id]
        label_text = label.text()
        if label_text == expected_text:
            self.test_result.pass_(u'Labels "%s" text is correct' % label_id)
        else:
            self.test_result.fail(u'Labels "%s" text should be "%s" but was "%s"' %
                                  (label_id, expected_text, label_text))

    def check_url(self, url, expected_answer):
        """Prüft ob eine gegebene URL die erwartete Antwort gibt und loggt das Ergebnis.
        
        :param url: String, URL die abgefragt wird.
        :param expected_answer:  String, die erwartete Antwort.
        """
        request = urllib2.urlopen(url, timeout=1.0)
        answer = request.read()
        if answer == expected_answer:
            self.test_result.pass_(u'URL "%s" answer is correct' % url)
        else:
            self.test_result.fail(u'URL "%s" answer should be "%s" but was "%s"' %
                                  (url, expected_answer, answer))

    @staticmethod
    def get_url(url):
        """Gibt die Serverantwort zurück

        :param url: String, URL die abgefragt wird.
        """
        request = urllib2.urlopen(url, timeout=1.0)
        return request.read()

    def assert_true(self, cond, message=u''):
        """Prüft ob die Bedingung wahr ist und loggt das Ergebnis.
        
        :param cond: Bedingung die geprüft wird. Muss nach Bool konvertierbar sein.
        :param message: Unicode, Meldung die geloggt wird.
        """
        if cond:
            self.test_result.pass_(u'Assert passed ' + message)
        else:
            self.test_result.fail(u'Assert failed ' + message)

    def assert_equal(self, expected, actual, message=u''):
        """Prüft ob expected und actual gleich sind und loggt das Ergebnis.

        :param expected: Der erwartete Wert.
        :param actual: Der wirkliche Wert.
        :param message: Unicode, Meldung die geloggt wird.
        """
        if expected == actual:
            self.test_result.pass_(u'Assert passed')
        else:
            if message:
                self.test_result.fail(u'Assert failed ' + message)
            else:
                self.test_result.fail(u'Assert failed. Expected: {0}, Actual: {1}'.format(expected, actual))

    def send_ir_command(self, ir_command):
        """Simuliert ein IR-Kommando.
        
        :param ir_command: Sting, IR-Kommando das simuliert wird.
        """
        self.__application.os_interface.add_ir_command(ir_command)
        self.test_result.message(u'IR Command : %s' % ir_command)

    def send_nw_command(self, action, action_data=None):
        """Simuliert ein Netzwerk-Kommando.

        :param action: Int, Aktion, die ausgeführt werden soll.
        :param action_data: Sting, IR-Kommando das simuliert wird.
        """
        self.__application.state_machine.add_network_command(action, action_data)
        if action_data is None:
            self.test_result.message(u'NW Command : {0}'.format(action))
        else:
            self.test_result.message(u'NW Command : {0} - {1}'.format(action, action_data))

    def run_tests(self):
        """Führt alle Tests aus der Datei "TestExecutionQueue.txt" aus."""
        if os.path.isfile('TestExecutionQueue.txt'):
            self.test_result = TestResult()
            self.test_result.message(u'Commit: %s' % get_commit())
            self.test_result.message(u'Maybe not everything is committed and pushed')
            with open('TestExecutionQueue.txt', 'r') as test_queue_file:
                test_queue = test_queue_file.readlines()
            if test_queue:
                if os.path.exists(UpdateHandler.UPDATE_PACKAGE_HOME_PATH):
                    os.remove(UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
                if os.path.exists(UpdateHandler.UPDATE_DIR):
                    shutil.rmtree(UpdateHandler.UPDATE_DIR)
                test = test_queue[0]
                test_func_str = test.strip()
                if hasattr(self, test_func_str):
                    test_func = getattr(self, test_func_str)
                    try:
                        test_func()
                    except Exception as e:
                        self.test_result.fail(u'Test Failed with message: %s' % unicode(e))
                        self.test_result.finish_test()
                        self.__application.quit()
                    else:
                        self.test_result.finish_test()
            self.stop()

    def test_restart_server(self):
        """Testet ob der CommandServer und der DisplayServer automatisch starten, nachdem sie abgestürzt sind."""
        self.test_result.start_test('Restart Server')

        time.sleep(180)

        self.assert_true(self.__application.display_server.is_running(), u'Display Server is running')
        self.__application.display_server.stop()
        self.assert_true(not self.__application.display_server.is_running(), u'Display Server not is running')
        time.sleep(6)
        self.assert_true(self.__application.display_server.is_running(), u'Display Server is running')
        time.sleep(1)

    def test_ir_large_input(self):
        """Testet das Verhalten wenn viele IR-Eingaben bei der Nummereingabe kommen."""
        self.test_result.start_test('IR Large Input Test')

        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        # test song input
        song = ''
        book = ''
        third = ''
        song_list = [str(randint(0, 9)) for _ in range(100)]
        book_list = [str(randint(0, 9)) for _ in range(100)]
        third_list = [str(randint(0, 9)) for _ in range(100)]

        for i in book_list:
            book += i
            self.send_ir_command(i)
            time.sleep(0.1)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in song_list:
            song += i
            self.send_ir_command(i)
            time.sleep(0.1)
        self.send_ir_command('+')
        time.sleep(0.5)
        for i in third_list:
            third += i
            self.send_ir_command(i)
            time.sleep(0.1)

        self.check_expected_label_text('Number', '<span style="color:#ffff00;">' + book + '</span> ' + song + ' ' +
                                       third)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '<span style="color:#ffff00;">X</span> ' + song)

        time.sleep(30)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_ir_commands(self):
        """Testet weite Teile der StateMachine."""
        # wait until the program has started
        self.test_result.start_test('FSM IR Commands Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # set settings for faster progress
        self.__application.general_settings.time_1_5 = 2
        self.__application.general_settings.time_2_1 = 15
        self.__application.general_settings.time_3_2 = 5
        self.__application.general_settings.general_song_books = ['3']

        # test back button without song
        self.send_ir_command('left')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(0.5)

        # test song input
        self.send_ir_command('0')
        time.sleep(0.5)
        self.send_ir_command('0')
        time.sleep(0.5)
        self.send_ir_command('4')
        time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(0.5)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.send_ir_command('3')
        time.sleep(0.5)
        self.send_ir_command('+')
        time.sleep(0.5)
        self.send_ir_command('3')
        time.sleep(0.5)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;">004</span> 123 32')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(7)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '<span style="color:#ffff00;">04</span> 123')
        self.check_expected_label_text('Sopran', '')
        self.check_expected_label_text('Alt', 'Alt')
        self.check_expected_label_text('Tenor', 'Tenor')
        self.check_expected_label_text('Bass', '')

        self.assert_true(not self.__application.song_handler.get_latest_song().general_song, 'Check general song')

        # test change solos
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', 'Sopran')
        self.check_expected_label_text('Alt', 'Alt')
        self.check_expected_label_text('Tenor', 'Tenor')
        self.check_expected_label_text('Bass', '')

        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', 'Sopran')
        self.check_expected_label_text('Alt', '')
        self.check_expected_label_text('Tenor', 'Tenor')
        self.check_expected_label_text('Bass', '')

        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', 'Sopran')
        self.check_expected_label_text('Alt', '')
        self.check_expected_label_text('Tenor', '')
        self.check_expected_label_text('Bass', '')

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', '')
        self.check_expected_label_text('Alt', '')
        self.check_expected_label_text('Tenor', '')
        self.check_expected_label_text('Bass', '')

        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', '')
        self.check_expected_label_text('Alt', '')
        self.check_expected_label_text('Tenor', '')
        self.check_expected_label_text('Bass', 'Bass')

        self.send_ir_command('4')
        time.sleep(0.5)
        self.check_expected_label_text('Sopran', '')
        self.check_expected_label_text('Alt', '')
        self.check_expected_label_text('Tenor', '')
        self.check_expected_label_text('Bass', '')

        time.sleep(30)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        # test back button
        self.send_ir_command('left')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(10)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('3')
        self.send_ir_command('+')
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(4)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.assert_true(self.__application.song_handler.get_latest_song().general_song, 'Check general song')
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        # test button left on state number
        self.send_ir_command('3')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('left')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        # test button exit on state number
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('right')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('ok')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(10)

        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(119)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test '+' to show song
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '1')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test invalid song
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '0')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span> ')
        self.send_ir_command('+')
        self.send_ir_command('7')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span>  7')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('3')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '3')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span> ')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span>  ')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('4')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '4')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span> ')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span>  ')
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('5')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '5')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span> ')
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span>  ')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(4)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test interpret number as song number
        self.send_ir_command('6')
        time.sleep(0.1)
        self.check_expected_label_text('Number', '6')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '6')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('7')
        time.sleep(0.1)
        self.check_expected_label_text('Number', '7')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_label_text('Number', '')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('+')
        time.sleep(0.1)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span> ')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('8')
        time.sleep(0.1)
        self.check_expected_label_text('Number', '<span style="color:#ffff00;"></span> 8')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '8')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test standby
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_NUMBER)
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(14)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(118)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(0.5)
        self.send_ir_command('1')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(0.5)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(35)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # restore default settings
        self.__application.general_settings.load_defaults()

        # test quit
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        time.sleep(10)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)

    def test_fsm_net_commands(self):
        """Testet die StateMachine mit Network-Commands"""
        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # set settings for faster progress
        self.__application.general_settings.time_1_5 = 2
        self.__application.general_settings.time_2_1 = 15
        self.__application.general_settings.time_3_2 = 5
        self.__application.general_settings.general_song_books = ['3']

        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG_AGAIN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        song = SongHandler.Song()
        song.book_abbreviation = '04'
        song.number = '123'
        song.alt = True
        song.tenor = True
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        time.sleep(10)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG_AGAIN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        time.sleep(10)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"This should not be <b>bold</b>", 10))
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE,
                             (r"{\rtf1\ansi\deff0 "
                              r"{\colortbl;\red0\green0\blue0;\red255\green0\blue0;} "
                              r"This line is the default color\line "
                              r"\cf2 "
                              r"This line is red\line "
                              r"\cf1 "
                              r"This line is the default color "
                              r"}", None))
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        time.sleep(59)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        time.sleep(1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        time.sleep(1)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG_AGAIN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_expected_label_text('SongNumber', '<span style="color:#ffff00;">04</span> 123')
        song = SongHandler.Song()
        song.book_abbreviation = '5'
        song.number = '43'
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.1)
        self.check_expected_label_text('SongNumber', '<span style="color:#ffff00;">5</span> 43')
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        time.sleep(0.1)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG_AGAIN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state network address
        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(0.1)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG_AGAIN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state show message
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG_AGAIN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.check_expected_label_text('Message', u'My Text')
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"My other Text", 120))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)
        self.check_expected_label_text('Message', u'My other Text')

        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # go to state clock
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # restore default settings
        self.__application.general_settings.load_defaults()

        self.send_nw_command(Global.CONST.SM.ACTION_SHUTDOWN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)

    def test_fsm_nw_command_shutdown_from_state_idle(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Shutdown From State Idle')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHUTDOWN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_nw_command_reboot_from_state_idle(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Reboot From State Idle')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_REBOOT)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')

    def test_fsm_nw_command_shutdown_from_state_song(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Shutdown From State Song')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        song = SongHandler.Song()
        song.book_abbreviation = '04'
        song.number = '123'
        song.alt = True
        song.tenor = True
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_SHUTDOWN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_nw_command_reboot_from_state_song(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Reboot From State Song')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        song = SongHandler.Song()
        song.book_abbreviation = '04'
        song.number = '123'
        song.alt = True
        song.tenor = True
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_SONG, song)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)

        self.send_nw_command(Global.CONST.SM.ACTION_REBOOT)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')

    def test_fsm_nw_command_shutdown_from_state_standby(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Shutdown From State Standby')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)

        self.send_nw_command(Global.CONST.SM.ACTION_SHUTDOWN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_nw_command_reboot_from_state_standby(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Reboot From State Standby')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_STANDBY)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)

        self.send_nw_command(Global.CONST.SM.ACTION_REBOOT)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')

    def test_fsm_nw_command_shutdown_from_state_show_ip(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Shutdown From State Show IP')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)

        self.send_nw_command(Global.CONST.SM.ACTION_SHUTDOWN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_nw_command_reboot_from_state_show_ip(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Reboot From State Show IP')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)

        self.send_nw_command(Global.CONST.SM.ACTION_REBOOT)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')

    def test_fsm_nw_command_shutdown_from_state_show_message(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Shutdown From State Show Message')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u'My Message', None))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHUTDOWN)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_nw_command_reboot_from_state_show_message(self):

        # wait until the program has started
        self.test_result.start_test('FSM Network Commands Reboot From State Show Message')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u'My Message', None))
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_DISPLAY_MESSAGE)

        self.send_nw_command(Global.CONST.SM.ACTION_REBOOT)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')

    def test_marquee_label(self):
        """Testet das Marquee-Label."""
        # wait until the program has started
        self.test_result.start_test('Marquee-Label Test')
        time.sleep(1)

        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"Hello", 20))
        time.sleep(19)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"Hello new world!", 20))
        time.sleep(19)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_MESSAGE, (u"asdf", 20))
        time.sleep(19)
        self.send_nw_command(Global.CONST.SM.ACTION_SHOW_CLOCK)
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)

    def test_fsm_shutdown(self):
        """Prüft ob das Programm das Betriebssystem herunterfährt."""
        # wait until the program has started
        self.test_result.start_test('FSM Shutdown Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_reboot(self):
        """Prüft ob das Programm das Betriebssystem neu startet."""
        # wait until the program has started
        self.test_result.start_test('FSM Reboot Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('3')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.REBOOT, u'Shutdown Mode')

    def test_fsm_shutdown_ok(self):
        """Prüft ob das Programm das Betriebssystem mit dem "OK"-Kommando herunterfährt."""
        # wait until the program has started
        self.test_result.start_test('FSM Shutdown OK Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_ir_settings(self):
        """Testet den Einstellungsbereich der StateMachine."""
        # wait until the program has started
        self.test_result.start_test('FSM IR Settings Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.__application.general_settings.load_defaults()

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        time.sleep(5)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(0.1)
        self.send_ir_command('1')
        time.sleep(0.1)
        self.send_ir_command('2')
        time.sleep(0.1)
        self.send_ir_command('3')
        time.sleep(0.1)
        self.send_ir_command('4')
        time.sleep(0.1)
        self.send_ir_command('5')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime', '12:34:5-')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime', '12:34:--')
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        time.sleep(0.1)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(0.1)
        self.send_ir_command('6')
        time.sleep(0.1)
        self.send_ir_command('7')
        time.sleep(0.1)
        self.send_ir_command('8')
        time.sleep(0.1)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDate', '67.89.0---')
        self.send_ir_command('back')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDate', '67.89.----')
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # check all left / right
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set clock idle
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.assert_true(self.__application.general_settings.clock_idle == '1', 'Check setting clock idle')
        self.check_expected_label_text('LabelSetClockIdle', '1')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockIdle', '2')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check setting clock idle')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockIdle', '1')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.clock_idle == '1', 'Check setting clock idle')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set clock song
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.assert_true(self.__application.general_settings.clock_song == '1', 'Check setting clock song')
        self.check_expected_label_text('LabelSetClockSong', '1')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockSong', '2')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.clock_song == '2', 'Check setting clock song')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetClockSong', '1')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.clock_song == '1', 'Check setting clock song')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set order solo
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.assert_true(self.__application.general_settings.order_solo == '1', 'Check setting order solo')
        self.check_expected_label_text('LabelSetOrderSolo', '1')
        self.send_ir_command('3')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetOrderSolo', '3')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.order_solo == '3', 'Check setting order solo')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetOrderSolo', '1')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.order_solo == '1', 'Check setting order solo')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set time 3 2
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check setting time 3 2')
        self.check_expected_label_text('LabelSetTime_3_2', '5')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_3_2', '10')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_3_2 == 10.0, 'Check setting time 3 2')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.send_ir_command('9')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_3_2', '29')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_3_2 == 29.0, 'Check setting time 3 2')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set time 2 1
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.assert_true(self.__application.general_settings.time_2_1 == 60.0, 'Check setting time 2 1')
        self.check_expected_label_text('LabelSetTime_2_1', '60')
        self.send_ir_command('3')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_2_1', '30')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_2_1 == 30.0, 'Check setting time 2 1')
        self.send_ir_command('4')
        time.sleep(0.1)
        self.send_ir_command('5')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_2_1', '45')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_2_1 == 45.0, 'Check setting time 2 1')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set time 1 5
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.assert_true(self.__application.general_settings.time_1_5 == 60.0, 'Check setting time 1 5')
        self.check_expected_label_text('LabelSetTime_1_5', '60')
        self.send_ir_command('7')
        time.sleep(0.1)
        self.send_ir_command('8')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_1_5', '78')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_1_5 == 78.0, 'Check setting time 1 5')
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_1_5', u'\u221E')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_1_5 == 0.0, 'Check setting time 1 5')
        self.send_ir_command('9')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetTime_1_5', '90')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.time_1_5 == 90.0, 'Check setting time 1 5')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set display server
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.assert_true(self.__application.general_settings.display_server == '2', 'Check setting display server')
        self.assert_true(not self.__application.display_server.is_running(), 'Check display server running')
        self.check_expected_label_text('LabelSetDisplayServer', '2')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDisplayServer', '1')
        self.assert_true(self.__application.general_settings.display_server == '2', 'Check setting display server')
        self.assert_true(not self.__application.display_server.is_running(), 'Check display server running')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check setting display server')
        time.sleep(5)
        self.assert_true(self.__application.display_server.is_running(), 'Check display server running')
        self.send_ir_command('2')
        time.sleep(0.1)
        self.send_ir_command('0')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetDisplayServer', '2')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.display_server == '2', 'Check setting display server')
        time.sleep(5)
        self.assert_true(not self.__application.display_server.is_running(), 'Check display server running')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state set allg lieder
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check setting general song books')
        self.check_expected_label_text('LabelSetGeneralSongBooks', '')
        self.send_ir_command('3')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetGeneralSongBooks', '3')
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check setting general song books')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.assert_true(self.__application.general_settings.general_song_books == ['3'],
                         'Check setting general song books')
        self.send_ir_command('+')
        self.send_ir_command('0')
        self.send_ir_command('0')
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetGeneralSongBooks', '3 001')
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetGeneralSongBooks', '3 01')
        self.assert_true(self.__application.general_settings.general_song_books == ['3', '01'],
                         'Check setting general song books')
        self.send_ir_command('+')
        self.send_ir_command('7')
        time.sleep(0.1)
        self.check_expected_label_text('LabelSetGeneralSongBooks', '3 01 7')
        self.send_ir_command('left')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        self.check_expected_label_text('LabelSetGeneralSongBooks', '3 01')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # state show system information
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('menu')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DATE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_CLOCK_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_ORDER_SOLO)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_NUMBER_SONG)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_SONG_IDLE)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_TIME_IDLE_STANDBY)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_DISPLAY_SERVER)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SET_GENERAL_SONG_BOOKS)
        self.send_ir_command('right')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        time.sleep(29)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SYSTEM_INFORMATION)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.__application.general_settings.load_defaults()

        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_general_settings(self):
        """Testet den GeneralSettingsHandler."""
        # wait until the program has started
        self.test_result.start_test('General Settings Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.__application.general_settings.load_defaults()

        # check default settings
        self.test_result.message('Check default settings')
        self.assert_true(self.__application.general_settings.time_1_5 == 60.0, 'Check default time_1_5')
        self.assert_true(self.__application.general_settings.time_2_1 == 60.0, 'Check default time_2_1')
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check default time_3_2')
        self.assert_true(self.__application.general_settings.clock_idle == '1', 'Check default clock_idle')
        self.assert_true(self.__application.general_settings.clock_song == '1', 'Check default clock_song')
        self.assert_true(self.__application.general_settings.display_server == '2', 'Check default display_server')
        self.assert_true(self.__application.general_settings.order_solo == '1', 'Check default order_solo')
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check default general_song_books')

        # check missing file
        self.test_result.message('Check missing file')
        os.remove(Global.CONST.STH.GENERAL.FILEPATH)
        time.sleep(2)
        self.assert_true(self.__application.general_settings.time_1_5 == 60.0, 'Check default time_1_5')
        self.assert_true(self.__application.general_settings.time_2_1 == 60.0, 'Check default time_2_1')
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check default time_3_2')
        self.assert_true(self.__application.general_settings.clock_idle == '1', 'Check default clock_idle')
        self.assert_true(self.__application.general_settings.clock_song == '1', 'Check default clock_song')
        self.assert_true(self.__application.general_settings.display_server == '2', 'Check default display_server')
        self.assert_true(self.__application.general_settings.order_solo == '1', 'Check default order_solo')
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check default general_song_books')

        # check modify file
        self.test_result.message('Check modify file')
        time.sleep(2)
        codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8').write('{"clock_idle": "2", "time_2_1": 62.0, '
                                                                           '"clock_song": "2", "time_3_2": 6.0, '
                                                                           '"time_1_5": 61.0, "display_server": "1",'
                                                                           '"order_solo": "3", '
                                                                           '"general_song_books": ["29", "5"]}')
        time.sleep(2)
        self.assert_true(self.__application.general_settings.time_1_5 == 61.0, 'Check settings time_1_5')
        self.assert_true(self.__application.general_settings.time_2_1 == 62.0, 'Check settings time_2_1')
        self.assert_true(self.__application.general_settings.time_3_2 == 6.0, 'Check settings time_3_2')
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.assert_true(self.__application.general_settings.clock_song == '2', 'Check settings clock_song')
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check settings display_server')
        self.assert_true(self.__application.general_settings.order_solo == '3', 'Check settings order_solo')
        self.assert_true(self.__application.general_settings.general_song_books == ["29", "5"],
                         'Check settings general_song_books')

        # check modify corrupt file
        self.test_result.message('Check modify corrupt file')
        time.sleep(2)
        codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8').write('{"clock_idle": "4", "time_2_1": 1000.0, '
                                                                           '"clock_song": "3", "time_3_2": 2.0, '
                                                                           '"time_1_5": 1.0, "display_server": "5",'
                                                                           '"order_solo": "7", '
                                                                           '"general_song_books": ["-5", "007"]}')
        time.sleep(2)
        self.assert_true(self.__application.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        self.assert_true(self.__application.general_settings.time_2_1 == 900.0, 'Check settings time_2_1')
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.assert_true(self.__application.general_settings.clock_song == '2', 'Check settings clock_song')
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check settings display_server')
        self.assert_true(self.__application.general_settings.order_solo == '3', 'Check settings order_solo')
        self.assert_true(self.__application.general_settings.general_song_books == ["07"],
                         'Check settings general_song_books')
        # to write to file
        self.__application.general_settings.clock_song = '1'

        # check modify defect file
        self.test_result.message('Check modify defect file')
        temp_file = codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8')
        temp_file.write('{"clock_idle": "2000", "time_2_1": 60.0, "clock_song": "1", "time_3_2": 5.0, "time_1_5": 60.0')
        temp_file.close()
        self.assert_true(self.__application.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        self.assert_true(self.__application.general_settings.time_2_1 == 900.0, 'Check settings time_2_1')
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.assert_true(self.__application.general_settings.clock_song == '1', 'Check settings clock_song')
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check settings display_server')
        self.assert_true(self.__application.general_settings.order_solo == '3', 'Check settings order_solo')
        self.assert_true(self.__application.general_settings.general_song_books == ["07"],
                         'Check settings general_song_books')

        # check modify empty file
        self.test_result.message('Check empty corrupt file')
        self.__application.general_settings.load_defaults()
        time.sleep(2)
        codecs.open(Global.CONST.STH.GENERAL.FILEPATH, 'w', 'utf-8').write('')
        time.sleep(2)
        self.assert_true(self.__application.general_settings.time_1_5 == 60.0, 'Check settings time_1_5')
        self.assert_true(self.__application.general_settings.time_2_1 == 60.0, 'Check settings time_2_1')
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.assert_true(self.__application.general_settings.clock_idle == '1', 'Check settings clock_idle')
        self.assert_true(self.__application.general_settings.clock_song == '1', 'Check settings clock_song')
        self.assert_true(self.__application.general_settings.display_server == '2', 'Check settings display_server')
        self.assert_true(self.__application.general_settings.order_solo == '1', 'Check settings order_solo')
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check settings general_song_books')
        self.__application.general_settings.load_defaults()

        # check set settings
        self.test_result.message('Check set settings')
        self.__application.general_settings.time_1_5 = '64'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_1_5 == 64.0, 'Check settings time_1_5')
        self.__application.general_settings.time_2_1 = '65'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_2_1 == 65.0, 'Check settings time_2_1')
        self.__application.general_settings.time_3_2 = '10'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_3_2 == 10.0, 'Check settings time_3_2')
        self.__application.general_settings.clock_idle = '2'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.__application.general_settings.clock_song = '2'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.clock_song == '2', 'Check settings clock_song')
        self.__application.general_settings.display_server = '1'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check settings display_server')
        self.__application.general_settings.order_solo = '2'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.order_solo == '2', 'Check settings order_solo')
        self.__application.general_settings.general_song_books = ['9']
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == ['9'],
                         'Check settings general_song_books')
        self.__application.general_settings.general_song_books = ['1', '5', '3']
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == ['1', '5', '3'],
                         'Check settings general_song_books')
        self.__application.general_settings.general_song_books = '9 '
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == ['9'],
                         'Check settings general_song_books')
        self.__application.general_settings.general_song_books = ' 1 5 3'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == ['1', '5', '3'],
                         'Check settings general_song_books')

        # check min limits
        self.test_result.message('Check min limits')
        self.__application.general_settings.time_1_5 = '1'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        self.__application.general_settings.time_1_5 = '-60'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_1_5 == 2.0, 'Check settings time_1_5')
        self.__application.general_settings.time_2_1 = '1'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_2_1 == 15.0, 'Check settings time_2_1')
        self.__application.general_settings.time_2_1 = '-20'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_2_1 == 15.0, 'Check settings time_2_1')
        self.__application.general_settings.time_3_2 = '1'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')
        self.__application.general_settings.time_3_2 = '-10'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_3_2 == 5.0, 'Check settings time_3_2')

        # check max limits
        self.test_result.message('Check max limits')
        self.__application.general_settings.time_1_5 = '10000'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_1_5 == 180.0, 'Check settings time_1_5')
        self.__application.general_settings.time_2_1 = '20000'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_2_1 == 900.0, 'Check settings time_2_1')
        self.__application.general_settings.time_3_2 = '30000'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_3_2 == 60.0, 'Check settings time_3_2')

        # check possible values
        self.test_result.message('Check possible values')
        self.__application.general_settings.clock_idle = '4'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.__application.general_settings.clock_idle = 1
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.clock_idle == '2', 'Check settings clock_idle')
        self.__application.general_settings.clock_song = '3'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.clock_song == '2', 'Check settings clock_song')
        self.__application.general_settings.clock_song = 1
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.clock_song == '2', 'Check settings clock_song')
        self.__application.general_settings.display_server = '3'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check settings display_server')
        self.__application.general_settings.display_server = 1
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.display_server == '1', 'Check settings display_server')
        self.__application.general_settings.order_solo = '0'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.order_solo == '2', 'Check settings order_solo')
        self.__application.general_settings.order_solo = 1
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.order_solo == '2', 'Check settings order_solo')
        self.__application.general_settings.general_song_books = '00562'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check settings general_song_books')
        self.__application.general_settings.general_song_books = '  0'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == ["0"],
                         'Check settings general_song_books')
        self.__application.general_settings.general_song_books = '  -5'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == [],
                         'Check settings general_song_books')
        self.__application.general_settings.general_song_books = '4'
        self.__application.general_settings.general_song_books = 25
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.general_song_books == ["4"],
                         'Check settings general_song_books')
        self.__application.general_settings.time_1_5 = '0'
        time.sleep(0.01)
        self.assert_true(self.__application.general_settings.time_1_5 == 0.0, 'Check settings time_1_5')

        # restore default settings
        self.__application.general_settings.load_defaults()

        # exit application
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.send_ir_command('power')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def test_fsm_show_ip(self):
        """Prüft ob die Seite mit der Netzwerkadresse richtig angezeigt wird."""
        # wait until the program has started
        self.test_result.start_test('FSM Show IP Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.__application.general_settings.display_server = Global.CONST.STH.GENERAL.DISPLAY_SERVER_OFF
        time.sleep(5)

        self.send_ir_command('guide')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.check_expected_label_text('LabelNetworkAddress', u'DisplayServer einschalten!')
        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.__application.general_settings.display_server = Global.CONST.STH.GENERAL.DISPLAY_SERVER_ON
        time.sleep(5)

        self.send_ir_command('guide')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        if Global.CONST.ON_RPI:
            self.check_expected_label_text('LabelNetworkAddress', u'http://' + os.popen('hostname -I').read().strip()
                                           + u':48080')
        else:
            self.check_expected_label_text('LabelNetworkAddress', u'http://' + socket.gethostbyname(socket.getfqdn())
                                           + u':48080')
        time.sleep(59)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        time.sleep(2)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.__application.general_settings.display_server = Global.CONST.STH.GENERAL.DISPLAY_SERVER_OFF
        time.sleep(5)

        qr_code_dir = os.path.join(Global.CONST.USER_DATA_DIR, 'QrCodes')
        for file_name in os.listdir(qr_code_dir)[:]:
            if file_name.endswith('.png'):
                os.remove(os.path.join(qr_code_dir, file_name))

        self.send_ir_command('guide')
        time.sleep(0.01)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        time.sleep(4.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)

        self.send_ir_command('exit')
        time.sleep(0.1)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')

    def display_server_test(self):
        """Prüft ob der DisplayServer korrekt funktioniert."""
        # wait until the program has started
        self.test_result.start_test('DisplayServer Test')
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.__application.general_settings.load_defaults()
        time.sleep(5)

        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.assert_true(not self.__application.display_server.is_running(), 'Check DisplayServer running')
        self.__application.general_settings.display_server = '1'
        time.sleep(10)

        self.assert_true(self.__application.display_server.is_running(), 'Check DisplayServer running')
        self.check_url('http://localhost:48080/erstetelefonnachricht', 'daspferdfrisstkeinengurkensalat')
        self.check_url('http://localhost:48080/version.json', '"{}"'.format(Global.CONST.DS.API_VERSION))
        self.check_url('http://localhost:48080/applicationversion.json', '"{}"'.format(Global.CONST.VERSION))
        self.check_url('http://localhost:48080/applicationbuildnumber.json', '"{}"'.format(Global.CONST.BUILD))
        self.check_url('http://localhost:48080/sungsongs.json', '[]')

        # check weather
        weather_text = self.get_url('http://localhost:48080/weather.json')
        weather_json = json.loads(weather_text)
        self.assert_true((15.0 < weather_json['temp']) and (weather_json['temp'] < 40.0), 'Check Weather Temperature')
        self.assert_true((950.0 < weather_json['pressure']) and (weather_json['pressure'] < 1100),
                         'Check Weather Pressure')
        self.assert_true((10.0 < weather_json['humidity']) and (weather_json['humidity'] < 50.0),
                         'Check Weather Humidity')
        time_delta = datetime.now() - datetime.strptime(weather_json['date'], '%Y-%m-%d %H:%M:%S.%f')
        self.assert_true(time_delta.total_seconds() < 1, 'Check Weather Date')

        self.check_url('http://localhost:48080/state.json', '{"state": "Zeige Uhr", "data": null, "state_number": 1}')

        # check state
        self.send_ir_command('4')
        self.send_ir_command('+')
        self.send_ir_command('5')
        self.check_url('http://localhost:48080/state.json', '{"state": "Zeige Zahl", "data": "4 5", "state_number": 3}')
        self.send_ir_command('9')
        time.sleep(6)
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/state.json', timeout=1.0).read())
        self.assert_equal("Zeige Lied", json_answer['state'])
        self.assert_equal(False, json_answer['data']['general_song'])
        self.assert_equal("4", json_answer['data']['book_abbreviation'])
        self.assert_equal("4", json_answer['data']['book_id'])
        self.assert_equal("#ffff00", json_answer['data']['book_color'])
        self.assert_equal("59", json_answer['data']['number'])
        self.assert_equal("", json_answer['data']['title'])
        self.assert_equal(False, json_answer['data']['sopran'])
        self.assert_equal("Sopran", json_answer['data']['sopran_name'])
        self.assert_equal(False, json_answer['data']['alt'])
        self.assert_equal("Alt", json_answer['data']['alt_name'])
        self.assert_equal(False, json_answer['data']['tenor'])
        self.assert_equal("Tenor", json_answer['data']['tenor_name'])
        self.assert_equal(False, json_answer['data']['bass'])
        self.assert_equal("Bass", json_answer['data']['bass_name'])
        self.assert_equal(2, json_answer['state_number'])
        self.send_ir_command('1')
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/state.json', timeout=1.0).read())
        self.assert_equal("Zeige Lied", json_answer['state'])
        self.assert_equal(False, json_answer['data']['general_song'])
        self.assert_equal("4", json_answer['data']['book_abbreviation'])
        self.assert_equal("4", json_answer['data']['book_id'])
        self.assert_equal("#ffff00", json_answer['data']['book_color'])
        self.assert_equal("59", json_answer['data']['number'])
        self.assert_equal("", json_answer['data']['title'])
        self.assert_equal(True, json_answer['data']['sopran'])
        self.assert_equal("Sopran", json_answer['data']['sopran_name'])
        self.assert_equal(False, json_answer['data']['alt'])
        self.assert_equal("Alt", json_answer['data']['alt_name'])
        self.assert_equal(False, json_answer['data']['tenor'])
        self.assert_equal("Tenor", json_answer['data']['tenor_name'])
        self.assert_equal(False, json_answer['data']['bass'])
        self.assert_equal("Bass", json_answer['data']['bass_name'])
        self.assert_equal(2, json_answer['state_number'])
        self.send_ir_command('2')
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/state.json', timeout=1.0).read())
        self.assert_equal("Zeige Lied", json_answer['state'])
        self.assert_equal(False, json_answer['data']['general_song'])
        self.assert_equal("4", json_answer['data']['book_abbreviation'])
        self.assert_equal("4", json_answer['data']['book_id'])
        self.assert_equal("#ffff00", json_answer['data']['book_color'])
        self.assert_equal("59", json_answer['data']['number'])
        self.assert_equal("", json_answer['data']['title'])
        self.assert_equal(True, json_answer['data']['sopran'])
        self.assert_equal("Sopran", json_answer['data']['sopran_name'])
        self.assert_equal(True, json_answer['data']['alt'])
        self.assert_equal("Alt", json_answer['data']['alt_name'])
        self.assert_equal(False, json_answer['data']['tenor'])
        self.assert_equal("Tenor", json_answer['data']['tenor_name'])
        self.assert_equal(False, json_answer['data']['bass'])
        self.assert_equal("Bass", json_answer['data']['bass_name'])
        self.assert_equal(2, json_answer['state_number'])
        self.send_ir_command('3')
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/state.json', timeout=1.0).read())
        self.assert_equal("Zeige Lied", json_answer['state'])
        self.assert_equal(False, json_answer['data']['general_song'])
        self.assert_equal("4", json_answer['data']['book_abbreviation'])
        self.assert_equal("4", json_answer['data']['book_id'])
        self.assert_equal("#ffff00", json_answer['data']['book_color'])
        self.assert_equal("59", json_answer['data']['number'])
        self.assert_equal("", json_answer['data']['title'])
        self.assert_equal(True, json_answer['data']['sopran'])
        self.assert_equal("Sopran", json_answer['data']['sopran_name'])
        self.assert_equal(True, json_answer['data']['alt'])
        self.assert_equal("Alt", json_answer['data']['alt_name'])
        self.assert_equal(True, json_answer['data']['tenor'])
        self.assert_equal("Tenor", json_answer['data']['tenor_name'])
        self.assert_equal(False, json_answer['data']['bass'])
        self.assert_equal("Bass", json_answer['data']['bass_name'])
        self.assert_equal(2, json_answer['state_number'])
        self.send_ir_command('4')
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/state.json', timeout=1.0).read())
        self.assert_equal("Zeige Lied", json_answer['state'])
        self.assert_equal(False, json_answer['data']['general_song'])
        self.assert_equal("4", json_answer['data']['book_abbreviation'])
        self.assert_equal("4", json_answer['data']['book_id'])
        self.assert_equal("#ffff00", json_answer['data']['book_color'])
        self.assert_equal("59", json_answer['data']['number'])
        self.assert_equal("", json_answer['data']['title'])
        self.assert_equal(True, json_answer['data']['sopran'])
        self.assert_equal("Sopran", json_answer['data']['sopran_name'])
        self.assert_equal(True, json_answer['data']['alt'])
        self.assert_equal("Alt", json_answer['data']['alt_name'])
        self.assert_equal(True, json_answer['data']['tenor'])
        self.assert_equal("Tenor", json_answer['data']['tenor_name'])
        self.assert_equal(True, json_answer['data']['bass'])
        self.assert_equal("Bass", json_answer['data']['bass_name'])
        self.assert_equal(2, json_answer['state_number'])
        time.sleep(0.1)

        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('guide')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IP)
        self.check_url('http://localhost:48080/state.json', '{"state": "Zeige Netzwerkadresse", "data": null, "state_nu'
                                                            'mber": 17}')
        time.sleep(6)

        self.send_ir_command('exit')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        # test datetime.json
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/datetime.json', timeout=1.0).read())
        server_date_time = datetime.strptime(json_answer['datetime'], "%Y-%m-%dT%H:%M:%S.%f")
        self.assert_true((datetime.now() - server_date_time).total_seconds() < 1.0, 'Check Server datetime')

        # test slave.json
        self.__application.general_settings.general_song_books = '0'
        self.check_url('http://localhost:48080/slave.json', '{"state": "idle", "data": null}')

        self.send_ir_command('4')
        self.send_ir_command('+')
        self.send_ir_command('5')
        self.check_url('http://localhost:48080/slave.json', '{"state": "idle", "data": null}')
        self.send_ir_command('9')
        time.sleep(6)
        self.check_url('http://localhost:48080/slave.json', '{"state": "idle", "data": null}')
        self.send_ir_command('exit')

        self.send_ir_command('0')
        self.send_ir_command('+')
        self.send_ir_command('5')
        self.check_url('http://localhost:48080/slave.json', '{"state": "idle", "data": null}')
        self.send_ir_command('9')
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_url('http://localhost:48080/slave.json', '{"state": "song", "data": {"number": "59", "title": ""}}')
        self.send_ir_command('exit')
        time.sleep(0.1)

        self.__application.general_settings.general_song_books = '0 2'

        self.send_ir_command('0')
        self.send_ir_command('+')
        self.send_ir_command('5')
        self.check_url('http://localhost:48080/slave.json', '{"state": "idle", "data": null}')
        self.send_ir_command('9')
        time.sleep(6)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_SONG)
        self.check_url('http://localhost:48080/slave.json', '{"state": "song", "data": {"color": "#ffff00", "book": "0"'
                                                            ', "number": "59", "title": ""}}')
        self.send_ir_command('exit')
        time.sleep(0.1)

        self.send_ir_command('power')
        time.sleep(0.1)
        self.send_ir_command('1')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_STANDBY)
        self.check_url('http://localhost:48080/slave.json', '{"state": "standby", "data": null}')
        self.send_ir_command('power')
        time.sleep(0.1)

        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)
        self.__application.general_settings.load_defaults()
        time.sleep(10)
        self.assert_true(not self.__application.display_server.is_running(), 'Check DisplayServer running')
        self.__application.general_settings.display_server = '1'
        time.sleep(10)
        self.assert_true(self.__application.display_server.is_running(), 'Check DisplayServer running')

        # test sung songs
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/sungsongs.json', timeout=1.0).read())
        self.assert_equal(4, len(json_answer))
        self.assert_equal(True, json_answer[0]['general_song'])
        self.assert_equal("0", json_answer[0]['book_abbreviation'])
        self.assert_equal("0", json_answer[0]['book_id'])
        self.assert_equal("#ffff00", json_answer[0]['book_color'])
        self.assert_equal("59", json_answer[0]['number'])
        self.assert_equal("", json_answer[0]['title'])
        self.assert_equal(False, json_answer[0]['sopran'])
        self.assert_equal("Sopran", json_answer[0]['sopran_name'])
        self.assert_equal(False, json_answer[0]['alt'])
        self.assert_equal("Alt", json_answer[0]['alt_name'])
        self.assert_equal(False, json_answer[0]['tenor'])
        self.assert_equal("Tenor", json_answer[0]['tenor_name'])
        self.assert_equal(False, json_answer[0]['bass'])
        self.assert_equal("Bass", json_answer[0]['bass_name'])

        for i in range(200):
            time.sleep(0.01)
            song = SongHandler.Song()
            song.number = str(i)
            self.__application.song_handler.sing_song(song)

        time.sleep(1)
        json_answer = json.loads(urllib2.urlopen('http://localhost:48080/sungsongs.json', timeout=1.0).read())
        self.assert_equal(100, len(json_answer))
        self.assert_equal("199", json_answer[0]['number'])

        self.check_url('http://localhost:48080/slave.json', '{"state": "idle", "data": null}')
        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.check_url('http://localhost:48080/slave.json', '{"state": "shutdown", "data": null}')
        self.__application.general_settings.load_defaults()

    def test_simple_update(self):
        """Testet ein normales Update."""
        self.test_result.start_test('Simple Update Test')
        # copy dummy update package
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'Dummy', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        # wait until the program has started
        time.sleep(35)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_UPDATE)

        # check back key
        self.send_ir_command('1')
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '12')
        time.sleep(0.5)
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '1')
        time.sleep(0.5)
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '')
        time.sleep(0.5)

        # enter confirmation number
        confirmation_number = UpdateHandler.get_update_confirmation_number()
        self.send_ir_command(confirmation_number[0])
        time.sleep(0.5)
        self.send_ir_command(confirmation_number[1])
        time.sleep(0.5)
        self.send_ir_command(confirmation_number[2])
        time.sleep(0.5)
        self.send_ir_command(confirmation_number[3])

        self.send_ir_command('ok')

        time.sleep(0.5)

        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)

    def test_update_lesser_version(self):
        """Testet ein Update auf eine zu kleine Version."""
        # wait until the program has started
        self.test_result.start_test('Update Lesser Version Test')
        # copy dummy update package
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'LesserVersion', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        time.sleep(25)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        self.check_expected_label_text('LabelStartState', u'Update kann nur auf eine neuere Version ausgeführt werden')

        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.__application.general_settings.load_defaults()

    def test_update_equal_version(self):
        """Testet ein Update auf eine gleiche."""
        # wait until the program has started
        self.test_result.start_test('Update Equal Version Test')
        # copy dummy update package
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'EqualVersion', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        time.sleep(25)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        self.check_expected_label_text('LabelStartState', u'Update kann nur auf eine neuere Version ausgeführt werden')

        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.__application.general_settings.load_defaults()

    def test_update_faulty_update_package(self):
        """Testet ein Update mit einem ungültigen Update-Package."""
        # wait until the program has started
        self.test_result.start_test('Update Faulty Update Package Test')
        # copy dummy update package
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'FaultyUpdatePackage', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        self.check_expected_label_text('LabelStartState', u'Fehler: Update konnte nicht entpackt werden. Update kann '
                                                          u'nicht ausgeführt werden.')

        time.sleep(15)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_QUIT)
        self.send_ir_command('ok')
        time.sleep(0.1)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)
        self.assert_true(self.__application.state_machine.shutdown_mode == Global.CONST.SM.SHUTDOWN, u'Shutdown Mode')
        self.__application.general_settings.load_defaults()

    def test_deny_update(self):
        """Testet das ablehnen eines Updates."""
        # wait until the program has started
        self.test_result.start_test('Deny Update Test')
        # copy dummy update package
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'Dummy', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        time.sleep(35)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_UPDATE)

        # check back key
        self.send_ir_command('1')
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '12')
        time.sleep(0.5)
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '1')
        time.sleep(0.5)
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '')
        time.sleep(0.5)

        # enter confirmation number
        confirmation_number = UpdateHandler.get_update_confirmation_number()
        self.send_ir_command(confirmation_number[0])
        time.sleep(0.5)
        self.send_ir_command(confirmation_number[1])
        time.sleep(0.5)
        self.send_ir_command(confirmation_number[2])
        time.sleep(0.5)
        self.send_ir_command(confirmation_number[3])

        self.send_ir_command('exit')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.send_ir_command('ok')
        time.sleep(0.5)
        time.sleep(0.5)

        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)

    def test_update_timeout(self):
        """Testet das timeout im Update-Bestätigen."""
        # wait until the program has started
        self.test_result.start_test('Update Timeout Test')
        # copy dummy update package
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'Dummy', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        time.sleep(35)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_CONFIRM_UPDATE)

        # check back key
        self.send_ir_command('1')
        self.send_ir_command('2')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '12')
        time.sleep(0.5)
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '1')
        time.sleep(0.5)
        self.send_ir_command('back')
        time.sleep(0.5)
        self.check_expected_label_text('LabelConfirmUpdate', '')

        time.sleep(26)

        self.check_expected_gui_screen(Global.CONST.SM.STATE_IDLE)

        self.send_ir_command('power')
        time.sleep(0.5)
        self.send_ir_command('ok')
        time.sleep(0.5)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_QUIT)

    def test_update_nextstart(self):
        """Testet das Nextstart bei einem Update."""
        # wait until the program has started
        self.test_result.start_test('Update Nextstart Test')
        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'Dummy', 'UpdateMDS.tar.gz'),
                        UpdateHandler.UPDATE_PACKAGE_HOME_PATH)
        if not os.path.isdir(UpdateHandler.UPDATE_DIR):
            os.makedirs(UpdateHandler.UPDATE_DIR)

        shutil.copyfile(os.path.join(CurrDir, '..', 'TestData', 'Updates', 'Dummy', 'Nextstart.py'),
                        os.path.join(UpdateHandler.UPDATE_DIR, 'Nextstart.py'))
        time.sleep(10)
        self.check_expected_gui_screen(Global.CONST.SM.STATE_START)
        self.check_expected_label_text('LabelStartState', u'Update wird weiter ausgeführt...')

    def is_running(self):
        """Gibt zurück ob ein Test ausgeführt wird."""
        self.thread.is_alive()

    def start(self):
        """Start die Testausführung."""
        self.thread.start()

    def stop(self):
        """Beendet die Testausführung und speichet das Ergebnis."""
        self.test_result.finish('TestResult.txt')


def get_time_str():
    """Gibt die aktuelle Uhrzeit zurück.
    
    :return: String, Datum und Uhrzeit
    """
    temp = str(datetime.now())[:23]
    if len(temp) < 23:
        temp += '.000'
    return temp


def get_commit():
    """Liefet die ID des aktuellen Kommits zurüc.k
    
    :return: Unicode, ID des aktuellen Kommits.
    """
    return unicode(os.popen('git rev-parse HEAD').read().strip())


class TestResult(object):
    """Implementiert ein Testergebniss. Kann 'Success' oder 'Fail' sein.
    """

    file_end_fail = '''[-------------------------------------------------------------------------------------------------]
[                                     FFFFF    A     I  L                                         ]
[                                     F       A A    I  L                                         ]
[                                     FFFF   AAAAA   I  L                                         ]
[                                     F     A     A  I  LLLLL                                     ]
[-------------------------------------------------------------------------------------------------]
'''

    file_end_success = '''[-------------------------------------------------------------------------------------------------]
[                         SSSS   U   U   CCC   CCC  EEEEE   SSSS    SSSS                          ]
[                        SS      U   U  CC    CC    E      SS      SS                             ]
[                         SSSS   U   U  C     C     EEEEE   SSSS    SSSS                          ]
[                            SS  U   U  CC    CC    E          SS      SS                         ]
[                         SSSS    UUU    CCC   CCC  EEEEE   SSSS    SSSS                          ]
[-------------------------------------------------------------------------------------------------]
'''

    def __init__(self):
        self.start_time = datetime.now()
        self.finish_time = None
        self.tests_number = 0
        self.test_failed = 0
        self.tests_succeded = 0
        self.current_test_verdict = 'Success'
        self.__log_text = '''[-------------------------------------------------------------------------------------------------]
[                                                                                                 ]
[                                           TEST RESULT                                           ]
[                                                                                                 ]
[-------------------------------------------------------------------------------------------------]
'''

    def get_duration(self):
        """Gibt zurück wie viel Zeit seit dem Start des Tests vergangen ist.
        
        :return: Int Testdauer in Sekunden
        """
        if not self.finish_time:
            return (datetime.now() - self.start_time).total_seconds()
        else:
            return (self.finish_time - self.start_time).total_seconds()

    def __log(self, message, severity=u''):
        """Loggt eine Meldung und gibt sie auf der Konsole aus.
        
        :param message: Unicode Meldung, die geloggt werden soll.
        :param severity: Unicode Gewichtung der Meldung.
        :return: 
        """
        text = u'[' + get_time_str() + u'] [% 4s] ' % severity + message
        print text
        self.__log_text += text + '\n'

    def fail(self, message=u''):
        """Loggt eine Meldung mit "Fail" als Gewichtung. Der Test gilt danach als "Fail".
        
        :param message: Unicode Meldung, die geloggt werden soll.
        """
        self.current_test_verdict = u'Fail'
        self.__log(message, u'Fail')

    def pass_(self, message=u''):
        """Loggt eine Meldung mit "pass" als Gewichtung.
        
        :param message: Unicode Meldung, die geloggt werden soll.
        """
        self.__log(message)

    def message(self, message=u''):
        """Loggt eine Meldung ohne Gewichtung.

        :param message: Unicode Meldung, die geloggt werden soll.
        """
        self.__log(message, u'~')

    def write(self, file_name):
        """Schreibt das Testergebiss in eine Datei.
        
        :param file_name: String, Pfad unter dem gespeichert wird.
        """
        with codecs.open(file_name, 'w', 'utf-8') as result_file:
            result_file.write(self.__log_text)

    def start_test(self, test_name):
        """Muss zum Starten des Tests aufgerufen werden.
        
        :param test_name: Name des Tests
        """
        self.current_test_verdict = None
        self.tests_number += 1
        self.__log(u'Test %s started' % test_name, u'----')

    def finish_test(self):
        """Muss zum Beenden des Tests aufgerufen werden."""
        if self.current_test_verdict is None:
            self.tests_succeded += 1
            self.__log(u'Test finished', u'OK')
        else:
            self.test_failed += 1
            self.__log(u'Test finished', u'Fail')

    def finish(self, file_name):
        """Schließt eine Testausführugn ab und speichert das Log.
        
        :param file_name: Unicode, Pfad unter dem das Ergebniss gespeichert wird.
        :return: 
        """
        if self.finish_time:
            return
        self.finish_time = datetime.now()
        if self.test_failed:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), u'Fail')
            self.__log_text += TestResult.file_end_fail
        else:
            self.__log('%i Tests finished %s succeeded, %s failed; time: %ss' % (
                self.tests_number, self.tests_succeded, self.test_failed, str(self.get_duration())), u'OK')
            self.__log_text += TestResult.file_end_success
        self.write(file_name)
