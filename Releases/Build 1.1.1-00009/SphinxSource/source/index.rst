.. MediaDisplay-Server documentation master file, created by
   sphinx-quickstart on Fri Mar 31 00:03:33 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Dokumentation des MediaDisplay-Server's
=======================================

.. toctree::
   :titlesonly:
   :caption: Inhalt:

   Module/DisplayServer
   Module/Global
   Module/Logger
   Module/main
   Module/MDSWidgets
   Module/OsInterface
   Module/OsInterfaceNoRpi
   Module/OsInterfaceRaspberry
   Module/SettingsHandler
   Module/SongHandler
   Module/ScreenHandler
   Module/StateMachine
   Module/TestRunner
   Module/UpdateHandler
   Module/Watchdog

Indizes
=======

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
