# Updateanleitung Liedanzeige

1. Updatepaket von [mediadock.org/software](https://www.mediadock.org/software) -> Liedanzeige -> Updates herunterladen.
2. Updatepaket (UpdateMDS.tar.gz) in das Stammverzeichnis eines USB-Sticks kopieren.
3. Liedanzeige herunterfahren.
4. USB-Stick mit Liedanzeige verbinden.
5. Liedanzeige starten. 
6. Den angezeigten Code mit der Fernbedienung eingeben und mit "OK" bestätigen. Das Update wird nun automatisch 
ausgeführt. Dabei kann es passieren, dass die Liedanzeige mehrfach neu gestartet wird. Nach dem letzten Neustart 
erscheint die Meldung "Update kann nur auf eine neuere Version ausgeführt werden". Das ist normal und muss nicht weiter 
beachtet werden.
7. Programm per Fernbedienung beenden mit "Power" und "9". Es wird Desktop des Betriebsystems angezeigt.
8. USB-Maus mit Liedanzeige verbinden.
8. USB-Stick mit Button oben rechts auswerfen und von der Liedanzeige abziehen.
9. Liedanzeige über das Menü neu starten und USB-Maus abziehen.
