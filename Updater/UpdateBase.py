#!/usr/bin/env python
# coding=utf-8
"""Dieses Modul stellt einige Widgets für PySide zur Verfügung.
"""

from PySide import QtCore, QtGui
from datetime import datetime
import re
import shutil
import threading
import os

TAG_RE = re.compile(r'<[^>]+>')


def remove_tags(text):
    """Entfernt alle Tags aus einem String.

    :param text: String
    :return: String ohne tags
    """
    return TAG_RE.sub('', text)


def on_rpi():
    try:
        if os.uname()[4].startswith("arm"):
            return True
        else:
            return False
    except AttributeError:
        return False


def get_home_path():
    if on_rpi():
        return '/home/pi'
    else:
        return os.path.expanduser('~')


CURR_DIR = os.path.abspath(os.path.dirname(__file__))
SOURCES_DIR = os.path.join(get_home_path(), 'Source')


def get_update_dir():
    return os.path.join(get_home_path(), 'Update')


def write_finish():
    file_ = open(os.path.join(get_update_dir(), 'finish'), 'a')
    file_.close()


def reboot():
    if on_rpi():
        os.system('sudo shutdown -r now')


def remove_old_sources():
    if os.path.exists(SOURCES_DIR):
        shutil.rmtree(SOURCES_DIR)


def copy_new_sources():
    shutil.copytree(os.path.join(CURR_DIR, 'Source'), SOURCES_DIR)


class StretchedLabel(QtGui.QLabel):
    """StretchedLabel ist ein Label dessen Schrift an die Größe des Labels anpasst.

    :param args: Wird an den Superkonstruktor weitergegeben.
    :param kwargs: Wird and den Superkonstruktor weitergegeben.
    """

    # Mit diesem Signal kann der Text des Labels gesetzt werden. Aufruf über StretchedLabel.set_text.emit(unicode text)
    set_text = QtCore.Signal(unicode)

    # Mit diesem Signal wird die Schriftgröße an den zur Verfügung stehenden Platz angepasst. Aufruf über
    # StretchedLabel.fit_font_size.emit()
    fit_font_size = QtCore.Signal()

    # Mit diesem Signal wird das StyleSheet des Labels gesetzt. Aufruf über
    # StretchedLabel.set_style_sheet.emit(unicode text)
    set_style_sheet = QtCore.Signal(unicode)

    def __init__(self, *args, **kwargs):
        QtGui.QLabel.__init__(self, *args, **kwargs)
        self.setSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Ignored)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.set_text.connect(self.__set_text)
        self.fit_font_size.connect(self.__fit_font_size)
        self.set_style_sheet.connect(self.__set_style_sheet)

    @QtCore.Slot(unicode)
    def __set_style_sheet(self, style):
        self.setStyleSheet(style)

    @QtCore.Slot(unicode)
    def __set_text(self, text):
        self.setText(text)

    def resizeEvent(self, event):
        self.__fit_font_size()

    @QtCore.Slot()
    def __fit_font_size(self):

        text = remove_tags(self.text())

        if text == '':
            return

        font = self.font()
        text_width = self.fontMetrics().boundingRect(text).width()
        margins = self.getContentsMargins()
        width = self.width() - margins[0] - margins[2]
        text_height = self.fontMetrics().boundingRect(text).height()
        height = self.height() - margins[1] - margins[3]

        x_ratio = float(width) / float(text_width)
        y_ratio = float(height) / float(text_height)
        ratio = min(x_ratio, y_ratio) * 0.95
        old_font_size = float(font.pointSize())
        new_font_size = max(1.0, font.pointSize() * ratio)
        if new_font_size - old_font_size > old_font_size * 0.01 or new_font_size < old_font_size:
            font.setPointSize(new_font_size)
            self.setFont(font)


class DigitalClock(StretchedLabel):
    """DigitalClock ist ein Widget, das die Uhrzeit/Datum im Digitalformat anzeigt. Das Anzeigeformat muss beim
    initialisieren angegeben werden

    :param time_format: Bestimmt das Anzeigeformat. Dokumentation unter https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
    :param args: Wird an den Superkonstruktor weitergegeben.
    :param kwargs: Wird and den Superkonstruktor weitergegeben.
    """
    def __init__(self, time_format, *args, **kwargs):
        StretchedLabel.__init__(self, *args, **kwargs)
        self.time_format = time_format
        self.setText(datetime.now().strftime(self.time_format))

        timer = QtCore.QTimer(self)
        # noinspection PyUnresolvedReferences
        timer.timeout.connect(self.__update_time)
        timer.start(10)

    def __update_time(self):
        self.setText(datetime.now().strftime(self.time_format))


class StretchedImage(QtGui.QLabel):
    """StretchedImage ist ein Widget, ein Bild anzeigt und dieses auf die Fläche des ganzen Bereiches ausdehnt.

    :param args: Wird an den Superkonstruktor weitergegeben.
    :param kwargs: Wird and den Superkonstruktor weitergegeben.
    """

    # Mit diesem Signal kann das Bild gesetzt werden. Aufruf über StretchedImage.set_image.emit(unicode text)
    set_image = QtCore.Signal(unicode)

    def __init__(self, *args, **kwargs):
        QtGui.QLabel.__init__(self, *args, **kwargs)
        self.setMinimumSize(1, 1)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.set_image.connect(self.__set_image)

    @QtCore.Slot(unicode)
    def __set_image(self, image_path):
        image = QtGui.QPixmap(image_path)
        min_size = min(self.height(), self.width())
        pic_size = QtCore.QSize(min_size, min_size)
        self.setPixmap(image.scaled(pic_size, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation))


class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.init_gui()

    def init_gui(self):
        """Initialisiert die Widgets der GUI."""

        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        label1 = StretchedLabel('Liedanzeige - Update')
        label1.setStyleSheet(u"QLabel { background-color: blue ; color : white; }")
        label2 = StretchedLabel(u'Strom nicht ausschalten!')
        label2.setStyleSheet(u"QLabel { background-color: blue ; color : white; }")
        self.label_text = StretchedLabel(u'\u00A9 +++ replace year +++ Liedanzeige Entwicklungsteam')
        self.label_text.setStyleSheet(u"QLabel { background-color: blue ; color : white; }")
        grid.addWidget(label1, 1, 0, 1, 1)
        grid.addWidget(label2, 2, 0, 1, 1)
        grid.addWidget(self.label_text, 3, 0, 1, 1)

        self.setLayout(grid)

        self.setGeometry(0, 0, 250, 150)

        self.setWindowTitle(u'Liedanzeige - Update')

        self.showFullScreen()

    def set_message(self, message, error=False):
        self.label_text.set_text.emit(message)
        self.label_text.fit_font_size.emit()
        if error:
            self.label_text.set_style_sheet.emit(u"QLabel { background-color : red; color: black; }")
        else:
            self.label_text.set_style_sheet.emit(u"QLabel { background-color: blue ; color: white; }")


class UpdateBaseApp(QtGui.QApplication):

    def __init__(self, *args, **kwargs):
        super(UpdateBaseApp, self).__init__(*args, **kwargs)
        self.main_window = MainWindow()
        self.thread = threading.Thread(target=self.update, args=(), name='Update')

    def run(self):
        self.thread.start()
        return self.exec_()
