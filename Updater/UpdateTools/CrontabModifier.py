# coding=utf-8

import os


class CrontabModifier(object):

    def __init__(self):
        self.__file_path = None
        self.__tabs = None

    @staticmethod
    def __check_tab(tab):
        return True  # todo

    def initialize(self, temp_file_path):
        # todo check temp_file_path
        self.__file_path = temp_file_path
        os.system('sudo crontab -l > {}'.format(self.__file_path))
        with open(self.__file_path, 'r') as cron_tab_file:
            content_lines = cron_tab_file.readlines()
        tabs = list()
        for content_line in content_lines:
            tabs.append(content_line.strip('\n\r'))
        self.__tabs = tabs

    def add_tab(self, tab):
        if self.__check_tab(tab):
            if tab not in self.__tabs:
                self.__tabs.append(tab)

    def remove_tab(self, tab):
        while tab in self.__tabs:
            self.__tabs.remove(tab)

    def commit(self):
        if not self.__tabs[-1] == '':
            self.__tabs.append('')
        with open(self.__file_path, 'w') as cron_tab_file:
            cron_tab_file.write(os.linesep.join(self.__tabs))
        os.system('sudo crontab {}'.format(self.__file_path))
