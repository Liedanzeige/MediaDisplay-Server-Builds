# coding=utf-8

import os

START_UNIT_TEMPLATE = 'sudo systemctl start {}'
STOP_UNIT_TEMPLATE = 'sudo systemctl stop {}'
ENABLE_UNIT_TEMPLATE = 'sudo systemctl enable {}'
DISABLE_UNIT_TEMPLATE = 'sudo systemctl disable {}'
RESTART_UNIT_TEMPLATE = 'sudo systemctl restart {}'
RELOAD_UNIT_TEMPLATE = 'sudo systemctl reload {}'


class SystemctlHandler(object):

    @classmethod
    def __check_unitname(cls, unitname):
        if ' ' in unitname:
            return False
        if '=' in unitname:
            return False
        return True

    @classmethod
    def start_unit(cls, unitname):
        if cls.__check_unitname(unitname):
            os.system(START_UNIT_TEMPLATE.format(unitname))

    @classmethod
    def stop_unit(cls, unitname):
        if cls.__check_unitname(unitname):
            os.system(STOP_UNIT_TEMPLATE.format(unitname))

    @classmethod
    def enable_unit(cls, unitname):
        if cls.__check_unitname(unitname):
            os.system(ENABLE_UNIT_TEMPLATE.format(unitname))

    @classmethod
    def disable_unit(cls, unitname):
        if cls.__check_unitname(unitname):
            os.system(DISABLE_UNIT_TEMPLATE.format(unitname))

    @classmethod
    def restart_unit(cls, unitname):
        if cls.__check_unitname(unitname):
            os.system(RESTART_UNIT_TEMPLATE.format(unitname))

    @classmethod
    def reload_unit(cls, unitname):
        if cls.__check_unitname(unitname):
            os.system(RELOAD_UNIT_TEMPLATE.format(unitname))
