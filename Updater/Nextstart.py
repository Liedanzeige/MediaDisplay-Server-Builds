#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time

import UpdateBase


class UpdateApp(UpdateBase.UpdateBaseApp):

    def update(self):
        time.sleep(5)
        self.main_window.set_message(u'Update wird beendet...')
        UpdateBase.write_finish()
        time.sleep(5)
        self.quit()


if __name__ == '__main__':
    App = UpdateApp(sys.argv)
    exit_code = App.run()
    UpdateBase.reboot()
    sys.exit(exit_code)
