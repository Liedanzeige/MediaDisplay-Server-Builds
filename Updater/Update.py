#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import shutil
import os

import UpdateBase
import UpdateTools


class UpdateApp(UpdateBase.UpdateBaseApp):

    def update(self):

        update_dir_path = UpdateBase.get_update_dir()

        time.sleep(5)
        self.main_window.set_message(u'Update wird ausgeführt\u2026')
        time.sleep(1)

        self.main_window.set_message(u'Lösche altes Programm\u2026')
        time.sleep(1)
        UpdateBase.remove_old_sources()

        self.main_window.set_message(u'Kopiere neues Programm\u2026')
        time.sleep(1)
        UpdateBase.copy_new_sources()

        self.main_window.set_message(u'Installiere Skripte\u2026')
        time.sleep(1)
        shutil.copyfile(
            os.path.join(update_dir_path, 'UpdateFiles', 'extendpartition.sh'),
            '/usr/local/bin/extendpartition.sh'
        )

        self.main_window.set_message(u'Modifiziere Crontabs\u2026')
        time.sleep(1)
        cron_tab_modifier = UpdateTools.CrontabModifier()
        cron_tab_modifier.initialize(os.path.join(update_dir_path, 'crontab.tmp'))
        cron_tab_modifier.add_tab('@reboot sudo bash /usr/local/bin/extendpartition.sh')
        cron_tab_modifier.commit()

        self.main_window.set_message(u'Starte neu\u2026')
        time.sleep(5)
        UpdateBase.write_finish()
        self.quit()


if __name__ == '__main__':
    App = UpdateApp(sys.argv)
    exit_code = App.run()
    UpdateBase.reboot()
    sys.exit(exit_code)
