# -*- coding: utf-8 -*-

import tarfile
import os
from datetime import datetime as dt
import tkSimpleDialog as sd
import tkMessageBox as mb
from Tkinter import *
import codecs
import shutil
import inspect


BUILDS_FILE_PATH = os.path.abspath('..\\Releases\\builds.txt')


def get_now_str():
    return str(dt.now())[0:23]


def print_log(message):
    pl_message = '%s : %s\n' % (get_now_str(), message.replace('\n', ''))
    print pl_message


def modify_file(input_filename, output_filename=None, encoding='utf-8'):
    if output_filename is None:
        output_filename = input_filename
    src_file = codecs.open(input_filename, 'r', encoding=encoding)
    src_content = src_file.readlines()
    src_file.close()
    j = -1
    temp_src_content = src_content[:]
    for k in src_content:
        j += 1
        # remove lines :  +++ remove this line +++
        if '+++ remove this line +++' in k:
            del temp_src_content[j]
            j -= 1
            continue
        temp_line = k
        for l, m in replacers.iteritems():
            temp_line = temp_line.replace(l, m)
        temp_src_content[j] = temp_line
    src_file = codecs.open(output_filename, 'w', encoding=encoding)
    src_file.writelines(temp_src_content)
    src_file.close()


def get_build():
    builds_file = open(BUILDS_FILE_PATH, 'r')
    content = builds_file.read()
    builds_file.close()
    content_int = list()
    for i in content.split('\n'):
        if i != '':
            content_int.append(int(i.split(' : ')[0]))
    last_build = max(content_int)
    new_build = last_build + 1
    return new_build


def write_build(build, release, commit):
    builds_file = open(BUILDS_FILE_PATH, 'a')
    builds_file.write('%s : %s : %s\n' % (build, release, commit))
    builds_file.close()


def get_commit(dir_path):
    curr_path = os.getcwd()
    os.chdir(dir_path)
    current_commit = os.popen('git rev-parse HEAD').read().strip()
    os.chdir(curr_path)
    return current_commit

master_window = Tk()

script_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
source_base_dir = os.path.join(script_dir, '..', '..', 'MediaDisplay-Server')
source_dir = os.path.join(source_base_dir, 'Source')
doc_source_dir = os.path.join(source_base_dir, 'SphinxSource')
source_changelog_path = os.path.join(source_base_dir, 'CHANGELOG.md')

commit = get_commit(source_dir)
if mb.askyesno('Build Type', 'NightlyBuild ausführen?'):
    build = 'XXXXX'
    counter = 65
    release = 'NB_%s_%s' % (dt.now().strftime('%y%m%d'), chr(counter))
    release_full_name = '%s-%s' % (release, build)
    target_dir = os.path.join(script_dir, '..', 'NightlyBuilds', 'Build %s' % release_full_name)
    while os.path.isdir(target_dir):
        counter += 1
        release = 'NB_%s_%s' % (dt.now().strftime('%y%m%d'), chr(counter))
        release_full_name = '%s-%s' % (release, build)
        target_dir = os.path.join(script_dir, '..', 'NightlyBuilds', 'Build %s' % release_full_name)
else:
    build = '%05i' % get_build()
    ans = sd.askstring('Release', 'Insert the name of the release:')
    if ans is None:
        exit()

    release = ans

    release_full_name = '%s-%s' % (release, build)
    target_dir = os.path.join(script_dir, '..', 'Releases', 'Build %s' % release_full_name)

    write_build(build, release, commit)

now = dt.now()
replacers = dict()
replacers['# +++ remove this +++'] = ''
replacers['+++ replace build +++'] = build
replacers['+++ replace full date +++'] = '%02i.%02i.%04i' % (now.day, now.month, now.year)
replacers['+++ replace year +++'] = str(now.year)
replacers['+++ replace release +++'] = release
replacers['+++ release full name +++'] = release_full_name
replacers['+++ replace commit +++'] = commit

target_source_dir = os.path.join(target_dir, 'Source')
target_doc_source_dir = os.path.join(target_dir, 'SphinxSource')
target_doc_dir = os.path.join(target_dir, 'Dokumentation')
target_tar = os.path.join(target_dir, 'MediaDisplay-Server Build %s.tar.gz' % release_full_name)
update_tar = os.path.join(target_dir, 'UpdateMDS.tar.gz')
updater_files_dir = os.path.normpath(os.path.join(script_dir, '..', 'Updater'))
update_files_dir = os.path.normpath(os.path.join(script_dir, '..', 'UpdateFiles'))
version_file_path = os.path.join(target_dir, 'Version.txt')
updater_build_dir = os.path.join(target_dir, 'UpdaterBuild')

# todo check if testes are executed


# todo make log file


# todo check if everything is committed


# todo check if everything ist pushed


# copy files
os.makedirs(target_dir)
shutil.copytree(source_dir, target_source_dir, ignore=shutil.ignore_patterns('*.pyc',
                                                                             'tmp*',
                                                                             '*TestExecutionQueue.txt',
                                                                             '*TestResult.txt'))

# copy doc files
shutil.copytree(doc_source_dir, target_doc_source_dir, ignore=shutil.ignore_patterns('*.pyc',
                                                                                     'tmp*',
                                                                                     '*TestExecutionQueue.txt',
                                                                                     '*TestResult.txt'))

# modify files
for dirpath, dirnames, filenames in os.walk(target_source_dir):
    for filename in filenames:
        if filename.endswith('.py') or filename.endswith('.html'):
            modify_file(os.path.join(dirpath, filename))
modify_file(os.path.join(target_doc_source_dir, 'source', 'conf.py'))

# create documentation
cwd = os.getcwd()
os.chdir(target_doc_source_dir)
os.system('make html')
os.chdir(cwd)

# delete pyc files
for dirpath, dirnames, filenames in os.walk(target_source_dir):
    for filename in filenames:
        if filename.endswith('.pyc'):
            os.remove(os.path.join(dirpath, filename))

# make tar.gz
with tarfile.open(target_tar, "w:gz") as tar:
    tar.add(target_source_dir, arcname='Source')
    tar.add(target_doc_dir, arcname='Dokumentation')

# copy changelog
shutil.copyfile(source_changelog_path, os.path.join(target_dir, 'CHANGELOG.md'))

# make version file
open(version_file_path, 'w').write(release)

# create update package
os.makedirs(updater_build_dir)
for file_name in filter(lambda x: x.endswith('.py'), os.listdir(updater_files_dir)):
    shutil.copy(os.path.join(updater_files_dir, file_name), os.path.join(updater_build_dir, file_name))
shutil.copytree(os.path.join(updater_files_dir, 'UpdateTools'), os.path.join(updater_build_dir, 'UpdateTools'))
for dirpath, dirnames, filenames in os.walk(updater_build_dir):
    for filename in filenames:
        if filename.endswith('.py') or filename.endswith('.html'):
            modify_file(os.path.join(dirpath, filename))
with tarfile.open(update_tar, "w:gz") as tar:
    tar.add(target_source_dir, arcname='Source')
    tar.add(update_files_dir, arcname='UpdateFiles')
    tar.add(source_changelog_path, arcname='CHANGELOG.md')
    for file_name in os.listdir(updater_build_dir):
        tar.add(os.path.join(updater_build_dir, file_name), arcname=file_name)
    tar.add(version_file_path, arcname='Version.txt')
shutil.rmtree(updater_build_dir)

print 'Release %s was created' % release_full_name
