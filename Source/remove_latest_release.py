# coding=utf-8

import os
import tkMessageBox
import Tkinter
import shutil

BUILDS_FILE_PATH = os.path.abspath('..\\Releases\\builds.txt')

master_window = Tkinter.Tk()

# read latest build number
builds_file = open(BUILDS_FILE_PATH, 'r')
temp = builds_file.read().split('\n')[-2].split(' : ')
builds_file.close()
build = temp[0]
release = temp[1]

# check if build dir exists
root_release_path = os.path.abspath(os.path.join(u'..\\Releases\\Build %s-%s' % (release, build)))
if not os.path.isdir(root_release_path):
    tkMessageBox.showerror(u'Error', u'The release path %s doesn\'t exists' % root_release_path)
    exit()

# ask if the latest release should really be removed
ans = tkMessageBox.askyesno(u'Remove Release', u'Should the release %s really be removed?' % root_release_path)
if not ans:
    exit()

# remove dir "Build<Number> Release <Release>"
shutil.rmtree(root_release_path)

# remove latest build number from file
build_file = open(BUILDS_FILE_PATH, 'r')
content = build_file.read()
build_file.close()
content_list = content.split('\n')
content_list_new = content_list[:-2]
content_new = '\n'.join(content_list_new) + '\n'
build_file = open(BUILDS_FILE_PATH, 'w')
build_file.write(content_new)
build_file.close()

# say if it was successful
tkMessageBox.showinfo(u'Remove Release', u'The release %s was removed successfully!' % root_release_path)

exit()
