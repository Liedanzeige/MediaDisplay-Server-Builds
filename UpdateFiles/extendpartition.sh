#!/bin/bash
filepath=/home/pi/.partext

if [ ! -f "$filepath" ];
then
	start=$(cat /sys/block/mmcblk0/mmcblk0p2/start)
	end=$(($start+$(cat /sys/block/mmcblk0/mmcblk0p2/size)))
	newend=$(($(cat /sys/block/mmcblk0/size)-8))
    parted -s /dev/mmcblk0 unit s resizepart 2 $(($newend-$start))
	resize2fs -p /dev/mmcblk0p2
	touch -a "$filepath"
fi